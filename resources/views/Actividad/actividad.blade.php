@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/Actividad/actividad5.js?v=1') }}">
		</script>	

		
	@stop


@section('content') 


<div class="container">
		<div class="content" id="main_actividad" class="row" data-url="{{ url('actividad') }}" ></div>
		<div id="main" class="row" data-url="{{ url('personas') }}" data-url-parques="{{ url('parques') }}">
		
		@if($id_datos!=0)
			<h3 class="head text-center"><strong class="text-warning">MODIFICANDO:</strong> actividad N° <b>{{$id_datos}}</b></h3>
		@else
			<h3 class="head text-center"><strong class="text-warning">REGISTRANDO NUEVA ACTIVIDAD</strong></h3>
		@endif
		

		<div class="row">
			<div class="board">
				


				<!-- MENU DE CREACIÓN DE LA ACTIVIDAD-->
				<div class="board-inner">

					<ul class="nav nav-tabs" id="myTab">
						<div class="liner"></div>
						
						<li class="active">
							<a href="#datos_comunidad" data-toggle="tab" title="bootsnipp goodies">
								<span class="round-tabs one">
									<i class="glyphicon glyphicon-user"></i><br>I
								</span>
							</a>
						</li>

						<li>
							<a href="#home" data-toggle="tab" title="welcome">
								<span class="round-tabs two">
									<i class="glyphicon glyphicon-floppy-saved"></i><br>II
								</span> 
							</a>
						</li>

						<li>
							<a href="#profile" data-toggle="tab" title="profile">
								<span class="round-tabs three">
									<i class="glyphicon glyphicon-floppy-saved"></i><br>III
								</span> 
							</a>
						</li>

						<li>
							<a href="#settings" data-toggle="tab" title="blah blah">
								<span class="round-tabs four">
									<i class="glyphicon glyphicon-floppy-saved"></i><br>IV
								</span> 
							</a>
						</li>

						<li>
							<a href="#doner" data-toggle="tab" title="completed">
								<span class="round-tabs five">
									<i class="glyphicon glyphicon-floppy-saved"></i><br>V
								</span>
							</a>
						</li>

					</ul>
				</div>
				<!--FIN:: MENU DE CREACIÓN DE LA ACTIVIDAD-->


			<form method="POST" id="form_registro_actividad">
				<!-- DATOS BASICOS DE LA ACTIVIDAD  -->
				<div class="tab-content">
					<div class="tab-pane fade" id="home">
						
						<div class="row">
							<div class="col-md-12 col-xs-12">
								<h3 class="head text-center"><strong class="text-warning">PASO II:</strong> Datos basicos de la actividad</h3>
								<p class="narrow text-center">
									Espacio para registrar las actividades basicas de la actvidad.
								</p>
								<br><br>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<label> 1. Programa </label>
									<input type="hidden" name="id_datos" id="id_datos" value="{{$id_datos}}">
									<select class="form-control" data-live-search="true" name="programa">
										<option value="">Seleccionar</option>
										@foreach($programas as $programa)
											<option value="{{$programa['idPrograma']}}">{{strtoupper($programa['programa'])}}</option>	
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<label> 2. Actividad </label>
									<select class="form-control" data-live-search="true" name="actividad">
										<option value="">Seleccionar</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<label> 3. Temática</label>
									<select class="form-control" data-live-search="true" name="tematica">
										<option value="">Seleccionar</option>
									</select>
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<label> 4. Componente </label>
									<select class="form-control" data-live-search="true" name="componente">
										<option value="">Seleccionar</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<p>
									<a href="" class="btn btn-success btn-xs" id="btn_agregar_datos_actividad"> Agregar datos de la actividad <span style="margin-left:10px;" class="glyphicon glyphicon-plus"></span></a><br>
								</p>
							</div>
						</div>

						<div id="alerta_datos">
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label>Registro de datos de la actividad</label>
									<div class="bs-example" data-example-id="bordered-table"> 
										<table class="table display no-wrap table-condensed table-bordered table-min" id="datos_actividad">
											<thead> 
												<tr class="active"> 
													<th>#</th> 
													<th>Programa</th> 
													<th>Actividad</th> 
													<th>Tematica</th> 
													<th>Componente</th> 
													<th>Eliminar</th> 
												</tr> 
											</thead>
												<tbody id="registros_datos">
													@if($dataActividad)
														@if($dataActividad->datosActividad)
															@foreach($dataActividad->datosActividad as $data)
																<tr> 
																	<td>#</td>
																	<td>{{$data->programa['programa']}}</td> 
																	<td>{{$data->actividad['actividad']}}</td> 
																	<td>{{$data->tematica['tematica']}}</td> 
																	<td>{{$data->componente['componente']}}</td> 
																	<td class="text-center"><button type="button" data-rel="{{$data['i_pk_id']}}" data-funcion="eliminar" class="eliminar_dato_actividad"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
																	</td>
																</tr> 
															@endforeach
														@endIf	
													@endIf
												</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<br><br>
								<center>Registro de datos de la actividad</center>
								<br>
								<hr><br><br>
							</div>
						</div>

					</div>
					<!-- FIN:: DATOS BASICOS DE LA ACTIVIDAD  -->



					<div class="tab-pane fade in active" id="datos_comunidad">
						
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<h3 class="head text-center"><strong class="text-success">PASO I:</strong> DATOS DE LA COMUNIDAD</h3>
								<p class="narrow text-center">
									Registro del tipo de comunidad que va asistir a la actividad.
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 1. Localidad </label>
									<input type="hidden" name="id" id="id" value="{{$id_datos}}">
									<select class="form-control" data-live-search="true" name="localidad_comunidad" id="localidad_comunidad">
										<option value="">Seleccionar</option>
										@if($localidades)
										@foreach($localidades as $localidad)
												
											@if($dataActividad)
												@if($dataActividad['i_fk_localidadComunidad']==$localidad['Id_Localidad'])
													<option value="{{$localidad['Id_Localidad']}}" selected>{{strtoupper($localidad['Nombre_Localidad'])}}</option>
												@else
													<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
												@endif
											@else
												<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
											@endif

										@endforeach
										@endif


									</select>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 2. Upz </label>
									<select class="form-control" data-live-search="true" name="Id_Upz_Comunidad" id="Id_Upz_Comunidad">
										
											@if($dataActividad)
												@if($dataActividad->upz_comunidad)
													<option value="{{$dataActividad['i_fk_upzComunidad']}}" selected>{{strtoupper($dataActividad->upz_comunidad['Upz'])}}</option>
												@else
													<option value="">Seleccionar</option>
												@endif
											@endif

									</select>
								</div>
							</div>
							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 3. Barrio </label>
									<select class="form-control" data-live-search="true" name="Id_Barrio_Comunidad" id="Id_Barrio_Comunidad">

											@if($dataActividad)
												@if($dataActividad->barrio_comunidad)
													<option value="{{$dataActividad['i_fk_barrioComunidad']}}" selected>{{strtoupper($dataActividad->barrio_comunidad['Barrio'])}}</option>
												@else
													<option value="">Seleccionar</option>
												@endif
											@endif

									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 4. Institución, Grupo, Comunidad:	</label>
									
									<input type="text" class="form-control" name="institucion_g_c" autocomplete="off" onkeyup="javascript:this.value=this.value.toUpperCase();" value="{{ ($dataActividad) ? $dataActividad['vc_institutoGrupoComunidad'] : '' }}">
								</div>
							</div>

							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 5. Características de la Poblacion a beneficiar:	</label>
									<select class="form-control"  name="caracteristicaPoblacion" id="caracteristicaPoblacion" >
										
										@if($caracteristicasPoblacion)
										@foreach($caracteristicasPoblacion as $caracteristicaPoblacion)
											
											@if($dataActividad)
												@if($dataActividad->datos_caracteristicas->count()>0)
													@if($dataActividad->datos_caracteristicas[0]->caracteristicaPoblacion[0]['i_pk_id']==$caracteristicaPoblacion['i_pk_id'])
														<option value="{{$caracteristicaPoblacion['i_pk_id']}}" selected>{{strtoupper($caracteristicaPoblacion['tx_caracteristicas'])}}</option>
													@else
														<option value="{{$caracteristicaPoblacion['i_pk_id']}}">{{strtoupper($caracteristicaPoblacion['tx_caracteristicas'])}}</option>
													@endif	
												@else
													<option value="{{$caracteristicaPoblacion['i_pk_id']}}">{{strtoupper($caracteristicaPoblacion['tx_caracteristicas'])}}</option>
												@endif	
											@else

												<option value="{{$caracteristicaPoblacion['i_pk_id']}}">{{strtoupper($caracteristicaPoblacion['tx_caracteristicas'])}}</option>

											@endif

										@endforeach
										@endif	
									</select>
								</div>
							</div>

							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 6. Específico:	</label>
									<select class="form-control" multiple data-selected-text-format="count > 4" name="caracteristicaEspecifica" id="caracteristicaEspecifica">
										<option value="">Seleccionar</option>
									</select>
									<input type="hidden" name="datosCaracterisitica" id="datosCaracterisitica" value="{{ json_encode($actividadEspecificas) }}">
								</div>
								<div class="form-group text-right">
									<button type="button" class="btn btn-link text-right" id="mostrar_seleccionados">Seleccionados</button>
								</div>
							</div>

							<!--<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label> 7. Número de Asistentes a Beneficiar:</label>
									<input type="text" class="form-control" name="fecha_suscripcion" autocomplete="off" onKeyPress="return soloNumeros( event )">
								</div>
							</div>-->
						</div>


						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<br><br>
								<center>Registro del tipo de comunidad que va asistir a la actividad.</center>
								<br>
								<hr><br><br>
							</div>
						</div>

					</div>





					<div class="tab-pane fade" id="profile">
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<h3 class="head text-center"><strong class="text-primary">PASO III</strong> Programación y asignación de la actividad</h3>
								<p class="narrow text-center">
									Espacio para registrar la programación y asignación de la activadad, se puede agregar varias actividades que coincidan con los mismos datos en los diferentes ítem
								</p>
								<br><br>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<label>1. CONFIGURACION HORARIO:</label><br>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<P> Responsable </P>
									<select class="form-control" name="responsable">
										<option value="">Seleccionar</option>
										@if($resosablesActividad)
										@foreach($resosablesActividad as $responsable)
											<option value="{{$responsable->persona['Id_Persona']}}">
												{{strtoupper($responsable->persona['Primer_Apellido'].' '.$responsable->persona['Segundo_Apellido'].' '.$responsable->persona['Primer_Nombre'].' '.$responsable->persona['Segundo_Nombre'])}}
											</option>	
										@endforeach
										@endIf
									</select>
									<input type="hidden" class="form-control" name="responsable_validada" id="responsable_validada" data-role1="datepicker" autocomplete="off">
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<p> Fecha de ejecución</p>
									<input type="text" class="form-control" name="fecha_ejecucion" id="fecha_ejecucion"  data-role1="datepicker" placeholder="dd/mm/yyyy" autocomplete="off" >
									<input type="hidden" class="form-control" name="fecha_ejecucion_validada" id="fecha_ejecucion_validada"  data-role1="datepicker"  autocomplete="off" >
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<p> Hora inicio</p>
									<input type="text" class="form-control" name="hora_inicio" id="hora_inicio" data-role1="datepicker" autocomplete="off">
									<input type="hidden" class="form-control" name="hora_inicio_validada"  id="hora_inicio_validada" data-role1="datepicker" autocomplete="off">
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<p> Hora fin</p>
									<input type="text" class="form-control" name="hora_fin" id="hora_fin" autocomplete="off">
									<input type="hidden" class="form-control" name="hora_fin_validada" id="hora_fin_validada" autocomplete="off">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<p>
										<a href="javascript(0)" class="btn btn-success btn-xs" id="btn_agregar_validar_disponiblidad"> Validar datos<span style="margin-left:10px;" class="glyphicon glyphicon-plus"></span></a>
									</p>
								</div>
								<div id="alerta_datos_acompanantes"></div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="label0">
										
										@if($dataActividad)
											DATOS VALIDADOS:
										@endIf
									</span>
								</div>
							</div>
							<div class="col-md-3 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="label11">
										@if($dataActividad)
											@if($dataActividad->datosActividad->count()>0)
												Actividad: {{$dataActividad->datosActividad[0]->actividad['actividad']}}
											@endIf
										@endIf
									</span>
									<span class="label label-warning labelDatos" id="label1">
										@if($dataActividad)
											@if($dataActividad->responsable)
												Responsable: {{$dataActividad->responsable['Primer_Apellido']}} {{$dataActividad->responsable['Segundo_Apellido']}} {{$dataActividad->responsable['Primer_Nombre']}}
											@endIf
										@endIf
									</span>
								</div>
							</div>
							<div class="col-md-2 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="label2">
										@if($dataActividad)
											Fecha: {{$dataActividad['d_fechaEjecucion']}}
										@endIf
									</span>
								</div>
							</div>
							<div class="col-md-2 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="label3">
										@if($dataActividad)
											Hora inicio: {{$dataActividad['t_horaInicio']}}
										@endIf
									</span>
								</div>
							</div>
							<div class="col-md-2 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="label4">
										@if($dataActividad)
											Hora fin: {{$dataActividad['t_horaFin']}}
										@endIf
									</span>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12"><br><hr>
							</div>	
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>2. REGISTRO DE ACOMPAÑANTE:</label><br>
								</div>
							</div>

							<div class="col-xs-6 col-sm-3">
								<p class="form-group">
									<button type="button" class="btn btn-primary btn-block" id="busquedaAcompananteLocalidad">Acompañante de mi localidad</button>
								</p>
							</div>

							<div class="col-xs-6 col-sm-3">
								<p class="form-group">
									<button type="button" class="btn btn-primary btn-block" id="busquedaAcompananteOtraLocalidad">Acompañante de otra localidad</button>
								</p>
							</div>

							<div class="col-xs-6 col-sm-6">
							</div>

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12"><br><hr>
							</div>	
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>3. NUMERO DE ASISTENTES:</label><br>
								</div>
							</div>

							<div class="col-xs-12 col-sm-2">
								<p class="form-group">
									<input type="text" class="form-control" name="numeroAsitentes" id="numeroAsitentes" value="{{($dataActividad)?$dataActividad['i_numeroAsistentes']:''}}">
								</p>
							</div>

							<div class="col-xs-6 col-sm-2">
								<p class="form-group">
									<button type="button" class="btn btn-info btn-xs btn-block" id="validarNumeroAcompa">Validar</button>
								</p>
							</div>
							<div class="col-xs-6 col-sm-2">
								<p class="form-group">
									<button type="button" class="btn btn-info btn-xs btn-block" data-toggle="modal" data-target=".bd-example-modal-lg">
										<span class="glyphicon glyphicon-align-justify"></span>  Lista de ayuda
									</button>
								</p>
							</div>

							<div class="col-xs-6 col-sm-6">
							</div>

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12"><br><hr>
							</div>	
						</div>

						<div class="row">
							<div class="col-xs-6 col-sm-6">
								<label>JUSTIFICACIÓN MENOR # DE ASISTENTES CON RELACION AL TALENTO HUMANO PROGRAMADO</label>
								<select class="form-control" multiple data-selected-text-format="count > 4" name="justificacionMenor" id="justificacionMenor">

										@if($justificacionMenor)
										@foreach($justificacionMenor as $justificacion)
											<option value="{{$justificacion['i_pk_id']}}">
												{{strtoupper($justificacion['vc_justificacion'])}}
											</option>	
										@endforeach
										@endIf
								</select>

							</div>
							<div class="col-xs-6 col-sm-6">
								<label>JUSTIFICACIÓN MAYOR # DE ASISTENTES CON RELACION AL TALENTO HUMANO PROGRAMADO</label>
								<select class="form-control" multiple data-selected-text-format="count > 4" name="justificacionMayor" id="justificacionMayor" value="[1]">

										@if($justificacionMayor)
										@foreach($justificacionMayor as $justificacion)
											<option value="{{$justificacion['i_pk_id']}}">
												{{strtoupper($justificacion['vc_justificacion'])}}
											</option>	
										@endforeach
										@endIf
								</select>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<br><br>
								<center>Programación y asignación de la actividad.</center>
								<br>
								<hr><br><br>
							</div>
						</div>

					</div>

					<div class="tab-pane fade" id="settings">

						<div class="col-xs-12 col-md-12">
							<h3 class="head text-center"><strong class="text-danger">PASO IV: </strong>DATOS DEL ESCENARIO</h3>
						</div>	
						<div class="col-xs-12">
							@include('direccion')
						</div>

						<div class="col-xs-12 col-md-6">
							<div class="row">
								<fieldset>
									<div class="col-xs-12">
										<div class="form-group">
											<label class="control-label" for="Escenario">Escenario</label>
											<input type="text" name="Escenario" class="form-control" value="{{ ($dataActividad) ? $dataActividad['vc_escenario'] : '' }}">
										</div>
									</div>
									<div class="col-md-6 col-xs-12">
										<div class="form-group">
											<label class="control-label" for="Cod_IDRD">Cod. IDRD</label>
											<input type="text" name="Cod_IDRD" class="form-control" value="{{ ($dataActividad) ? $dataActividad['vc_codigoParque'] : '' }}">
											<a href="https://www.idrd.gov.co/SIM/Parques/buscadorParques.php" type="button" class="btn btn-link btn-xs" target="_new">Búscador parques</a>
										</div>
									</div>
									<div class="col-md-6 col-xs-12">
										<div class="form-group">
										</div>
									</div>
									<div class="col-xs-12"><hr></div>
									<div class="col-xs-12 col-md-4">
										<div class="form-group">
											<label>Localidad </label>
											<select class="form-control" data-live-search="true" name="localidad_escenario" id="localidad_escenario">
												<option value="">Seleccionar</option>
												@if($TodasLocalidades)
												@foreach($TodasLocalidades as $localidad)
												
													@if($dataActividad)
														@if($dataActividad['i_fk_localidadEscenario']==$localidad['Id_Localidad'])
															<option value="{{$localidad['Id_Localidad']}}" selected>{{strtoupper($localidad['Nombre_Localidad'])}}</option>
														@else
															<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
														@endif
													@else
														<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
													@endif

												@endforeach
												@endif
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-md-4">
										<div class="form-group">
											<label class="control-label" for="Id_Upz">Upz</label>
											<select class="form-control" data-live-search="true" name="Id_Upz_escenario" id="Id_Upz_escenario">
												<option value="">Seleccionar</option>
												@if($dataActividad)
													@if($dataActividad->upz_escenario)
														<option value="{{$dataActividad['i_fk_upzEscenario']}}" selected>{{strtoupper($dataActividad->upz_escenario['Upz'])}}</option>
													@else
														<option value="">Seleccionar</option>
													@endif
												@endif
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-md-4">
										<div class="form-group">
											<label class="control-label" for="Id_Barrio">Barrio</label>
											<select class="form-control" data-live-search="true" name="Id_Barrio_escenario" id="Id_Barrio_escenario">
												<option value="">Seleccionar</option>
												@if($dataActividad)
													@if($dataActividad->barrio_escenario)
														<option value="{{$dataActividad['i_fk_barrioEscenario']}}" selected>{{strtoupper($dataActividad->barrio_escenario['Barrio'])}}</option>
													@else
														<option value="">Seleccionar</option>
													@endif
												@endif
											</select>
										</div>
									</div>
									<div class="col-xs-12"><hr></div>
									<div class="col-md-6 col-xs-12">
										<div class="form-group">
											<label class="control-label" for="Cod_IDRD">Latitud</label>
											<input type="text" class="form-control" name="Latitud" value="{{ ($dataActividad) ? $dataActividad['db_latitud'] : '' }}" readonly>
										</div>
									</div>
									<div class="col-md-6 col-xs-12">
										<div class="form-group">
											<label class="control-label" for="Cod_IDRD">Longitud</label>
											<input type="text" class="form-control" name="Longitud" value="{{ ($dataActividad) ? $dataActividad['db_longitud'] : '' }}" readonly>
										</div>
									</div>
									
								</fieldset>
							</div>
						</div>

						<div class="col-xs-12 col-md-6">
							<div class="form-group ">
								<label class="control-label" for="">Ubicación:   </label>
								<div id="map" style="height: 350px;width: 100%"></div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<br><br>
								<center>Datos del escenario donde se va a realizar la actividad.</center>
								<br>
								<hr><br><br>
							</div>
						</div>
					</div>


					



					<div class="tab-pane fade" id="doner">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<div class="text-center">
									<i class="img-intro icon-checkmark-circle"></i>
								</div>
								<h3 class="head text-center">ASPECTOS A TENER EN CUENTA</h3>
							</div>
						</div>

						<div class="row">
							<div class="col-md-2 col-xs-12">
								<div class="form-group">
									<label> 1. Hora de implementación:	</label>
									<input type="text" class="form-control" name="hora_implementacion" id="hora_implementacion" autocomplete="off" value="{{ ($dataActividad) ? $dataActividad['t_horaImplementacion'] : '' }}">
								</div>
							</div>

							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 2. Punto de encuentro:	</label><br><br>
									<input type="text" class="form-control" name="punto_encuentro" autocomplete="off" value="{{ ($dataActividad) ? $dataActividad['vc_puntoEncuentro'] : '' }}">
								</div>
							</div>

							<div class="col-md-6 col-xs-12">
								<div class="form-group">
								</div>
							</div>

							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<span class="label label-warning labelDatos" id="labelhoraInicio">
										@if($dataActividad)
											Hora inicio: {{$dataActividad['t_horaInicio']}}
										@endIf
									</span>
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label> 3. Nombre de la persona de contacto:</label>
									<input type="text" class="form-control" name="nombre_persona" autocomplete="off" value="{{ ($dataActividad) ? $dataActividad['vc_personaContacto'] : '' }}">
								</div>
							</div>

							<div class="col-md-4 col-xs-12">
								<div class="form-group">
									<label> 4. Rol en la comunidad:	</label>
									<input type="text" class="form-control" name="roll_comunidad" autocomplete="off" value="{{ ($dataActividad) ? $dataActividad['vc_rollComunidad'] : '' }}">
								</div>
							</div>

							<div class="col-md-2 col-xs-12">
								<div class="form-group">
									<label> 5. Telefono:	</label>
									<input type="text" class="form-control" name="telefono_persona" autocomplete="off" value="{{ ($dataActividad) ? $dataActividad['i_telefono'] : '' }}">
								</div>
							</div>

							<div class="col-md-6 col-xs-12">
								<div class="form-group">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<p class="text-center">
									<a href="javascript:void(0)" class="btn btn-success btn-outline-rounded green" id="registrarActividad"> Registrar actividad <span style="margin-left:10px;" class="glyphicon glyphicon-ok"></span></a>
								</p>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-xs-12">
								<br><br>
								<center>Aspectos a tener en cuenta.</center>
								<br>
								<hr><br><br>
							</div>
						</div>

					</div>


					<div class="clearfix"></div>
				</div>
			</form>

			</div>
		</div>


		<div class="modal fade"  id="myModal_mal">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-body">
              	<h4><strong>MENSAJE:</strong></h4><br>
                <ul class="list-group" id="list_error"></ul>
              </div>
              
              <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="cerrar">Cerrar</button>
              </div>

            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="row">
			
			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			      	<div class="row">
			      		
			      		<div class="col-md-1"></div>
			      		<div class="col-md-10">
			      			<br><br>
			      			<h4>TABLA DE CONVENCIONES</h4>
			      		</div>
			      		<div class="col-md-12"></div>

			      		<div class="col-md-1"></div>
				      	<div class="col-md-10">
					      	@include('limitesAcompanates')
						</div>
						<div class="col-md-1"></div>
					</div>
			    </div>
			  </div>
			</div>

		</div>

</div>

<script type="text/javascript">

	var vectorJustificacion = '{{ ($justificacionMenorD) ? implode(",", $justificacionMenorD) : ""}}';
	console.log(vectorJustificacion);
	$('#justificacionMenor').selectpicker('val', vectorJustificacion.split(","));
	$('#justificacionMenor').selectpicker('refresh');


	var vectorJustificacion2 = '{{ ($justificacionMayorD) ? implode(",", $justificacionMayorD) : ""}}';
	$('#justificacionMayor').selectpicker('val', vectorJustificacion2.split(","));
	$('#justificacionMayor').selectpicker('refresh');
	
</script>
@stop


