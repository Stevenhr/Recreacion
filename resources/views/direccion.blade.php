<div class="col-md-2">
	<div class="form-group">
		<label>Tipo de Vía</label>
		<select class="form-control" data-live-search="true" name="tipoVia1" id="tipoVia1">
			<option value="">Seleccionar</option>
			<option>Autopista AU</option>
			<option>Avenida AV</option>
			<option>Avenida Calle AC</option>
			<option>Avenida Carrera AK</option>
			<option>Bulevar BL</option>
			<option>Calle CL</option>
			<option>Carrera KR</option>
			<option>Carretera CT</option>
			<option>Circular CQ</option>
			<option>Circunvalar CV</option>
			<option>Cuentas Corridas CC</option>
			<option>Diagonal DG</option>
			<option>Pasaje PJ</option>
			<option>Paseo PS</option>
			<option>Peatonal PT</option>
			<option>Transversal TV</option>
			<option>Troncal TC</option>
			<option>Variante VT</option>
			<option>Vía VI</option>
		</select>
	</div>
</div>

<div class="col-md-2">
	<div class="form-group">
		<label>Número</label>
		<input type="text" name="num1"  id="num1" class="form-control" value="" placeholder="10a">
	</div>
</div>

<div class="col-md-1">
	<div class="form-group">
		<label></label>
		<select class="form-control" data-live-search="true" name="tipoVia2" id="tipoVia2">
			<option value="">Seleccionar</option>
			<option>Este ESTE</option>
			<option>Norte NORTE</option>
			<option>Oeste OESTE</option>
			<option>Sur SUR</option>
		</select>
	</div>
</div>

<div class="col-md-1">
	<label></label>
	<label>#</label>
</div>

<div class="col-md-2">
	<div class="form-group">
		<label>Número</label>
		<input type="text" name="num2"  id="num2" class="form-control" value="" placeholder="10a">
	</div>
</div>


<div class="col-md-1">
	<div class="form-group">
		<label></label>
		<select class="form-control" data-live-search="true" name="tipoVia3" id="tipoVia3">
			<option value="">Seleccionar</option>
			<option>Este ESTE</option>
			<option>Norte NORTE</option>
			<option>Oeste OESTE</option>
			<option>Sur SUR</option>
		</select>
	</div>
</div>

<div class="col-md-1">
	<div class="form-group">
		<label></label>
		<input type="text" name="num3"  id="num3" class="form-control" value="" placeholder="5">
	</div>
</div>

<div class="col-md-2">
	<div class="form-group">
		<label>Otros datos</label>
		<input type="text" name="otrosdatos"  id="otrosdatos" class="form-control" value="" placeholder="Bloq. Manz.">
	</div>
</div>


<div class="col-xs-12">
	<div class="form-group">
		<label class="control-label" for="Direccion">Dirección</label>
		<input type="text" name="Direccion"  id="Direccion" class="form-control" value="{{ ($dataActividad) ? $dataActividad['vc_direccion'] : '' }}" readonly="readonly">
	</div>
</div>

<div class="col-xs-12"></div>

<script type="text/javascript">
	$(function()
	{
		var tipoVia1='';
		var num1='';
		var tipoVia2='';
		var num2='';
		var tipoVia3='';
		var num3='';
		var otrosdatos='';

		$('select[name="tipoVia1"]').on('change', function(e)
	    {
	        
	        tipoVia1 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);

	    });

	    $('input[name="num1"]').on('blur', function(e)
   		{
   			num1 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);
    	});

    	$('select[name="tipoVia2"]').on('change', function(e)
	    {
	        
	        tipoVia2 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);

	    });

	    $('input[name="num2"]').on('blur', function(e)
   		{
   			num2 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);
    	});


    	$('select[name="tipoVia3"]').on('change', function(e)
	    {
	        
	        tipoVia3 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);

	    });

    	$('input[name="num3"]').on('blur', function(e)
   		{
   			num3 = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);
    	});

    	$('input[name="otrosdatos"]').on('blur', function(e)
   		{
   			otrosdatos = $(this).val();
	        $('#Direccion').val(tipoVia1+' '+num1+' '+tipoVia2+'   # '+num2+' '+tipoVia3+' - '+num3+' '+otrosdatos);
    	});

	});
</script>