
<h4>Tabla limites de acompañantes:</h4><br>

  	@php
  	use App\Modulos\ActividadRecreativa\LimiteAcompanate;
  	$listas = LimiteAcompanate::all();
  	$var=1;
  	@endphp



<table id="tbl_listaAcomp" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
    <thead> 
        <tr> 
           <th>#</th> 
            <th scope="row" class="text-center" style="width:auto;">Actividad</th> 
            <th>N° Acompañante</th> 
            <th>Limite inferior</th> 
            <th>Limite superior</th>  
        </tr> 
    </thead>
    <tfoot>
		<tr>
			<th>#</th> 
            <th scope="row" class="text-center" style="width:auto;">Actividad</th> 
            <th>N° Acompañante</th> 
            <th>Limite inferior</th> 
            <th>Limite superior</th>  
		</tr>
	</tfoot>
    <tbody >
    	@if($listas)
			@foreach($listas as $lista)		
			 <tr class="bg-warning">    		
				<td>{{$var++}}</td>
				<td>{{$lista['vc_actividad']}}</td>
				<td>{{$lista['i_numAcompa']}}</td>
				<td>{{$lista['i_limiteInferior']}}</td>
				<td>{{$lista['i_limiteSuperior']}}</td>
			</tr>
			@endforeach
		@endif
    </tbody>
</table>
<br><br><br>