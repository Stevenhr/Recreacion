@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/MisActividad/tablaGestionEjecucion.js?v=7') }}"></script>	
	@stop


@section('content') 
<?php use App\Modulos\Configuracion\Configuracion; ?>
<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('gestionEjecucion') }}" ></div>

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				<div class="col-md-12">
					<h4 ><b>TABLA DE LAS EJECUCIONES POR GESTIONAR</b><br><span class="glyphicon glyphicon-th-list text-{{$color}} " > <label class="text-{{$color}} ">{{$tipo}} </label></span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>
				<div class="col-md-12">

					<div class="table-responsive">
					<table id="tbl_resposablePrograma" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
		            <thead> 
		                <tr style="{{$style}}"> 
		                    <th>#</th> 
		                    <th>Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th>
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th >Programación / <br>Ejecución</th>
		                    <th >Estado</th>
		                    
		                </tr> 
		            </thead>
		            <tfoot>
						<tr>
							<th>#</th> 
		                    <th scope="row" class="text-center" style="width:auto;">Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th> 
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th >Programación / <br>Ejecución</th>
		                    <th >Estado</th>
						</tr>
					</tfoot>
		            <tbody>
		            <?php $num=1;?>
		            @if($actividades!='')
		                @foreach ($actividades as $actividad) 
		                    	<tr class="something" data-row="{{ $actividad['i_pk_id'] }}" >
			                        <td class="col-md-1">{{$num}}</td>
			                        <td class="col-md-13"><b><p class="text-info text-center" style="font-size: 15px">{{$actividad['i_pk_id']}} </p></b></td>
			                        <td >{{$actividad['d_fechaEjecucion']}}</td>
			                        <td>{{$actividad['t_horaInicio']}}</td>
			                        <td>{{$actividad['t_horaFin']}}</td>
			                        <td>{{($actividad->datosActividad->count()>0)?$actividad->datosActividad[0]->programa['programa']:''}}</td>
			                        <td>{{($actividad->datosActividad->count()>0)?$actividad->datosActividad[0]->actividad['actividad']:''}}</td>
			                        <td>
			                        	<label>Tematicas:</label><br>
			                        	@if($actividad->datosActividad->count()>0)
				                        	@foreach ($actividad->datosActividad as $datoTC) 
				                        		<ul>
												 	<li>{{$datoTC->tematica['tematica']}}</li>
												</ul>
				                        	@endforeach
				                        @endif
			                        	<label>Componente:</label>
			                        	@if($actividad->datosActividad->count()>0)
				                        	@foreach ($actividad->datosActividad as $datoTC) 
				                        		<ul>
												  	<li>{{$datoTC->componente['componente']}}</li>
												</ul>
				                        	@endforeach
				                        @endif
			                        </td>
			                        <td>{{$actividad->responsable['Primer_Apellido']}} {{$actividad->responsable['Segundo_Apellido']}}<br>{{$actividad->responsable['Primer_Nombre']}}</td>
			                        <td>
			                        	<label>Responsable:</label><br>
			                        	{{$actividad->gestor['Primer_Apellido']}} {{$actividad->gestor['Segundo_Apellido']}}<br>{{$actividad->gestor['Primer_Nombre']}}
			                        	<br><label>Acompantes:</label><br>
			                        	@foreach ($actividad->acompanates as $acompanante) 
			                        		<ul>
											 	<li>{{$acompanante->usuario['Primer_Apellido']}} {{$acompanante->usuario['Segundo_Apellido']}} {{$acompanante->usuario['Primer_Nombre']}}</li>
											</ul>
			                        	@endforeach
			                        </td>
			                        <th class="text-center">
			                        	
			                        	<ul class="list-group">
										  <li class="list-group-item"><button type="button" class="btn btn-link btn-xs btn-block" data-rel="{{$actividad['i_pk_id']}}" data-funcion="programacion" data-toggle="modal" data-target="#modalProgramacion"><span class="glyphicon glyphicon-eye-open"></span> Ver programaciòn </button></li>

										  <li class="list-group-item"><button type="button" class="btn btn-success btn-xs btn-block" data-rel="{{$actividad['i_pk_id']}}" data-funcion="ejecucionDatos" data-toggle="modal" data-target="#modalDatosEjecucion"><span class="glyphicon glyphicon-list-alt"></span> Datos registrados </button></li>
										</ul>


										<?php
											$carbon = new \Carbon\Carbon();
											$date1 = $carbon->createFromFormat('Y-m-d', $actividad['d_fechaEjecucion']);
											$date11 = $date1->addDay(1);
											$fechaInicio = $date11->format('Y-m-d');

											$date = $carbon->createFromFormat('Y-m-d', $actividad['d_fechaEjecucion']);
											$endDate2 = $date->addDay(4);
											$fechaLimite = $endDate2->format('Y-m-d');
										?>

										<?php 
											if($carbon->between($date11, $endDate2)  && $tipo!="Todas las actividades.")
											{
										?>
											<ul class="list-group">
											  <li class="list-group-item"><button type="button" class="btn btn-success btn-xs btn-block" data-rel="{{$actividad['i_pk_id']}}" data-funcion="ejecucion" data-toggle="modal" data-target="#modalEjecucion"><span class="glyphicon glyphicon-pencil"></span> Registro ejecución </button></li>
											 </ul>

											<div class="btn-group">
											    <button type="button" class="btn btn-success btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-val="0"><span class="glyphicon glyphicon-ok"></span></button>
											    <button type="button" class="btn btn-warning btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-val="1"><span class="glyphicon glyphicon-arrow-left"></span></button>
											    <button type="button" class="btn btn-danger btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-val="2"><span class="glyphicon glyphicon-remove"></span></button>
											</div>

										<?php 
											}
										?>
											<br>
											<h5>Fechas de Aprobación:</h5>
											<p>{{$fechaInicio}} al {{$fechaLimite}}</p>

										<div id="{{$actividad['i_pk_id']}}mensaje"></div>
			                        </th>

			                        <th>
			                        	<?php
			                        		$tabla='Programación:';
			                        		switch($actividad['i_estadoAprobaPrograma']){

				                        		case Configuracion::INCOMPLETA:
				                        			$tabla=$tabla.' <span class="label label-danger"> Incompleta</span>
				                        							<div id="'.$actividad['i_pk_id'].'mensaje"></div>
				                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
				                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
					                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
					                        							<button type="submit" class="btn btn-link btn-xs"   >
																			<span class="glyphicon glyphicon-edit"></span>  Editar
																		</button>
																	</form>

																		                        					
																	<button type="button" data-funcion="eliminar" data-rel="'.$actividad['i_pk_id'].'" class="btn btn-link btn-xs">
																	<span class="glyphicon glyphicon-trash"></span>  Elimnar
																	</button>
																	';
																	
				                        							
				                        		break;

				                        		case Configuracion::PENDIENTE:
				                        			$tabla=$tabla.'<span class="label label-info"><b>Pendiente de revisión</span>';
				                        		break;

				                        		case Configuracion::APROBADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b></b> Aprobada</span>';
				                        		break;

				                        		case Configuracion::DEVUELTO:
				                        			$tabla=$tabla.' <span class="label label-warning"><b></b> Denegada</span>
				                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
				                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
					                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
					                        							<button type="submit" class="btn btn-link btn-xs"   >
																			<span class="glyphicon glyphicon-edit"></span>  Editar
																		</button>
																	</form>';
																	
				                        					
				                        		break;

				                        		case Configuracion::CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Confirmación:';
				                        	switch($actividad['i_estadoConfirmaPrograma']){
				                        		case Configuracion::CONFIRMADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b><b>Programación:</b> Confirmada</span>';
				                        		break;

				                        		case Configuracion::CONFIRMADO_CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
	  
				                        		case Configuracion::CONFIRMADO_REPROGRAMADA:
				                        			$tabla=$tabla.'<span class="label label-success">Confirmada/ Reprogramada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}


				                        	$tabla=$tabla.'<br><br>Ejecución:';
				                        	switch($actividad['i_estadoRegistroEjecucion']){
				                        		case Configuracion::EJECUTADA:
				                        			$tabla=$tabla.'<span class="label label-success">Registrada.</span>';
				                        		break;

				                        		case Configuracion::EJECUTADA_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada al registrar.</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Estado ejecución:';
				                        	switch($actividad['i_estadoAprobaEjecucion']){
				                        		case Configuracion::EJECUCION_APROBADA:
				                        			$tabla=$tabla.'<span class="label label-success"> Aprobada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger"> Cancelada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_DENEGADA:
				                        			$tabla=$tabla.'<span class="label label-warning"> Denegada</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;

				                        	}
				                        	echo $tabla;
			                        	?>
			                        </th>
			                        
			                    </tr>
		                    <?php $num++; ?>
		                @endforeach
		            @endIf
		            </tbody>
		        </table>

				</div>

				<div class="col-md-12">
					<div class="form-group">
					  <label for="comment">Observaciones:</label>
					  <textarea class="form-control" rows="5" id="tx_observaciones" id="tx_observaciones"></textarea>
					</div>
				</div>

			</form>
		</div>


			<!-- MODAL PROGRAMACION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalProgramacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-primary text-center" id="myModalLabel">Programación de la actividad<br>
							<b>ID:</b> <label id="id_actividadProgramacion"></label></h3>
						</div>

						<form id="form_agregar_estudio_comveniencia">
							<div id="cargando"></div>
							<div class="modal-body" id="modalBody">		
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA COMUNIDAD:</h4>
									</div>
																		
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioP"></p>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Institución, Grupo, Comunidad:</label>
										<p id="modalinstitucionGrupoCP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Características de la Población:</label>
										<p id="modalCaracteristicasP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Específico:</label>
										<p id="modalCaracEspecificasP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Programa
													</th>
													<th>
														Actividad
													</th>
													<th>
														Temática
													</th>
													<th>
														Componente
													</th>
												</tr>
											</thead>
											<tbody id="datosModalActividad">
											</tbody>
										</table>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>PROGRAMACIÓN Y ASIGNACIÓN DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-3 col-sm-3">
										<label>Responsable:</label>
										<p id="modalResponsableP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Fecha ejecución:</label>
										<p id="modalFechaEjecucionP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora inicio:</label>
										<p id="modalHoraInicioP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora fin:</label>
										<p id="modalHoraFinP"></p>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DEL ESCENARIO:</h4>
									</div>

									<div class="col-xs-4 col-sm-4">
										<label>Dirección:</label>
										<p id="modalDireccionEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Escenario:</label>
										<p id="modalEscenarioEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Codigo IDRD:</label>
										<p id="modalCodigoIP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioEP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>ASPECTOS A TENER EN CUENTA:</h4>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Hora de implementación:</label>
										<p id="horaImplementacion"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Punto de encuentro:</label>
										<p id="puntoEncuentro"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Nombre de la persona de contacto:</label>
										<p id="nombreContacto"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Rol en la comunidad:</label>
										<p id="rollComunidad"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Telefono:</label>
										<p id="telefono"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>OBSERVACIONES:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Usuario
													</th>
													<th>
														Observación
													</th>
													<th>
														Tipo
													</th>
													<th>
														Fecha
													</th>
												</tr>
											</thead>
											<tbody id="ObservacionActividad">
											</tbody>
										</table>
										</div>	
									</div>

								</div>					
							</div>

							<div class="modal-footer" >
								<div class="row">
									<div class="col-xs-12 col-sm-12" style="text-align: left;">
										<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
									</div>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>





			<!-- MODAL EJECUCION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalDatosEjecucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-success text-center" id="myModalLabel">Datos registrados en la ejecución de la actividad<br>
							<b>ID:</b> <label id="id_actividadDatosEjecucion"></label></h3>
						</div>
						
						<div class="modal-body">	
							<div class="row">
								
								<div class="col-xs-12 col-sm-12">
									<div class="table-responsive">
										<table id="tbl_datosEjecucionActividad" class="table table-responsive table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
								            <thead> 
								                <tr style=""> 
								                    <th rowspan="2">#</th> 
								                    <th rowspan="2">Institución, grupo o comunidad:</th> 
								                    <th rowspan="2">Entidad</th> 
								                    <th rowspan="2">Localidad</th> 
								                    <th rowspan="2">Upz</th> 
								                    <th rowspan="2">Barrio</th>  
								                    <th rowspan="2">Tipo</th> 
								                    <th rowspan="2">Condición</th>
								                    <th rowspan="2">Situación</th>
								                    <th colspan="2">0 a 5</th>
								                    <th colspan="2">6 a 12</th>
								                    <th colspan="2">13 a 17</th>
								                    <th colspan="2">18 a 26</th>
								                    <th colspan="2">27 a 59</th>
								                    <th colspan="2">60 a mas</th>
								                    <th >Total</th>
								                    <th >Eliminar</th>
								                </tr> 
								                <tr style=""> 
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>Total</th>
								                    <th >Eliminar</th>
								                </tr> 
								            </thead>
								            <tfoot>
												<tr style=""> 
													<th rowspan="2">#</th> 
								                    <th rowspan="2">Institución, grupo o comunidad:</th> 
								                    <th rowspan="2">Entidad</th> 
								                    <th rowspan="2">Localidad</th> 
								                    <th rowspan="2">Upz</th> 
								                    <th rowspan="2">Barrio</th>  
								                    <th rowspan="2">Tipo</th> 
								                    <th rowspan="2">Condición</th>
								                    <th rowspan="2">Situación</th>

								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>M</th>
								                    <th>F</th>
								                    <th>Eliminar</th>
								                </tr> 
												<tr style=""> 
								                    <th colspan="2">0 a 5</th>
								                    <th colspan="2">6 a 12</th>
								                    <th colspan="2">13 a 17</th>
								                    <th colspan="2">18 a 26</th>
								                    <th colspan="2">27 a 59</th>
								                    <th colspan="2">60 a mas</th>
								                    <th>F</th>
								                    <th>Eliminar</th>
								                </tr> 
											</tfoot>
								            <tbody id="tdEjecucion">
								            </tbody>
								        </table>
								    	</div>
							        </div>
								</div>
							
							
								<div class="col-xs-12 col-sm-12">
									<hr>
								</div>	

								<div class="col-xs-12 col-sm-12">
									<div class="table-responsive">
										<table id="tbl_datosEjecucionRequisitos" class="table table-bordered" width="100%" cellspacing="0">
								            <thead> 
								                <tr> 
								                    <th>#</th> 
								                    <th>Requisitos que se incumplen: Seleccionar</th> 
								                    <th>Las causas o el porque</th> 
								                    <th>Acciones tomadas</th> 
								                    <th>Eliminar</th>
								                </tr> 
								            </thead>
								            <tfoot>
												<tr> 
								                    <th>#</th> 
								                    <th>Requisitos que se incumplen: Seleccionar</th> 
								                    <th>Las causas o el porque</th> 
								                    <th>Acciones tomadas</th> 
								                    <th>Eliminar</th>
								                </tr> 
											</tfoot>
								            <tbody id="tdEjecucionRequisitos">
								            </tbody>
								        </table>
								    </div>
								</div>

								<div class="col-xs-12 col-sm-12">
									<hr>
								</div>	


								<div class="col-xs-12 col-sm-12">
									<div class="table-responsive">
										<table id="tbl_datosEjecucionEncuesta" class="table table-bordered" width="100%" cellspacing="0">
								            <thead> 
								                <tr> 
								                	<th>Puntualidad </th>
													<th>Divulgación</th>
													<th>Escenario y montaje</th>
													<th>Cumplimiento del Objetivo</th>
													<th>Variedad y Creatividad</th>
													<th>Imagen Institucional</th>
													<th>Seguridad</th>
								                </tr> 
								            </thead>
								            <tfoot>
												<tr> 
								                    <th>Puntualidad </th>
													<th>Divulgación</th>
													<th>Escenario y montaje</th>
													<th>Cumplimiento del Objetivo</th>
													<th>Variedad y Creatividad</th>
													<th>Imagen Institucional</th>
													<th>Seguridad</th>
								                </tr> 
											</tfoot>
								            <tbody id="tdEjecucionEncuesta">
								            </tbody>
								        </table>
								    </div>
								</div>

								<div class="col-xs-12 col-sm-12">
									<h4>AGREGAR OBSERVACIONES:</h4>
								</div>
								<div class="col-xs-12 col-sm-12">
									<div class="form-group">
									    <label for="exampleFormControlTextarea1">Observación:</label>
									    <input type="hidden" name="id_act_obs" id="id_act_obs">
									    <textarea class="form-control" id="mensajeObser" rows="3"></textarea>
									    
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 text-right">
									<button type="button" class="btn btn-primary btn-xs" id="agregarObservacion">Agregar observación</button>
								</div>


							</div>

						</div>
				
						<div class="modal-footer" >
							<div class="row">
								<div class="col-xs-12 col-sm-12" style="text-align: left;">
									<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<!-- MODAL EJECUCION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalEjecucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h3 class="modal-title text-success text-center" id="myModalLabel">Ejecuciòn de la actividad<br>
									<b>ID:</b> <label id="id_actividadEjecucion"></label>
								</h3>
							</div>
						
							
							<div class="modal-body">	
								<div class="row">
									<form id="form_agregar_poblacion">
										<div class="col-xs-8 col-sm-8">
											<div class="form-group">
												<label>Institución, grupo o comunidad:</label>
												<input  class="form-control" type="text" name="insGrupoComu_ejecucion" id="insGrupoComu_ejecucion" >
												<input type="hidden" name="id_actividad" id="id_actividad">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Entidad:</label>
												<select class="form-control" data-live-search="true" name="entidad_ejecucion" id="entidad_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($entidades as $entidad)
														<option value="{{$entidad['i_pk_id']}}">{{strtoupper($entidad['vc_entidad'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12"> </div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Localidad:</label>
												<select class="form-control" data-live-search="true" name="localidad_ejecucion" id="localidad_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($Localidades as $localidad)
														<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Upz:</label>
												<select class="form-control" data-live-search="true" name="Id_Upz_ejecucion" id="Id_Upz_ejecucion">
													<option value="">Seleccionar</option>
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Barrio:</label>
												<select class="form-control" data-live-search="true" name="Id_Barrio_ejecucion" id="Id_Barrio_ejecucion">
													<option value="">Seleccionar</option>
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12"> </div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Tipo:</label>
												<select class="form-control" data-live-search="true" name="tipoPoblacion_ejecucion" id="tipoPoblacion_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($tipoPoblaciones as $tipoPoblacion)
														<option value="{{$tipoPoblacion['i_pk_id']}}">{{strtoupper($tipoPoblacion['vc_tipo'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Condición:</label>
												<select class="form-control" data-live-search="true" name="condicion_ejecucion" id="condicion_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($condiciones as $condicion)
														<option value="{{$condicion['i_pk_id']}}">{{strtoupper($condicion['vc_condicion'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Situación:</label>
												<select class="form-control" data-live-search="true" name="situacion_ejecucion" id="situacion_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($situaciones as $situacion)
														<option value="{{$situacion['i_pk_id']}}">{{strtoupper($situacion['vc_situacion'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12"></div>

										<div class="col-xs-2 col-sm-2">
											
											<div class="form-group">
												<label>0 a 5</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text"
												  name="i_0a5_f" id="i_0a5_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>

											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_0a5_m" id="i_0a5_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>

										</div>

										<div class="col-xs-2 col-sm-2">
											<div class="form-group">
												<label>6 a 12</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text" name="i_6a12_f" id="i_6a12_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>
												
											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_6a12_m" id="i_6a12_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>
										</div>

										<div class="col-xs-2 col-sm-2">
											<div class="form-group">
												<label>13 a 17</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text" name="i_13a17_f" id="i_13a17_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>
												
											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_13a17_m" id="i_13a17_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>
										</div>

										<div class="col-xs-2 col-sm-2">
											<div class="form-group">
												<label>18 a 26</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text" name="i_18a26_f" id="i_18a26_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>	
											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_18a26_m" id="i_18a26_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>
										</div>

										<div class="col-xs-2 col-sm-2">
											<div class="form-group">
												<label>27 a 59</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text" name="i_27a59_f" id="i_27a59_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>
											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_27a59_m" id="i_27a59_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>
										</div>

										<div class="col-xs-2 col-sm-2">
											<div class="form-group">
												<label>60 a mas</label>
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">F</span>
												  </div>
												  <input type="text" name="i_60_f" id="i_60_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
												</div>
											</div>

											<div class="form-group">
												<div class="input-group mb-3">
												  <div class="input-group-prepend">
												    <span class="input-group-text" id="basic-addon1">M</span>
												  </div>
												  <input type="text" name="i_60_m" id="i_60_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
												</div>
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-12">
											<div id="mensajeEjecucion"></div>
										</div>

										<div class="col-xs-12 col-sm-12" style="text-align: right;">
											<button type="button" class="btn btn-success btn-sm" id="id_agregar_poblacion">Agregar</button>
										</div>	
									</form>
								</div>	

								<div class="row">
									<hr>
									<form id="form_agregar_requisitos">
										<label>REPORTE DE NOVEDADES:</label>
										<br>
										<div class="col-xs-12 col-sm-12"></div>
										
										<div class="col-xs-12 col-sm-12">
											<div class="form-group">
												<label>Requisitos que se incumplen:</label>
												<input type="hidden" name="id_actividad1" id="id_actividad1">
												<select class="form-control" data-live-search="true" name="requisito_ejecucion" id="requisito_ejecucion">
													<option value="">Seleccionar</option>
													@foreach($requisitos as $requisito)
														<option value="{{$requisito['i_pk_id']}}">{{strtoupper($requisito['vc_requisito'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12">
											<div class="form-group">
											  <label for="comment">Las causas o el porque:</label>
											  <textarea class="form-control" rows="5" name="causa_ejecucion" id="causa_ejecucion"></textarea>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12">
											<div class="form-group">
											  <label for="comment">Acciones tomadas:</label>
											  <textarea class="form-control" rows="5" name="accion_ejecucion" id="accion_ejecucion"></textarea>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12">
											<div id="mensajeRequisito"></div>
										</div>

										<div class="col-xs-12 col-sm-12" style="text-align: right;">
											<button type="button" class="btn btn-success btn-sm" id="id_agregar_requisito">Agregar</button>
										</div>	
									</form>
								</div>


								<div class="row">
									<hr>
									<form id="form_agregar_encuesta">
										<label>CALIFICACIÓN DEL SERVICIO:</label>
										<br>

										<div class="col-xs-12 col-sm-12"></div>
										
										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
												<label>Puntualidad:</label>
												<input type="hidden" name="id_actividad2" id="id_actividad2">
												<select class="form-control" data-live-search="true" name="puntualidad_ejecucion" id="puntualidad_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>Divulgación:</label>
												<select class="form-control" data-live-search="true" name="divulgacion_ejecucion" id="divulgacion_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>escenario y montaje:</label>
												<select class="form-control" data-live-search="true" name="escenario_ejecucion" id="escenario_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>Cumplimiento del Objetivo:</label>
												<select class="form-control" data-live-search="true" name="cumplimiento_ejecucion" id="cumplimiento_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-12 col-sm-12"></div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>Variedad y Creatividad:</label>
												<select class="form-control" data-live-search="true" name="veriedad_ejecucion" id="veriedad_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>Imagen Institucional:</label>
												<select class="form-control" data-live-search="true" name="imagen_ejecucion" id="imagen_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3">
											<div class="form-group">
											    <label>Seguridad:</label>
												<select class="form-control" data-live-search="true" name="seguridad_ejecucion" id="seguridad_ejecucion">
													<option value="">Seleccionar</option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>

										<div class="col-xs-3 col-sm-3" style="text-align: right;">
											<br><br>
											<button type="button" class="btn btn-success btn-sm" id="id_agregar_encuesta">Registro</button>
										</div>	

										<div class="col-xs-12 col-sm-12">
											<div id="mensajeEncuesta"></div>
										</div>
										<div class="col-xs-12 col-sm-12"></div>
									</form>
								</div>

							</div>

							<div class="modal-footer" >
								<div class="row">
									<div class="col-xs-12 col-sm-12" style="text-align: left;">
										<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
									</div>
								</div>
							</div>

						

					</div>
				</div>
			</div>
    
</div>

@stop


