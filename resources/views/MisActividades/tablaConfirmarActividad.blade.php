@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/MisActividad/tablaConfirmarActividad.js?v=12') }}"></script>	
	@stop

@section('content') 
<?php use App\Modulos\Configuracion\Configuracion; ?>
<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('confirmarActividades') }}" ></div>
	<div id="main" class="row" data-url="{{ url('personas') }}" data-url-parques="{{ url('parques') }}"></div>
		

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				<div class="col-md-12">
					<h4 ><b>TABLA CONFIRMAR ACTIVIDADES</b><br><span class="glyphicon glyphicon-th-list text-{{$color}} " > <label class="text-{{$color}} ">{{$tipo}} </label></span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>
				<div class="col-md-12">

					<div class="table-responsive">
					<table id="tbl_confirmarActividad" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
		            <thead> 
		                <tr style="{{$style}}"> 
		                    <th>#</th> 
		                    <th>Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th>
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th >Programación</th>
		                    <th >Estado</th>
		                    
		                </tr> 
		            </thead>
		            <tfoot>
						<tr>
							<th>#</th> 
		                    <th scope="row" class="text-center" style="width:auto;">Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th> 
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th >Programación</th>
		                    <th >Estado</th>
						</tr>
					</tfoot>
		            <tbody>
		            <?php $num=1;?>
		            @if($actividades!='')
		                @foreach ($actividades as $actividad) 
		                    	<tr class="something" data-row="{{ $actividad['i_pk_id'] }}">
			                        <td class="col-md-1">{{$num}}</td>
			                        <td class="col-md-13"><b><p class="text-info text-center" style="font-size: 15px">{{$actividad['i_pk_id']}} </p></b></td>
			                        <td >{{$actividad['d_fechaEjecucion']}}</td>
			                        <td>{{$actividad['t_horaInicio']}}</td>
			                        <td>{{$actividad['t_horaFin']}}</td>
			                        <td>{{$actividad->datosActividad[0]->programa['programa']}}</td>
			                        <td>{{$actividad->datosActividad[0]->actividad['actividad']}}</td>
			                        <td>
			                        	<label>Tematicas:</label><br>
			                        	@foreach ($actividad->datosActividad as $datoTC) 
			                        		<ul>
											 	<li>{{$datoTC->tematica['tematica']}}</li>
											</ul>
			                        	@endforeach
			                        	<label>Componente:</label>
			                        	@foreach ($actividad->datosActividad as $datoTC) 
			                        		<ul>
											  	<li>{{$datoTC->componente['componente']}}</li>
											</ul>
			                        	@endforeach
			                        </td>
			                        <td>{{$actividad->responsable['Primer_Apellido']}} {{$actividad->responsable['Segundo_Apellido']}}<br>{{$actividad->responsable['Primer_Nombre']}}</td>
			                        <td>
			                        	<label>Responsable:</label><br>
			                        	{{$actividad->gestor['Primer_Apellido']}} {{$actividad->gestor['Segundo_Apellido']}}<br>{{$actividad->gestor['Primer_Nombre']}}
			                        	<br><label>Acompantes:</label><br>
			                        	@foreach ($actividad->acompanates as $acompanante) 
			                        		<ul>
											 	<li>{{$acompanante->usuario['Primer_Apellido']}} {{$acompanante->usuario['Segundo_Apellido']}} {{$acompanante->usuario['Primer_Nombre']}}</li>
											</ul>
			                        	@endforeach
			                        </td>
			                        <th class="text-center">
			                        	<ul class="list-group">
										  <li class="list-group-item"><button type="button" class="btn btn-link btn-xs" data-rel="{{$actividad['i_pk_id']}}" data-funcion="programacion" data-toggle="modal" data-target="#modalProgramacion"><span class="glyphicon glyphicon-eye-open"></span> Ver programaciòn </button></li>
										</ul>
										<?php

											$carbon = new \Carbon\Carbon();
											$date = $carbon->createFromFormat('Y-m-d H:i:s', $actividad['d_fechaEjecucion'].' '.$actividad['t_horaInicio']);
											$endDate1 = $date->subDay(App\Modulos\Configuracion\Configuracion::DIA_ANTEPENULTIMO_CONFIRMACION);
											$fechaInicio = $endDate1->format('Y-m-d H:i:s');

											$date = $carbon->createFromFormat('Y-m-d H:i:s', $actividad['d_fechaEjecucion'].' '.$actividad['t_horaInicio']);
											$endDate2 = $date->subDay(App\Modulos\Configuracion\Configuracion::DIA_ULTIMO_PROGRAMACION);
											$fechaLimite = $endDate2->format('Y-m-d H:i:s');
										?>

										<?php 
											if($carbon->between($endDate1, $endDate2) && $tipo!="Todas las actividades.")
											{
										?>
											<div class="btn-group">
											    <button type="button" class="btn btn-success btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-val="0"><span class="glyphicon glyphicon-hand-right"></span></button>
											    <button type="button" class="btn btn-danger btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-val="1"><span class="glyphicon glyphicon-thumbs-down"></span></button>
											    <button type="button" class="btn btn-default btn-xs tama" data-rel="{{$actividad['i_pk_id']}}" data-funcion="aprobar" data-toggle="modal" data-target="#modalReProgramacion" data-val="2"><span class="glyphicon glyphicon-dashboard"></span> </button>
											</div>											
										<?php 
											}
										?>
											<br>
											<h5>Fechas de Confirmación:</h5>
											<p>{{$fechaInicio}} <br>al<br> {{$fechaLimite}}</p>
										<div id="{{$actividad['i_pk_id']}}mensajeConfirmacion"></div>
										<div id="{{$actividad['i_pk_id']}}mensaje"></div>
			                        </th>
			                        <th>
			                        	<?php
			                        		$tabla='Programación:';
			                        		switch($actividad['i_estadoAprobaPrograma']){

				                        		case Configuracion::INCOMPLETA:
				                        			$tabla=$tabla.' <span class="label label-danger"> Incompleta</span>
				                        							<div id="'.$actividad['i_pk_id'].'mensaje"></div>';
				                        		break;

				                        		case Configuracion::PENDIENTE:
				                        			$tabla=$tabla.'<span class="label label-info"><b>Pendiente de revisión</span>';
				                        		break;

				                        		case Configuracion::APROBADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b></b> Aprobada</span>';
				                        		break;

				                        		case Configuracion::DEVUELTO:
				                        			$tabla=$tabla.' <span class="label label-warning"><b></b> Denegada</span>
				                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
				                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
					                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
					                        							<button type="submit" class="btn btn-link btn-xs"   >
																			<span class="glyphicon glyphicon-edit"></span>  Editar
																		</button>
																	</form>';
																	
				                        					
				                        		break;

				                        		case Configuracion::CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Confirmación:';
				                        	switch($actividad['i_estadoConfirmaPrograma']){
				                        		case Configuracion::CONFIRMADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b><b>Programación:</b> Confirmada</span>';
				                        		break;

				                        		case Configuracion::CONFIRMADO_CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
	  
				                        		case Configuracion::CONFIRMADO_REPROGRAMADA:
				                        			$tabla=$tabla.'<span class="label label-success">Confirmada/ Reprogramada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}


				                        	$tabla=$tabla.'<br><br>Ejecución:';
				                        	switch($actividad['i_estadoRegistroEjecucion']){
				                        		case Configuracion::EJECUTADA:
				                        			$tabla=$tabla.'<span class="label label-success">Registrada.</span>';
				                        		break;

				                        		case Configuracion::EJECUTADA_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada al registrar.</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Estado ejecución:';
				                        	switch($actividad['i_estadoAprobaEjecucion']){
				                        		case Configuracion::EJECUCION_APROBADA:
				                        			$tabla=$tabla.'<span class="label label-success"> Aprobada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger"> Cancelada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_DENEGADA:
				                        			$tabla=$tabla.'<span class="label label-warning"> Denegada</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;

				                        	}
				                        	echo $tabla;
			                        	?>
			                        </th>
			                    </tr>
		                    <?php $num++; ?>
		                @endforeach
		            @endIf
		            </tbody>
		        </table>
		    	</div>

				</div>

				<div class="col-md-12">
					<div class="form-group">
					  <label for="comment">Observaciones:</label>
					  <textarea class="form-control" rows="5" id="tx_observaciones" id="tx_observaciones"></textarea>
					</div>
				</div>
			</form>
		</div>



			<!-- MODAL PROGRAMACION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalProgramacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-header">

							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-primary text-center" id="myModalLabel">Programación de la actividad<br>
							<b>ID:</b> <label id="id_actividadProgramacion"></label></h3>
						</div>
						<form id="form_agregar_estudio_comveniencia">
							<div id="cargando"></div>
							<div class="modal-body" id="modalBody">		
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA COMUNIDAD:</h4>
									</div>
																		
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioP"></p>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Institución, Grupo, Comunidad:</label>
										<p id="modalinstitucionGrupoCP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Características de la Población:</label>
										<p id="modalCaracteristicasP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Específico:</label>
										<p id="modalCaracEspecificasP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Programa
													</th>
													<th>
														Actividad
													</th>
													<th>
														Temática
													</th>
													<th>
														Componente
													</th>
												</tr>
											</thead>
											<tbody id="datosModalActividad">
											</tbody>
										</table>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>PROGRAMACIÓN Y ASIGNACIÓN DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-3 col-sm-3">
										<label>Responsable:</label>
										<p id="modalResponsableP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Fecha ejecución:</label>
										<p id="modalFechaEjecucionP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora inicio:</label>
										<p id="modalHoraInicioP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora fin:</label>
										<p id="modalHoraFinP"></p>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DEL ESCENARIO:</h4>
									</div>

									<div class="col-xs-4 col-sm-4">
										<label>Dirección:</label>
										<p id="modalDireccionEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Escenario:</label>
										<p id="modalEscenarioEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Codigo IDRD:</label>
										<p id="modalCodigoIP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioEP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>ASPECTOS A TENER EN CUENTA:</h4>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Hora de implementación:</label>
										<p id="horaImplementacion"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Punto de encuentro:</label>
										<p id="puntoEncuentro"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Nombre de la persona de contacto:</label>
										<p id="nombreContacto"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Rol en la comunidad:</label>
										<p id="rollComunidad"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Telefono:</label>
										<p id="telefono"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>OBSERVACIONES:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
											<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Usuario
													</th>
													<th>
														Observación
													</th>
													<th>
														Tipo
													</th>
													<th>
														Fecha
													</th>
												</tr>
											</thead>
											<tbody id="ObservacionActividad">
											</tbody>
											</table>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>AGREGAR OBSERVACIONES:</h4>
									</div>
									<div class="col-xs-12 col-sm-12">
										<div class="form-group">
										    <label for="exampleFormControlTextarea1">Observación:</label>
										    <input type="hidden" name="id_act_obs" id="id_act_obs">
										    <textarea class="form-control" id="mensajeObser" rows="3"></textarea>
										    
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 text-right">
										<button type="button" class="btn btn-primary btn-xs" id="agregarObservacion">Agregar observación</button>
									</div>



								</div>					
							</div>

							<div class="modal-footer" >
								<div class="row">
									<div class="col-xs-12 col-sm-12" style="text-align: left;">
										<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
									</div>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>









			<!-- MODAL REPROGRAMACION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalReProgramacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-header">

							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-primary text-center" id="myModalLabel">Re-Programación de la actividad<br>
							<b>ID:</b> <label id="id_actividadReProgramacion"></label></h3>
						</div>
						<form id="form_agregar_reprogramacion">
							
								<div id="cargando"></div>
								<div class="modal-body" id="modalBody">		
									<div class="row">
										
										<div class="col-xs-12 col-sm-12">
											<h4>PROGRAMACIÓN Y ASIGNACIÓN DE LA ACTIVIDAD:</h4>
										</div>

										<div class="col-xs-4 col-sm-4">
											<label>Fecha registro:</label>
											<p id="modalReprogramacionFechaRegistro"></p>
											<input  type="hidden" name="fechaRegistoReProgramacion" id="fechaRegistoReProgramacion">
										</div>

										<div class="col-xs-12 col-sm-12">
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Fecha ejecución:</label>
												<p id="modalReprogramacionFechaEjecucionP"></p>
												<input  class="form-control" type="text" name="fechaEjecucionReprogramacion" id="fechaEjecucionReprogramacion">
												<input  type="hidden" name="idActividadReProgramacion" id="idActividadReProgramacion">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Hora inicio:</label>
												<p id="modalReprogramacionHoraInicioP"></p>
												<input  class="form-control" type="text" name="horaInicioReprogramacion" id="horaInicioReprogramacion">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Hora fin:</label>
												<p id="modalReprogramacionHoraFinP"></p>
												<input  class="form-control" type="text" name="horaFinReprogramacion" id="horaFinReprogramacion">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Hora implementación:</label>
												<p id="modalReprogramacionHorahoraImple"></p>
												<input  class="form-control" type="text" name="horaImpleReprogramacion" id="horaImpleReprogramacion">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Punto de encuentro:</label>
												<p id="modalpuntoEncuprogramacion"></p>
												<input  class="form-control" type="text" name="puntoEncuprogramacion" id="puntoEncuprogramacion">
											</div>
										</div>

										<div class="col-xs-12 col-sm-12">
											<h4>DATOS DEL ESCENARIO:</h4>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Dirección:</label>
												<p id="modalReprogramacionDireccionEP"></p>
												<input  class="form-control" type="text" name="direccionReprogramacion" id="direccionReprogramacion">
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Escenario:</label>
												<p id="modalReprogramacionEscenarioEP"></p>
												<input  class="form-control" type="text" name="escenarioReprogramacion" id="escenarioReprogramacion">
											</div>	
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Codigo IDRD:</label>
												<p id="modalReprogramacionCodigoIP"></p>
												<input  class="form-control" type="text" name="codigoReprogramacion" id="codigoReprogramacion">
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-12"> </div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Localidad:</label>
												<p id="modalLocalidadEProgramacion"></p>
												<select class="form-control" data-live-search="true" name="localidad_reprogramacion" id="localidad_reprogramacion">
													<option value="">Seleccionar</option>
													@foreach($Localidades as $localidad)
														<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>	
													@endforeach
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Upz:</label>
												<p id="modalUpzEProgramacion"></p>
												<select class="form-control" data-live-search="true" name="Id_Upz_Reprogramacion" id="Id_Upz_Reprogramacion">
													<option value="">Seleccionar</option>
												</select>
											</div>
										</div>

										<div class="col-xs-4 col-sm-4">
											<div class="form-group">
												<label>Barrio:</label>
												<p id="modalBarrioEProgramacion"></p>
												<select class="form-control" data-live-search="true" name="Id_Barrio_Reprogramacion" id="Id_Barrio_Reprogramacion">
													<option value="">Seleccionar</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12">
											<div id="mensajeReprogramacion"></div>
										</div>

									</div>					
								</div>

								<div class="modal-footer" >
									<div class="row">
										<div class="col-xs-12 col-sm-12" style="text-align: left;">
											<button type="button" class="btn btn-primary" id="btn_reprogramar">REPROGRAMAR</button>
											<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
										</div>
									</div>
								</div>

						</form>

					</div>
				</div>
			</div>

 
    
</div>

@stop


