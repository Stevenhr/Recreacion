@extends('master')                              

	@section('script')
		@parent
		<script src="{{ asset('public/Js/MisActividad/actividadHabilitada.js') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('habbilitacionActividades') }}" ></div>
	

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				
				
				<div class="col-md-12">
					<h4><b>ACTIVIDADES HABILITADAS</b><br><span class="glyphicon glyphicon-user"> Gestor y responsable</span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>
	 			
	 			<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Tipo:</label>
			                <select class="form-control" data-live-search="true" name="Tipoactividad" id="Tipoactividad">
								<option value="-1">Seleccionar</option>
								<option value="2">Programación</option>
								<option value="3">Ejecución</option>
							</select>
			            </div>
				</div>


				<div class="col-md-12"></div>
				
				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_Actividades_habilitadas"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			</form>
		</div>


		<div class="row">
			
			<div class="col-md-12">
				<br>
				<h3>RESULTADO:</h3>
				<br>
			</div>

			<div class="col-md-12">
				<div  id="resultadoBusqueda"></div>
			</div>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target=".bd-example-modal-lg">
					<span class="glyphicon glyphicon-align-justify"></span>  Convención de estados
				</button>
				
				<br><br>
			</div>
			<div class="col-md-12" >
				<div class="table-responsive">
				<table id="tbl_confirmarActividad" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
		            <thead> 
		                <tr> 
		                    <th>#</th> 
		                    <th>Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th>
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th>Estado</th>
		                    <th >Programación</th>
		                    <th >Ejecución</th>
		                </tr> 
		            </thead>
		            <tfoot>
						<tr>
							<th>#</th> 
		                    <th scope="row" class="text-center" style="width:auto;">Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th> 
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th>Estado</th>
		                    <th >Programación</th>
		                    <th >Ejecución</th>
						</tr>
					</tfoot>
		            <tbody id="tablaCreadas">
		            </tbody>
		        </table>
		    	</div>
			</div>
			
		</div>

		<div class="row">
			
			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			      	<div class="row">
			      		
			      		<div class="col-md-1"></div>
			      		<div class="col-md-10">
			      			<br><br>
			      			<h4>TABLA DE CONVENCIONES</h4>
			      		</div>
			      		<div class="col-md-12"></div>

			      		<div class="col-md-1"></div>
				      	<div class="col-md-10">
					      	@include('estados')
						</div>
						<div class="col-md-1"></div>
					</div>
			    </div>
			  </div>
			</div>

		</div>

		<div class="row">
			<br><br>
		</div>

    </div>
</div>




<!-- MODAL EJECUCION-->
	<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalEjecucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title text-success text-center" id="myModalLabel">Ejecuciòn de la actividad<br>
							<b>ID:</b> <label id="id_actividadEjecucion"></label>
						</h3>
					</div>
				
					
					<div class="modal-body">	
						<div class="row">
							<form id="form_agregar_poblacion">
								<div class="col-xs-8 col-sm-8">
									<div class="form-group">
										<label>Institución, grupo o comunidad:</label>
										<input  class="form-control" type="text" name="insGrupoComu_ejecucion" id="insGrupoComu_ejecucion">
										<input type="hidden" name="id_actividad" id="id_actividad">
									</div>
								</div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Entidad:</label>
										<select class="form-control" data-live-search="true" name="entidad_ejecucion" id="entidad_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($entidades as $entidad)
												<option value="{{$entidad['i_pk_id']}}">{{strtoupper($entidad['vc_entidad'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12"> </div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Localidad:</label>
										<select class="form-control" data-live-search="true" name="localidad_ejecucion" id="localidad_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($Localidades as $localidad)
												<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Upz:</label>
										<select class="form-control" data-live-search="true" name="Id_Upz_ejecucion" id="Id_Upz_ejecucion">
											<option value="">Seleccionar</option>
										</select>
									</div>
								</div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Barrio:</label>
										<select class="form-control" data-live-search="true" name="Id_Barrio_ejecucion" id="Id_Barrio_ejecucion">
											<option value="">Seleccionar</option>
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12"> </div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Tipo:</label>
										<select class="form-control" data-live-search="true" name="tipoPoblacion_ejecucion" id="tipoPoblacion_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($tipoPoblaciones as $tipoPoblacion)
												<option value="{{$tipoPoblacion['i_pk_id']}}">{{strtoupper($tipoPoblacion['vc_tipo'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Condición:</label>
										<select class="form-control" data-live-search="true" name="condicion_ejecucion" id="condicion_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($condiciones as $condicion)
												<option value="{{$condicion['i_pk_id']}}">{{strtoupper($condicion['vc_condicion'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-4 col-sm-4">
									<div class="form-group">
										<label>Situación:</label>
										<select class="form-control" data-live-search="true" name="situacion_ejecucion" id="situacion_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($situaciones as $situacion)
												<option value="{{$situacion['i_pk_id']}}">{{strtoupper($situacion['vc_situacion'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12"></div>

								<div class="col-xs-2 col-sm-2">
									
									<div class="form-group">
										<label>0 a 5</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text"
										  name="i_0a5_f" id="i_0a5_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_0a5_m" id="i_0a5_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>

								</div>

								<div class="col-xs-2 col-sm-2">
									<div class="form-group">
										<label>6 a 12</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text" name="i_6a12_f" id="i_6a12_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>
										
									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_6a12_m" id="i_6a12_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>
								</div>

								<div class="col-xs-2 col-sm-2">
									<div class="form-group">
										<label>13 a 17</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text" name="i_13a17_f" id="i_13a17_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>
										
									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_13a17_m" id="i_13a17_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>
								</div>

								<div class="col-xs-2 col-sm-2">
									<div class="form-group">
										<label>18 a 26</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text" name="i_18a26_f" id="i_18a26_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>	
									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_18a26_m" id="i_18a26_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>
								</div>

								<div class="col-xs-2 col-sm-2">
									<div class="form-group">
										<label>27 a 59</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text" name="i_27a59_f" id="i_27a59_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_27a59_m" id="i_27a59_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>
								</div>

								<div class="col-xs-2 col-sm-2">
									<div class="form-group">
										<label>60 a mas</label>
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">F</span>
										  </div>
										  <input type="text" name="i_60_f" id="i_60_f" class="form-control" value="0" aria-label="Femenino" aria-describedby="basic-addon1">
										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">M</span>
										  </div>
										  <input type="text" name="i_60_m" id="i_60_m" class="form-control" value="0" aria-label="Masculino" aria-describedby="basic-addon1">
										</div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-12">
									<div id="mensajeEjecucion"></div>
								</div>

								<div class="col-xs-12 col-sm-12" style="text-align: right;">
									<button type="button" class="btn btn-success btn-sm" id="id_agregar_poblacion">Agregar</button>
								</div>	
							</form>
						</div>	

						<div class="row">
							<hr>
							<form id="form_agregar_requisitos">
								<label>REPORTE DE NOVEDADES:</label>
								<br>
								<div class="col-xs-12 col-sm-12"></div>
								
								<div class="col-xs-12 col-sm-12">
									<div class="form-group">
										<label>Requisitos que se incumplen:</label>
										<input type="hidden" name="id_actividad1" id="id_actividad1">
										<select class="form-control" data-live-search="true" name="requisito_ejecucion" id="requisito_ejecucion">
											<option value="">Seleccionar</option>
											@foreach($requisitos as $requisito)
												<option value="{{$requisito['i_pk_id']}}">{{strtoupper($requisito['vc_requisito'])}}</option>	
											@endforeach
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12">
									<div class="form-group">
									  <label for="comment">Las causas o el porque:</label>
									  <textarea class="form-control" rows="5" name="causa_ejecucion" id="causa_ejecucion"></textarea>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12">
									<div class="form-group">
									  <label for="comment">Acciones tomadas:</label>
									  <textarea class="form-control" rows="5" name="accion_ejecucion" id="accion_ejecucion"></textarea>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12">
									<div id="mensajeRequisito"></div>
								</div>

								<div class="col-xs-12 col-sm-12" style="text-align: right;">
									<button type="button" class="btn btn-success btn-sm" id="id_agregar_requisito">Agregar</button>
								</div>	
							</form>
						</div>


						<div class="row">
							<hr>
							<form id="form_agregar_encuesta">
								<label>CALIFICACIÓN DEL SERVICIO:</label>
								<br>

								<div class="col-xs-12 col-sm-12"></div>
								
								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
										<label>Puntualidad:</label>
										<input type="hidden" name="id_actividad2" id="id_actividad2">
										<select class="form-control" data-live-search="true" name="puntualidad_ejecucion" id="puntualidad_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>Divulgación:</label>
										<select class="form-control" data-live-search="true" name="divulgacion_ejecucion" id="divulgacion_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>escenario y montaje:</label>
										<select class="form-control" data-live-search="true" name="escenario_ejecucion" id="escenario_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>Cumplimiento del Objetivo:</label>
										<select class="form-control" data-live-search="true" name="cumplimiento_ejecucion" id="cumplimiento_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12"></div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>Variedad y Creatividad:</label>
										<select class="form-control" data-live-search="true" name="veriedad_ejecucion" id="veriedad_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>Imagen Institucional:</label>
										<select class="form-control" data-live-search="true" name="imagen_ejecucion" id="imagen_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3">
									<div class="form-group">
									    <label>Seguridad:</label>
										<select class="form-control" data-live-search="true" name="seguridad_ejecucion" id="seguridad_ejecucion">
											<option value="">Seleccionar</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
										</select>
									</div>
								</div>

								<div class="col-xs-3 col-sm-3" style="text-align: right;">
									<br><br>
									<button type="button" class="btn btn-success btn-sm" id="id_agregar_encuesta">Registro</button>
								</div>	

								<div class="col-xs-12 col-sm-12">
									<div id="mensajeEncuesta"></div>
								</div>
								<div class="col-xs-12 col-sm-12"></div>
							</form>
						</div>

					</div>

					<div class="modal-footer" >
						<div class="row">
							<div class="col-xs-12 col-sm-12" style="text-align: left;">
								<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
							</div>
						</div>
					</div>

				

			</div>
		</div>
	</div>




	<!-- MODAL EJECUCION-->
	<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalDatosEjecucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title text-success text-center" id="myModalLabel">Datos registrados en la ejecución de la actividad<br>
					<b>ID:</b> <label id="id_actividadDatosEjecucion"></label></h3>
				</div>
				
				<div class="modal-body">	
					<div class="row">
						
						<div class="col-xs-12 col-sm-12">
							<div class="table-responsive">
								<table id="tbl_datosEjecucionActividad" class="table table-responsive table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
						            <thead> 
						                <tr style="" > 
						                    <th rowspan="2">#</th> 
						                    <th rowspan="2">Institución, grupo o comunidad:</th> 
						                    <th rowspan="2">Entidad</th> 
						                    <th rowspan="2">Localidad</th> 
						                    <th rowspan="2">Upz</th> 
						                    <th rowspan="2">Barrio</th>  
						                    <th rowspan="2">Tipo</th> 
						                    <th rowspan="2">Condición</th>
						                    <th rowspan="2">Situación</th>
						                    <th colspan="2">0 a 5</th>
						                    <th colspan="2">6 a 12</th>
						                    <th colspan="2">13 a 17</th>
						                    <th colspan="2">18 a 26</th>
						                    <th colspan="2">27 a 59</th>
						                    <th colspan="2">60 a mas</th>
						                    <th >Total</th>
						                    <th >Eliminar</th>
						                </tr> 
						                <tr style=""> 
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>Total</th>
						                    <th>Eliminar</th>
						                </tr> 
						            </thead>
						            <tfoot>
										<tr style=""> 
											<th rowspan="2">#</th> 
						                    <th rowspan="2">Institución, grupo o comunidad:</th> 
						                    <th rowspan="2">Entidad</th> 
						                    <th rowspan="2">Localidad</th> 
						                    <th rowspan="2">Upz</th> 
						                    <th rowspan="2">Barrio</th>  
						                    <th rowspan="2">Tipo</th> 
						                    <th rowspan="2">Condición</th>
						                    <th rowspan="2">Situación</th>

						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>M</th>
						                    <th>F</th>
						                    <th>Eliminar</th>
						                </tr> 
										<tr style=""> 
						                    <th colspan="2">0 a 5</th>
						                    <th colspan="2">6 a 12</th>
						                    <th colspan="2">13 a 17</th>
						                    <th colspan="2">18 a 26</th>
						                    <th colspan="2">27 a 59</th>
						                    <th colspan="2">60 a mas</th>
						                    <th>F</th>
						                    <th>Eliminar</th>
						                </tr> 
									</tfoot>
						            <tbody id="tdEjecucion">
						            </tbody>
						        </table>
					        </div>
						</div>
					
					
						<div class="col-xs-12 col-sm-12">
							<hr>
						</div>	

						<div class="col-xs-12 col-sm-12">
							<div class="table-responsive">
								<table id="tbl_datosEjecucionRequisitos" class="table table-bordered" width="100%" cellspacing="0">
						            <thead> 
						                <tr> 
						                    <th>#</th> 
						                    <th>Requisitos que se incumplen: Seleccionar</th> 
						                    <th>Las causas o el porque</th> 
						                    <th>Acciones tomadas</th> 
						                    <th>Eliminar</th>
						                </tr> 
						            </thead>
						            <tfoot>
										<tr> 
						                    <th>#</th> 
						                    <th>Requisitos que se incumplen: Seleccionar</th> 
						                    <th>Las causas o el porque</th> 
						                    <th>Acciones tomadas</th> 
						                    <th>Eliminar</th>
						                </tr> 
									</tfoot>
						            <tbody id="tdEjecucionRequisitos">
						            </tbody>
						        </table>
						    </div>
						</div>

						<div class="col-xs-12 col-sm-12">
							<hr>
						</div>	


						<div class="col-xs-12 col-sm-12">
							<div class="table-responsive">
								<table id="tbl_datosEjecucionEncuesta" class="table table-bordered" width="100%" cellspacing="0">
						            <thead> 
						                <tr> 
						                	<th>Puntualidad </th>
											<th>Divulgación</th>
											<th>Escenario y montaje</th>
											<th>Cumplimiento del Objetivo</th>
											<th>Variedad y Creatividad</th>
											<th>Imagen Institucional</th>
											<th>Seguridad</th>
						                </tr> 
						            </thead>
						            <tfoot>
										<tr> 
						                    <th>Puntualidad </th>
											<th>Divulgación</th>
											<th>Escenario y montaje</th>
											<th>Cumplimiento del Objetivo</th>
											<th>Variedad y Creatividad</th>
											<th>Imagen Institucional</th>
											<th>Seguridad</th>
						                </tr> 
									</tfoot>
						            <tbody id="tdEjecucionEncuesta">
						            </tbody>
						        </table>
						    </div>
						</div>
					</div>

				</div>

				<div class="modal-footer" >
					<div class="row">
						<div class="col-xs-12 col-sm-12" style="text-align: left;">
							<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

@stop

