@extends('master')                              

	@section('script')
		@parent
		<script src="{{ asset('public/Js/MisActividad/misgestionejecuciones.js') }}"></script>	
	@stop


@section('content') 
<?php use App\Modulos\Configuracion\Configuracion; ?>
<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('miGestionEjecucionGestor') }}" ></div>	

		<div class="row">
			<form method="POST" id="form_consulta_ejecucion_date">
				<div class="col-md-12">
					<h4><b>GESTIÓN DE EJECUCIONES REGISTRADAS</b><br><span class="glyphicon glyphicon-user"> GESTOR (Aprueba solo las ejecuciones registradas en las actividades creadas por el gestor)</span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>

	 			<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" autocomplete="off" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin" autocomplete="off"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>
				
				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Id:</label>
			                <input type='text' class="form-control" name="idAct" id="idAct" autocomplete="off" />
			            </div>
				</div>

				<div class="col-md-12"></div>
				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_buscar_Actividades"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			</form>
		</div>


		<div class="row" id="resultadoBusqueda" style="display: none;">
			<div class="col-md-1"></div>
			<div class="col-md-11">
				<br>
				<h3>RESULTADO:</h3>
				<br>
			</div>
			
			<div class="col-md-12"></div>


				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Por aprobar</h5>
					    <p class="card-text">Ejecuciones registradas por el responsable de la actividad, pendientes por aprobar.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'ejecucionesGestion','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{ Configuracion::EJECUTADA }}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="uno"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-3">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Aprobado</h5>
					    <p class="card-text">Ejecuciones registradas por el responsable de la actividad, revisadas y aprobadas.</p>
					   		
					  </div>
					  {{ Form::open(array('route' => 'ejecucionesGestion','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{ Configuracion::EJECUCION_APROBADA }}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-success badge2 btn-xs"><span class="badge" id="dos"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>

				

				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/denegada.png" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Denegada</h5>
					    <p class="card-text">Ejecuciones registradas por el responsable de la actividad, 
					 	 revisadas y denegadas.</p>
					  </div>
					 
					    	{{ Form::open(array('route' => 'ejecucionesGestion','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{ Configuracion::EJECUCION_DENEGADA }}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-warning badge2 btn-xs"><span class="badge" id="cuatro"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>


				<div class="col-md-3">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					  <div class="card-body">
					    <h5 class="card-title">Canceladas</h5>
					    <p class="card-text">Ejecuciones registradas por el responsable de la actividad, revisadas y canceladas.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'ejecucionesGestion','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{ Configuracion::EJECUCION_CANCELADA }}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="tres"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">  
	              <div class="card" style="width: 100%;">  
	                <img class="card-img-top" >  
	                <div class="card-body">  
	                  <h5 class="card-title">Todo</h5>
	                  <p class="card-text">Listado de todas las actividades registradas al responsable.</p>  
	                   
	                </div>  
	                 {{ Form::open(array('route' => 'ejecucionesGestion','target' => '_blank')) }}  
	                    <input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">  
	                    <input type="hidden" name="fechaFinHiden" class="fechaFinHiden">  
	                    <input type="hidden" name="idHiden" class="idHiden">  
	                    <input type="hidden" name="opcion" id="opcion" value="100">  
	                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>  
	                    <button type="submit" class="btn badge2 btn-xs"><span class="badge" id="todo"></span> Ir a actividades</button>  
	                    {{ Form::close() }}  
	              </div>  
	            </div>

		</div>
 
    </div>
</div>

@stop

