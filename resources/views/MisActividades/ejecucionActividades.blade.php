@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/MisActividad/ejecutarActividades.js') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('ejecutarActividades') }}" ></div>	

		<div class="row">
			<form method="POST" id="form_consulta_actividades_ejecutar">

				<div class="col-md-12">
					<h4><b>EJECUCIÓN DE ACTIVIDADES</b><br><span class="glyphicon glyphicon-user"> Responsable de actividad</span></h4>
					<br>
				</div>

				<div class="col-md-12"></div>

				
	 			<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio"  autocomplete="off"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin" autocomplete="off"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>
				
				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Id:</label>
			                <input type='text' class="form-control" name="idAct" id="idAct" autocomplete="off" />
			            </div>
				</div>

				<div class="col-md-12"></div>
				
				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_buscar_Actividades_Ejecucion"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			</form>
		</div>


		<div class="row" id="resultadoBusqueda" style="display: none;">
			<div class="col-md-2"></div>
			<div class="col-md-10">
				<br>
				<h3>RESULTADO:</h3>
				<br>
			</div>

			@php 
        		use App\Modulos\Configuracion\Configuracion; 
      		@endphp 

			<div class="col-md-12"></div>
				
				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>SIN APROBACIONES <br>POR EJECUTAR</h4>
					    <p class="card-text">Actividades que no han sido revisadas ni confirmadas.</p>
					  </div>
					  {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }}
					  			<br><br>
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::PENDIENTE}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="cero0"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>SIN CONFIRMACION <br>POR EJECUTAR</h4>
					    <p class="card-text">Actividades que no han sido confirmadas por el responsable de actividad.</p>
					  </div>
					  {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }}
					  			<br><br>
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::APROBADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="cero1"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CON CONFIRMACION <br>POR EJECUTAR</h4>
					    <p class="card-text">Actividades que no han sido ejecutadas por el responsable de la actividad.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }}
					  			<br><br>
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CONFIRMADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="uno"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>EJECUTADAS</h4>
					    <p class="card-text">Actividades ejecutadas por el resposanble de la actividad.</p>
					   		
					  </div>
					  {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }}
					  			<br><br>
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUTADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-success badge2 btn-xs"><span class="badge" id="dos"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CANCELADA EN EJECUCIÓN.</H4>
					    <p class="card-text">Actividades canceladas al momento de subir la ejecución por el resposanble de la actividad.</p>

					  </div>
					  {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }}
					   		<br><br>
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUTADA_CANCELADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="tres"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>


				<div class="col-md-2">  
	              <div class="card" style="width: 100%;">  
	                <img class="card-img-top" >  
	                <div class="card-body">  
	                  <h5 class="card-title">Todo</h5>  
	                  <p class="card-text">Listado de todas las actividades registradas al responsable.</p>  
	                    
	                </div>  
	                {{ Form::open(array('route' => 'actividadesPorEjecutarResponsable','target' => '_blank')) }} 
	                	<br><br> 
	                    <input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">  
	                    <input type="hidden" name="fechaFinHiden" class="fechaFinHiden">  
	                    <input type="hidden" name="idHiden" class="idHiden">  
	                    <input type="hidden" name="opcion" id="opcion" value="100">  
	                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>  
	                    <button type="submit" class="btn badge2 btn-xs"><span class="badge" id="todo"></span> Ir a actividades</button>  
	                    {{ Form::close() }}  
	              </div>  
	            </div>

		</div>
 
    </div>
</div>

@stop

