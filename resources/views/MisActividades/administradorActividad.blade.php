@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/Administrador/administradorActividad.js') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('administracionActividad') }}" ></div>
	
		

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				<div class="col-md-12">
					<h4><b>Administrador de actividades</b><br><span class="glyphicon glyphicon-user"> Administrador</span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>

	 			<div class="col-md-3">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" autocomplete="off" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-3">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin" autocomplete="off"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-3">
			            <div class="form-group">
			            	<label>Id:</label>
			                <input type='text' class="form-control" name="idAct" id="idAct" autocomplete="off"/>
			            </div>
				</div>

				<div class="col-md-3">
			            <div class="form-group">
			            	<label>Parque:</label>
			                <input type="text" name="Cod_IDRD" class="form-control" value="">
			            </div>
				</div>

				<div class="col-md-12"></div>

	 			<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Localidad comunidad</label>
						<select class="form-control" data-live-search="true" name="localidad_comunidad" id="localidad_comunidad">
							<option value="">Seleccionar</option>
							@if($localidades)
							@foreach($localidades as $localidad)
									
								@if(isset($dataActividad))
									@if($dataActividad['i_fk_localidadComunidad']==$localidad['Id_Localidad'])
										<option value="{{$localidad['Id_Localidad']}}" selected>{{strtoupper($localidad['Nombre_Localidad'])}}</option>
									@else
										<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
									@endif
								@else
									<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
								@endif

							@endforeach
							@endif
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Upz comunidad</label>
						<select class="form-control" data-live-search="true" name="Id_Upz_Comunidad" id="Id_Upz_Comunidad">
							
								@if(isset($dataActividad))
									@if($dataActividad->upz_comunidad)
										<option value="{{$dataActividad['i_fk_upzComunidad']}}" selected>{{strtoupper($dataActividad->upz_comunidad['Upz'])}}</option>
									@else
										<option value="">Seleccionar</option>
									@endif
								@endif

						</select>
					</div>
				</div>

				<div class="col-md-4">
			            
				</div>

				<div class="col-md-12"></div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Localidad escenario</label>
						<select class="form-control" data-live-search="true" name="localidad_Escenario" id="localidad_Escenario">
							<option value="">Seleccionar</option>
							@if($localidades)
							@foreach($localidades as $localidad)
									<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Upz escenario</label>
						<select class="form-control" data-live-search="true" name="Id_Upz_Escenario" id="Id_Upz_Escenario">
							
								@if(isset($dataActividad))
									@if($dataActividad->upz_comunidad)
										<option value="{{$dataActividad['i_fk_upzComunidad']}}" selected>{{strtoupper($dataActividad->upz_comunidad['Upz'])}}</option>
									@else
										<option value="">Seleccionar</option>
									@endif
								@endif

						</select>
					</div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            </div>
				</div>

				<div class="col-md-12"></div>



				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Gestor</label>
						<select class="form-control" data-live-search="true" name="gestor" id="gestor">
							<option value="">Seleccionar</option>
							@if($gestores)
							@foreach($gestores as $gestore)
								<option value="{{$gestore->persona['Id_Persona']}}">
									{{strtoupper($gestore->persona['Primer_Apellido'].' '.$gestore->persona['Segundo_Apellido'].' '.$gestore->persona['Primer_Nombre'].' '.$gestore->persona['Segundo_Nombre'])}}
								</option>	
							@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Responsable</label>
						<select class="form-control" data-live-search="true" name="resposanble" id="resposanble">
							<option value="">Seleccionar</option>
							@if($resposanbles)
							@foreach($resposanbles as $resposanble)
								<option value="{{$resposanble->persona['Id_Persona']}}">
									{{strtoupper($resposanble->persona['Primer_Apellido'].' '.$resposanble->persona['Segundo_Apellido'].' '.$resposanble->persona['Primer_Nombre'].' '.$resposanble->persona['Segundo_Nombre'])}}
								</option>	
							@endforeach
							@endIf

						</select>
					</div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            </div>
				</div>

				<div class="col-md-12"></div>

				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_buscar_Actividades"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			</form>
		</div>

		@php
		use App\Modulos\Configuracion\Configuracion;
		@endphp

		<div class="row" id="resultadoBusqueda" style="display: none;">
			<div class="col-md-1"></div>
			<div class="col-md-11">
				<br>
				<h3>RESULTADO:</h3>
				<H4>Total: <span class="badge" id="labelTotal"></span></H4>
				<br>
			</div>
			
			<div class="col-md-12"></div>

			<div class="col-md-12"><H4>PROGRAMACIÓN</H4></div>
			
			<div class="col-md-1"></div>
			
				<div class="col-md-2">
					<div class="card" style="width: 100%;"  id="card1">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>INCOMPLETA</h4>
					    <p class="card-text">Actividades del gestor, que no han completado el registro de los cinco pasos.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::INCOMPLETA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label{{Configuracion::INCOMPLETA}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card2">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>PENDIENTE</H4>
					    <p class="card-text">Actividades registradas por el gestor, pendientes por  ser aprobadas.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::PENDIENTE}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label{{Configuracion::PENDIENTE}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card3">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>APROBADA</h4>
					    <p class="card-text">Actividades registradas por el gestor, revisadas y aprobadas.</p>
					   		
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::APROBADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-success badge2 btn-xs"><span class="badge" id="label{{Configuracion::APROBADO}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>

				

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card4">
					  <img class="card-img-top" src="../public/Img/denegada.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>DENEGADA</h5>
					    <p class="card-text">Actividades registradas por el gestor, revisadas y denegadas (devueltas).</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::DEVUELTO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-warning badge2 btn-xs"><span class="badge" id="label{{Configuracion::DEVUELTO}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>


				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card5">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CANCELADA</h4>
					    <p class="card-text">Actividades registradas por el gestor, revisadas y canceladas.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CANCELADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="label{{Configuracion::CANCELADO}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>
			<div class="col-md-1"></div>
			

			<div class="col-md-12"><hr></div>


			<div class="col-md-12"><H4>CONFIRMACIÓN</H4></div>
			<div class="col-md-1"></div>
				
				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card6">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CONFIRMADA<br>SIN APROBACIÓN</h4>
					    <p class="card-text">Actividades confirmadas por el responsable de la actividad sin la revisión y aprobación.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CONFIRMADO}}{{Configuracion::CONFIRMADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label44"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card7">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CONFIRMADA</h4>
					    <p class="card-text">Actividades confirmadas por el responsable de la actividad.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CONFIRMADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label{{Configuracion::CONFIRMADO}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card8">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CANCELADAS</h4>
					    <p class="card-text">Actividades canceladas por el resposnable de la actividad.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CONFIRMADO_CANCELADO}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="label{{Configuracion::CONFIRMADO_CANCELADO}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card9">
					  <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>REPROGRAMADAS</h5>
					    <p class="card-text">Actividades reprogramadas en la confirmación.</p>
					   		
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::CONFIRMADO_REPROGRAMADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-success badge2 btn-xs"><span class="badge" id="label{{Configuracion::CONFIRMADO_REPROGRAMADA}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
				</div>

			<div class="col-md-1"></div>



			<div class="col-md-12"><hr></div>


			<div class="col-md-12"><H4>EJECUCIÓN</H4></div>
			<div class="col-md-1"></div>
				
				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card10">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>EJECUCIÓN <br>SIN APROBACIÓN</h4>
					    <p class="card-text">Actividad sin revisión y aprobación de la programación.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="70">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label70"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card11">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>EJECUCIÓN <br>SIN CONFIRMACIÓN</h4>
					    <p class="card-text">Actividad sin confirmación del resposanble de la actividad.</p>
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="71">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label71"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card12">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					   <div class="card-body">
					    <h4>EJECUCIÓN <br>SIN  APROBACIÓN Y CONFIRMACIÓN</h4>
					    <p class="card-text">Actividad sin aprobación de la programación ni confirmación.</p>
					  </div>

					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="72">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label72"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card13">
					  <img class="card-img-top" src="../public/Img/revisando.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>EJECUCIÓN REGISTRADA</h4>
					    <p class="card-text">Actividad con ejecución registrada en espera de revisión.</p>
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUTADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-default badge2 btn-xs"><span class="badge" id="label{{Configuracion::EJECUTADA}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					</div>
				</div>

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card14">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					    <div class="card-body">
						    <h4>CANCELADA, SIN EJECUCIÓN</h4>
						    <p class="card-text">Cancelada antes de registrar la ejecución.</p>
					    </div>
					    	{{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUTADA_CANCELADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
						    	<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="label{{Configuracion::EJECUTADA_CANCELADA}}"></span> Ir a actividades</button>
						    {{ Form::close() }}
					 
					</div>
				</div>

				<div class="col-md-1"></div>
				<div class="col-md-12"><br></div>

				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card15">
					   <img class="card-img-top" src="../public/Img/aprobado.png" alt="Card image cap">
						 <div class="card-body">
						    <h4>APROBADO</h4>
					    	<p class="card-text">Actividades con ejecución aprobada</p>
					     </div>
					   		{{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUCION_APROBADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-success badge2 btn-xs"><span class="badge" id="label{{Configuracion::EJECUCION_APROBADA}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					  
					</div>
				</div>

				

				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card16">
					  <img class="card-img-top" src="../public/Img/cancelado.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>CANCELADA</h4>
					    <p class="card-text">Actividades con ejecución revisadas y canceladas.</p>
					    	
					  </div>
					  {{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUCION_CANCELADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-danger badge2 btn-xs"><span class="badge" id="label{{Configuracion::EJECUCION_CANCELADA}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					</div>
				</div>


				<div class="col-md-2">
					<div class="card" style="width: 100%;" id="card17">
					  <img class="card-img-top" src="../public/Img/denegada.png" alt="Card image cap">
					  <div class="card-body">
					    <h4>DENEGADAS</h4>
					    <p class="card-text">Actividades con ejecución revisadas y denegadas.</p>
					     </div>
					    	{{ Form::open(array('route' => 'actividadesFiltros','target' => '_blank')) }}
								<input type="hidden" name="fechaInicioHiden" class="fechaInicioHiden">
								<input type="hidden" name="fechaFinHiden" class="fechaFinHiden">
								<input type="hidden" name="idActHiden" class="idActHiden">
								<input type="hidden" name="Cod_IDRDHiden" class="Cod_IDRDHiden">
								<input type="hidden" name="localidad_comunidadHiden" class="localidad_comunidadHiden">
								<input type="hidden" name="Id_Upz_ComunidadHiden" class="Id_Upz_ComunidadHiden">
								<input type="hidden" name="localidad_EscenarioHiden" class="localidad_EscenarioHiden">
								<input type="hidden" name="Id_Upz_EscenarioHiden" class="Id_Upz_EscenarioHiden">
								<input type="hidden" name="gestorHiden" class="gestorHiden">
								<input type="hidden" name="resposanbleHiden" class="resposanbleHiden">
								<input type="hidden" name="opcion" id="opcion" value="{{Configuracion::EJECUCION_DENEGADA}}">
								<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
								<button type="submit" class="btn btn-warning badge2 btn-xs"><span class="badge" id="label{{Configuracion::EJECUCION_DENEGADA}}"></span> Ir a actividades</button>
					    	{{ Form::close() }}
					 
					</div>
				</div>

			<div class="col-md-1"></div>

		</div>
 
    </div>
</div>

@stop

