@extends('master')                              

	@section('script')
		@parent
		<script src="{{ asset('public/Js/MisActividad/misActividadesCreadas.js?v=1') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('misProgramacionesCreadas') }}" ></div>
	<div id="main" class="row" data-url="{{ url('personas') }}" data-url-parques="{{ url('parques') }}">
		

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				
				
				<div class="col-md-12">
					<h4><b>MIS PROGRAMACIONES</b><br><span class="glyphicon glyphicon-user"> Gestor </span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>

	 			<div class="col-md-3">
			            <div class="form-group">
			            	<label>Fecha inicio de ejecución:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-3">
			            <div class="form-group">
			            	<label>Fecha fin de ejecución:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>
				<div class="col-md-3">
						<div class="form-group">
			            	<label>Id Actividad:</label>
			                <input type='text' class="form-control" name="id_actividad" id="id_actividad"/>
			            </div>
				</div>

				<div class="col-md-12"></div>

				<div class="col-md-3">
						<div class="form-group">
			            	<label> Responsable </label>
							<select class="form-control" name="responsable">
								<option value="">Seleccionar</option>
								@if($resosablesActividad)
								@foreach($resosablesActividad as $responsable)
									<option value="{{$responsable->persona['Id_Persona']}}">
										{{strtoupper($responsable->persona['Primer_Apellido'].' '.$responsable->persona['Segundo_Apellido'].' '.$responsable->persona['Primer_Nombre'].' '.$responsable->persona['Segundo_Nombre'])}}
									</option>	
								@endforeach
								@endIf
							</select>
			            </div>
				</div>
				

				
				
				<div class="col-md-3">
						<div class="form-group">
			            	<label> Acompañante </label>
							<select class="form-control" name="acompanante">
								<option value="">Seleccionar</option>
								@if($resosablesActividad)
								@foreach($acompanantes as $acompanante)
									<option value="{{$acompanante->persona['Id_Persona']}}">
										{{strtoupper($acompanante->persona['Primer_Apellido'].' '.$acompanante->persona['Segundo_Apellido'].' '.$acompanante->persona['Primer_Nombre'].' '.$acompanante->persona['Segundo_Nombre'])}}
									</option>	
								@endforeach
								@endIf
							</select>
			            </div>
				</div>

				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_Actividades_creadas"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			</form>
		</div>


		<div class="row">
			
			<div class="col-md-12">
				<br>
				<h3>RESULTADO:</h3>
				<br>
			</div>

			<div class="col-md-12">
				<div  id="resultadoBusqueda"></div>
			</div>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target=".bd-example-modal-lg">
					<span class="glyphicon glyphicon-align-justify"></span>  Convención de estados
				</button>
				
				<br><br>
			</div>
			<div class="col-md-12" >
				<div class="table-responsive">
				<div class="table-responsive">
				<table id="tbl_confirmarActividad" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
		            <thead> 
		                <tr> 
		                    <th>#</th> 
		                    <th>Id</th> 
		                    <th>Fecha<br> Ejecuciòn</th> 
		                    <th>Fecha<br> Registro</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th>
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th>Aprobacion</th>
		                    <th>Observaciones</th>
		                    
		                </tr> 
		            </thead>
		            <tfoot>
						<tr>
							<th>#</th> 
		                    <th scope="row" class="text-center" style="width:auto;">Id</th> 
		                    <th>Fecha<br> Ejecuciòn</th>
		                    <th>Fecha<br> Registro</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Programa</th> 
		                    <th>Actividad</th> 
		                    <th>Tematicas / Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                    <th>Aprobacion</th>
		                    <th>Observaciones</th>
						</tr>
					</tfoot>
		            <tbody id="tablaCreadas">
		            </tbody>
		        </table>
				</div>
			</div>
			
		</div>

		<div class="row">
			
			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			      	<div class="row">
			      		
			      		<div class="col-md-1"></div>
			      		<div class="col-md-10">
			      			<br><br>
			      			<h4>TABLA DE CONVENCIONES</h4>
			      		</div>
			      		<div class="col-md-12"></div>

			      		<div class="col-md-1"></div>
				      	<div class="col-md-10">
					      	@include('estados')
						</div>
						<div class="col-md-1"></div>
					</div>
			    </div>
			  </div>
			</div>

		</div>

		<div class="row">
			<br><br>
		</div>

		<!-- MODAL PROGRAMACION-->
			<div class="modal fade" data-backdrop="static" data-keyboard="false" id="modalProgramacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h3 class="modal-title text-primary text-center" id="myModalLabel">Programación de la actividad<br>
							<b>ID:</b> <label id="id_actividadProgramacion"></label></h3>
						</div>

						<form id="form_agregar_estudio_comveniencia">
							<div id="cargando"></div>
							<div class="modal-body" id="modalBody">		
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA COMUNIDAD:</h4>
									</div>
																		
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioP"></p>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Institución, Grupo, Comunidad:</label>
										<p id="modalinstitucionGrupoCP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Características de la Población:</label>
										<p id="modalCaracteristicasP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Específico:</label>
										<p id="modalCaracEspecificasP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Programa
													</th>
													<th>
														Actividad
													</th>
													<th>
														Temática
													</th>
													<th>
														Componente
													</th>
												</tr>
											</thead>
											<tbody id="datosModalActividad">
											</tbody>
										</table>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>PROGRAMACIÓN Y ASIGNACIÓN DE LA ACTIVIDAD:</h4>
									</div>

									<div class="col-xs-3 col-sm-3">
										<label>Responsable:</label>
										<p id="modalResponsableP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Fecha ejecución:</label>
										<p id="modalFechaEjecucionP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora inicio:</label>
										<p id="modalHoraInicioP"></p>
									</div>
									<div class="col-xs-3 col-sm-3">
										<label>Hora fin:</label>
										<p id="modalHoraFinP"></p>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>DATOS DEL ESCENARIO:</h4>
									</div>

									<div class="col-xs-4 col-sm-4">
										<label>Dirección:</label>
										<p id="modalDireccionEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Escenario:</label>
										<p id="modalEscenarioEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Codigo IDRD:</label>
										<p id="modalCodigoIP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Localidad:</label>
										<p id="modalLocalidadEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Upz:</label>
										<p id="modalUpzEP"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Barrio:</label>
										<p id="modalBarrioEP"></p>
									</div>


									<div class="col-xs-12 col-sm-12">
										<h4>ASPECTOS A TENER EN CUENTA:</h4>
									</div>


									<div class="col-xs-4 col-sm-4">
										<label>Hora de implementación:</label>
										<p id="horaImplementacion"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Punto de encuentro:</label>
										<p id="puntoEncuentro"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Nombre de la persona de contacto:</label>
										<p id="nombreContacto"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Rol en la comunidad:</label>
										<p id="rollComunidad"></p>
									</div>
									<div class="col-xs-4 col-sm-4">
										<label>Telefono:</label>
										<p id="telefono"></p>
									</div>
										<div class="col-xs-4 col-sm-4">
									</div>
									
									<div class="col-xs-12 col-sm-12">
										<h4>OBSERVACIONES:</h4>
									</div>

									<div class="col-xs-12 col-sm-12">
										<div class="table-responsive">
										<table class="table table-bordered table-striped table-condensed table-responsive">
											<thead>
												<tr>
													<th>
														Usuario
													</th>
													<th>
														Observación
													</th>
													<th>
														Tipo
													</th>
													<th>
														Fecha
													</th>
												</tr>
											</thead>
											<tbody id="ObservacionActividad">
											</tbody>
										</table>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12">
										<h4>AGREGAR OBSERVACIONES:</h4>
									</div>
									<div class="col-xs-12 col-sm-12">
										<div class="form-group">
										    <label for="exampleFormControlTextarea1">Observación:</label>
										    <input type="hidden" name="id_act_obs" id="id_act_obs">
										    <textarea class="form-control" id="mensajeObser" rows="3"></textarea>
										    
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 text-right">
										<button type="button" class="btn btn-primary btn-xs" id="agregarObservacion">Agregar observación</button>
									</div>

								</div>					
							</div>

							<div class="modal-footer" >
								<div class="row">
									<div class="col-xs-12 col-sm-12" style="text-align: left;">
										<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
									</div>
								</div>
							</div>

						</form>

					</div>
				</div>
			</div>


    </div>
</div>

@stop

