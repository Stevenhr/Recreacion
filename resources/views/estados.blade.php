<table class="table table-dark table-bordered">
  <thead>
    <tr class="bg-warning">
        <td><span class="label label-danger"><b>PROGRAMACION:</b> INCOMPLETA</span><br></td>
        <td>Actividad registrada sin completar los 5 pasos, esta actividad no será tenida en cuenta para revisión y el proceso no se seguira efectuando.</td>
    </tr>
    <tr class="bg-warning">
    	<td><span class="label label-info"><b>Programación: pendiente de revisión</b></span><br></td>
		<td>Actividad registrada en los 5 pasos exitosamente, esta en esperá de ser revisada.</td>
	</tr>
	<tr class="bg-warning">
		<td><span class="label label-success"><b>Programación:</b> APROBADA</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-warning">
		<td><span class="label label-warning"><b>Programación:</b> DENEGADA</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-warning">
		<td><span class="label label-danger"><b>Programación:</b> CANCELADA</span><br></td>
		<td></td>
	</tr>
	<tr>
		<td><span class="label label-success"><b>Programación:</b> CONFIRMADA</span><br></td>
		<td></td>
	</tr>
	<tr>
		<td><span class="label label-danger">Cancelado en confirmación</span><br></td>
		<td></td>
	</tr>
	<tr>
		<td><span class="label label-success">Confirmado con reprogramación</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-success">
		<td><span class="label label-default">Ejecución: Con ejecución registrada.</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-success">
		<td><span class="label label-danger">Ejecución: Cancelada al registrar ejecución.</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-success">
		<td><span class="label label-success"><b>Ejecución:</b> APROBADA</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-success">
		<td><span class="label label-danger"><b>Ejecución:</b> CANCELADA</span><br></td>
		<td></td>
	</tr>
	<tr class="bg-success">
		<td><span class="label label-warning"><b>Ejecución:</b> DENEGADA</span><br></td>
		<td></td>
	</tr>
	<tr>
		<td><span class="label label-danger"><b>Error</span><br></td>
		<td></td>
	</tr>
  </thead>
</table>