@extends('master')                              

	@section('script')
		@parent
		<script src="{{ asset('public/Js/Reportes/reporteGeneral.js') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('reportes') }}" ></div>
	
		

		<div class="row">
			<form method="POST" id="form_consulta_actividades">
				<div class="col-md-12">
					<h4><b>Reporte general</b><br><span class="glyphicon glyphicon-user"> Administrador</span></h4>
					<br>
				</div>
				<div class="col-md-12"></div>

	 			<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Parque:</label>
			                <input type="text" name="Cod_IDRD" class="form-control" value="" placeholder="Codigo: 03-036">
			            </div>
				</div>

				<div class="col-md-12"></div>

	 			<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Localidad comunidad</label>
						<select class="form-control" data-live-search="true" name="localidad_comunidad" id="localidad_comunidad">
							<option value="">Seleccionar</option>
							@if($localidades)
							@foreach($localidades as $localidad)
									
								@if(isset($dataActividad))
									@if($dataActividad['i_fk_localidadComunidad']==$localidad['Id_Localidad'])
										<option value="{{$localidad['Id_Localidad']}}" selected>{{strtoupper($localidad['Nombre_Localidad'])}}</option>
									@else
										<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
									@endif
								@else
									<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
								@endif

							@endforeach
							@endif
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Upz comunidad</label>
						<select class="form-control" data-live-search="true" name="Id_Upz_Comunidad" id="Id_Upz_Comunidad">
							
								@if(isset($dataActividad))
									@if($dataActividad->upz_comunidad)
										<option value="{{$dataActividad['i_fk_upzComunidad']}}" selected>{{strtoupper($dataActividad->upz_comunidad['Upz'])}}</option>
									@else
										<option value="">Seleccionar</option>
									@endif
								@endif

						</select>
					</div>
				</div>

				<div class="col-md-4">
			            
				</div>

				<div class="col-md-12"></div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Localidad escenario</label>
						<select class="form-control" data-live-search="true" name="localidad_Escenario" id="localidad_Escenario">
							<option value="">Seleccionar</option>
							@if($localidades)
							@foreach($localidades as $localidad)
									<option value="{{$localidad['Id_Localidad']}}">{{strtoupper($localidad['Nombre_Localidad'])}}</option>
							@endforeach
							@endif
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Upz escenario</label>
						<select class="form-control" data-live-search="true" name="Id_Upz_Escenario" id="Id_Upz_Escenario">
							
								@if(isset($dataActividad))
									@if($dataActividad->upz_comunidad)
										<option value="{{$dataActividad['i_fk_upzComunidad']}}" selected>{{strtoupper($dataActividad->upz_comunidad['Upz'])}}</option>
									@else
										<option value="">Seleccionar</option>
									@endif
								@endif

						</select>
					</div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            </div>
				</div>

				<div class="col-md-12"></div>



				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Gestor</label>
						<select class="form-control" data-live-search="true" name="gestor" id="gestor">
							<option value="">Seleccionar</option>
							@if($gestores)
							@foreach($gestores as $gestore)
								<option value="{{$gestore->persona['Id_Persona']}}">
									{{strtoupper($gestore->persona['Primer_Apellido'].' '.$gestore->persona['Segundo_Apellido'].' '.$gestore->persona['Primer_Nombre'].' '.$gestore->persona['Segundo_Nombre'])}}
								</option>	
							@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-4 col-xs-12">
					<div class="form-group">
						<label> Responsable</label>
						<select class="form-control" data-live-search="true" name="resposanble" id="resposanble">
							<option value="">Seleccionar</option>
							@if($resposanbles)
							@foreach($resposanbles as $resposanble)
								<option value="{{$resposanble->persona['Id_Persona']}}">
									{{strtoupper($resposanble->persona['Primer_Apellido'].' '.$resposanble->persona['Segundo_Apellido'].' '.$resposanble->persona['Primer_Nombre'].' '.$resposanble->persona['Segundo_Nombre'])}}
								</option>	
							@endforeach
							@endIf

						</select>
					</div>
				</div>


				<div class="col-md-12"></div>



				<div class="col-md-3 col-xs-12">
					<div class="form-group">
						<label> Programa</label>
						<select class="form-control" data-live-search="true" name="programa" id="programa">
							<option value="">Seleccionar</option>
							@if($programas)
								@foreach($programas as $programa)
									<option value="{{$programa['idPrograma']}}">{{strtoupper($programa['programa'])}}</option>	
								@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-3 col-xs-12">
					<div class="form-group">
						<label> Actividad</label>
						<select class="form-control" data-live-search="true" name="actividad" id="actividad">
							<option value="">Seleccionar</option>
							@if($actividades)
								@foreach($actividades as $actividad)
									<option value="{{$actividad['idActividad']}}">{{strtoupper($actividad['actividad'])}}</option>	
								@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-3 col-xs-12">
					<div class="form-group">
						<label> Tematica</label>
						<select class="form-control" data-live-search="true" name="tematica" id="tematica">
							<option value="">Seleccionar</option>
							@if($tematicas)
								@foreach($tematicas as $tematica)
									<option value="{{$tematica['idTematica']}}">{{strtoupper($tematica['tematica'])}}</option>	
								@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-3 col-xs-12">
					<div class="form-group">
						<label> Componente</label>
						<select class="form-control" data-live-search="true" name="componente" id="componente">
							<option value="">Seleccionar</option>
							@if($componentes)
								@foreach($componentes as $componente)
									<option value="{{$componente['idComponente']}}">{{strtoupper($componente['componente'])}}</option>	
								@endforeach
							@endIf
						</select>
					</div>
				</div>

				<div class="col-md-12"></div>

				<div class="col-md-12"><button type="button" class="btn btn-primary btn-sm" id="btn_buscar_Actividades"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
				</form>
		</div>


		<div class="row" >
			<div class="col-md-12">
				<div id="divTotales"></div>
				<br><br>
				<h4><b>Respuesta:</b><br>
				<br>
			</div>
			<div class="col-md-12">
				  <table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>NÚMERO ACTIVIDADES EJECUTADAS</th>
				        <th>NÚMERO DE PERSONAS ASISTENTES</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td><p class="text-center" id="actEjecutadas"></p></td>
				        <td><p class="text-center" id="numAsistentes"></p></td>
				      </tr>
				    </tbody>
				  </table>
			</div>
			<div class="col-md-12">
				<h3>Totales por genero:</h3>
			</div>
			<div class="col-md-12">
				  <table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>Genero</th>
				        <th>0 - 5</th>
				        <th>6 - 12</th>
				        <th>13 - 17</th>
				        <th>18 - 26</th>
				        <th>27 - 59</th>
				        <th> < 60</th>
				        <th>Total</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>Femenino</td>
				        <td><p class="text-center" id="lb_i_0a5_f"></p></td>
				        <td><p class="text-center" id="lb_i_6a12_f"></p></td>
				        <td><p class="text-center" id="lb_i_13a17_f"></p></td>
				        <td><p class="text-center" id="lb_i_18a26_f"></p></td>
				        <td><p class="text-center" id="lb_i_27a59_f"></p></td>
				        <td><p class="text-center" id="lb_i_60_f"></p></td>
				        <td><p class="text-center" id="total_lb_f"></p></td>
				      </tr>
				      <tr>
				        <td>Masculino</td>
				        <td><p class="text-center" id="lb_i_0a5_m"></p></td>
				        <td><p class="text-center" id="lb_i_6a12_m"></p></td>
				        <td><p class="text-center" id="lb_i_13a17_m"></p></td>
				        <td><p class="text-center" id="lb_i_18a26_m"></p></td>
				        <td><p class="text-center" id="lb_i_27a59_m"></p></td>
				        <td><p class="text-center" id="lb_i_60_m"></p></td>
				        <td><p class="text-center" id="total_lb_m"></p></td>
				      </tr>

				      <tr>
				        <td>Totales</td>
				        <td><p class="text-center" id="lb_i_0a5_t"></p></td>
				        <td><p class="text-center" id="lb_i_6a12_t"></p></td>
				        <td><p class="text-center" id="lb_i_13a17_t"></p></td>
				        <td><p class="text-center" id="lb_i_18a26_t"></p></td>
				        <td><p class="text-center" id="lb_i_27a59_t"></p></td>
				        <td><p class="text-center" id="lb_i_60_t"></p></td>
				        <td><p class="text-center" id="total_lb_t"></p></td>
				      </tr>

				    </tbody>
				  </table>
			</div>


		</form>
</div>

@stop

