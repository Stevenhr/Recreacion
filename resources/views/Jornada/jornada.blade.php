@extends('master')                              

	@section('script')
		@parent
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmhb8BVo311Mnvr35sv8VngIvXiiTnKQ4" defer></script>
		<script src="{{ asset('public/Js/jornada/jornada.js') }}"></script>	
	@stop


@section('content') 

<div class="container-fluid">
	<div class="content" id="main_actividad" class="row" data-url="{{ url('jornadasActividades') }}" ></div>
		

		<div class="row">
			{!! Form::open(['url' => 'formConsultaJornada','id' => 'formConsultaJornada']) !!}

				<div class="col-md-2"></div>
				<div class="col-md-10">
					<h4><b>GENERADOR DE JORNADAS</b><br><span class="glyphicon glyphicon-user"> Responsable de la actividad</span></h4>
				</div>
				<div class="col-md-12"></div>

				<div class="col-md-2"></div>
	 			<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha inicio:</label>
			                <div class='input-group date' id='datetimepicker1'>
			                    <input type='text' class="form-control" name="fechaInicio" id="fechaInicio" />
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>

				<div class="col-md-4">
			            <div class="form-group">
			            	<label>Fecha fin:</label>
			                <div class='input-group date' id='datetimepicker2'>
			                    <input type='text' class="form-control" name="fechaFin" id="fechaFin"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
				</div>
				<div class="col-md-2"></div>

				<div class="col-md-12"></div>
				<div class="col-md-2"></div>
				<div class="col-md-10"><button type="button" class="btn btn-primary btn-sm" id="btn_buscarJornada"><span class="glyphicon glyphicon-search"></span> Buscar</button></div>
			{!! Form::close() !!}
		</div>


		<div class="row">
			
			<div class="col-md-12">
				<br>
				<h3>RESULTADO:</h3>
				<br>
			</div>

			<div class="col-md-12">
				<div  id="resultadoBusqueda"></div>
			</div>
			<div class="col-md-12 text-right">
				<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target=".bd-example-modal-lg">
					<span class="glyphicon glyphicon-align-justify"></span>  Convención de estados
				</button>
				
				<br><br>
			</div>
			<div class="col-md-12" >
				
				<table id="tbl_jornadas" class="display responsive no-wrap table table-min table-bordered" width="100%" cellspacing="0" style="width:auto;">
		            <thead> 
		                <tr> 
		                    <th>#</th> 
		                    <th>Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th> 
		                    <th>Jornada (hora) Mañana</th> 
		                    <th>Jornada (hora) Tarde</th>  
		                    <th>Jornada (hora) Noche</th>
		                    <th>Programa</th> 
		                    <th>Actividad</th>
		                    <th>Tematicas</th>
		                    <th>Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
		                </tr> 
		            </thead>
		            <tfoot>
						<tr>
							<th>#</th> 
		                    <th scope="row" class="text-center" style="width:auto;">Id</th> 
		                    <th>Fecha Ejecuciòn</th> 
		                    <th>Hora incio</th> 
		                    <th>Hora fin</th>  
		                    <th>Jornada (hora) Mañana</th> 
		                    <th>Jornada (hora) Tarde</th> 
		                    <th>Jornada (hora) Noche</th> 
		                    <th>Programa</th> 
		                    <th>Actividad</th> 
		                    <th>Tematicas</th>
		                    <th>Componentes</th>
		                    <th>Gestor</th>
		                    <th>Responsable</th>
						</tr>
					</tfoot>
		            <tbody id="tablaCreadas">
		            </tbody>
		        </table>

			</div>
			
		</div>

		<div class="row">
			
			<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			      	<div class="row">
			      		
			      		<div class="col-md-1"></div>
			      		<div class="col-md-10">
			      			<br><br>
			      			<h4>TABLA DE CONVENCIONES</h4>
			      		</div>
			      		<div class="col-md-12"></div>

			      		<div class="col-md-1"></div>
				      	<div class="col-md-10">
					      	@include('estados')
						</div>
						<div class="col-md-1"></div>
					</div>
			    </div>
			  </div>
			</div>

		</div>
 
    </div>
</div>

@stop

