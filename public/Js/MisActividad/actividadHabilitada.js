$(function () {
    
    $('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    var URL = $('#main_actividad').data('url');
    var t="";

    $('#tbl_confirmarActividad tfoot th').each( function () {
        var title = $(this).text();
        if(title!="Menu" && title!="#"){
          $(this).html( '<input type="text" placeholder="Buscar"/>' );
        }
    } );


    
    $('#btn_Actividades_habilitadas').on('click', function(e)
    {
        $('.fechaInicioHiden').val($('#fechaInicio').val());
        $('.fechaFinHiden').val($('#fechaFin').val());

        $('#tablaCreadas').html('');
        $('#resultadoBusqueda').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

        $.post(
            URL+'/tablaActividadHabilitadas',            
            $('#form_consulta_actividades').serialize(),
            function(data)
            {
                if(data.status == 'error')
                {
                    validador_datos(data.errors);
                     $('#resultadoBusqueda').html('');
                }
                else
                {
                        
                        $('#resultadoBusqueda').html('');

                        if ( $.fn.dataTable.isDataTable( '#tbl_confirmarActividad' ) ) 
                        {
                            $("#tbl_confirmarActividad").dataTable().fnDestroy();
                        }
                        
                        $('#tablaCreadas').html(data.tabla);

                        t = $('#tbl_confirmarActividad').DataTable( {responsive: true,
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf'],
                            pageLength: 5
                        });
                        
                                             
                        // Apply the search
                        t.columns().every( function () {
                            var that = this;
                            $( 'input', this.footer() ).on( 'keyup change', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        });

                    
                    $("#resultadoBusqueda").html('');
                }
                
            }
        );
        return false;
    });


    var validador_datos = function(data)
    {
        $('#form_consulta_actividades .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {
                    case 'fechaInicio':
                    case 'fechaFin':
                        selector = 'input';
                    break;    


                     case 'Tipoactividad':
                        selector = 'select';
                    break;             
                }
                $('#form_consulta_actividades '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }



    //Ejecucion

     $('#tbl_confirmarActividad').delegate('button[data-funcion="ejecucion"]','click',function (e) { 

        var id = $(this).data('rel'); 
        $('#id_actividadEjecucion').html(id);
        $('#id_actividad').val(id);
        $('#id_actividad1').val(id);
        $('#id_actividad2').val(id);

    }); 

    $('#modalEjecucion').delegate('#id_agregar_poblacion','click',function (e)
    {
        $.post(
            URL+'/registro_poblacional',            
            $('#form_agregar_poblacion').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {

                    $('#mensajeEjecucion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeEjecucion').show();

                    setTimeout(function(){
                          $('#mensajeEjecucion').html('');
                    }, 2000)

                    validador_form_reprogramacion(data.errors);

                }else{
                    
                    $('#mensajeEjecucion').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeEjecucion').show();
                    
                    setTimeout(function(){
                          $('#mensajeEjecucion').html('');
                    }, 2000)
                    $('#form_agregar_poblacion').reset();

                    validador_form_reprogramacion(data.errors);
                }

            }
        );
        return false;
    });

     var validador_form_reprogramacion = function(data)
    {
        $('#form_agregar_poblacion .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'entidad_ejecucion':
                    case 'localidad_ejecucion':
                    case 'Id_Upz_ejecucion':
                    case 'Id_Barrio_ejecucion':
                    case 'tipoPoblacion_ejecucion':
                    case 'condicion_ejecucion':
                    case 'situacion_ejecucion':
                    case 'localidad_reprogramacion':
                    case 'Id_Upz_Reprogramacion':
                    case 'Id_Barrio_Reprogramacion':
                        selector = 'select';
                    break;      


                    case 'insGrupoComu_ejecucion':
                    case 'i_0a5_f':
                    case 'i_0a5_m':
                    case 'i_6a12_f':
                    case 'i_6a12_m':
                    case 'i_13a17_f':
                    case 'i_13a17_m':
                    case 'i_18a26_f':
                    case 'i_18a26_m':
                    case 'i_27a59_f':
                    case 'i_27a59_m':
                    case 'i_60_f':
                        selector = 'input';
                    break;       

                }
                $('#form_agregar_poblacion '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


     //Carga de las Upzs comunidad
    $('#modalEjecucion').delegate('select[name="localidad_ejecucion"]','change',function (e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_ejecucion"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_ejecucion/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_ejecucion"]').html(html).val($('select[name="Id_Barrio_ejecucion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };



    $('#modalEjecucion').delegate('select[name="Id_Upz_ejecucion"]','change',function (e)
    {
        var otherValue=$(this).find('option:selected').attr('data-othervalue');
        selecionar_barrios_comunidad(otherValue,$('select[name="Id_Barrio_ejecucion"]'));
    });
    
    var selecionar_barrios_comunidad = function(id, select)
    { 
       $('select[name="Id_Barrio_ejecucion"]').html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_barrio_ejecucion/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                select.html(html).val($('select[name="Id_Barrio_ejecucion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data, function(i, eee)
                  {
                            html += '<option value="'+eee['IdBarrio']+'"  >'+eee['Barrio'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', $('select[name="Id_Barrio_ejecucion"]').data('value'));
            }
        });
    };



    $('#modalEjecucion').delegate('#id_agregar_requisito','click',function (e)
    {
        $.post(
            URL+'/registro_requisitos',            
            $('#form_agregar_requisitos').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {

                    validador_form_requisitos(data.errors);
                    
                    $('#mensajeRequisito').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeRequisito').show();

                    setTimeout(function(){
                          $('#mensajeRequisito').html('');
                    }, 2000)

                    

                }else{
                    
                    
                    validador_form_requisitos(data.errors);
                    $('#mensajeRequisito').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeRequisito').show();
                    
                    setTimeout(function(){
                          $('#mensajeRequisito').html('');
                    }, 2000)
                    $('#form_agregar_poblacion').reset();
                    
                }

            }
        );
        return false;
    });



     var validador_form_requisitos = function(data)
    {
        $('#form_agregar_requisitos .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'requisito_ejecucion':
                        selector = 'select';
                    break;      

                    case 'causa_ejecucion':
                    case 'accion_ejecucion':
                        selector = 'textarea';
                    break;       

                }
                $('#form_agregar_requisitos '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }



     $('#modalEjecucion').delegate('#id_agregar_encuesta','click',function (e)
    {
        $.post(
            URL+'/registro_encuesta',            
            $('#form_agregar_encuesta').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {
                    $('#mensajeEncuesta').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeEncuesta').show();

                    setTimeout(function(){
                          $('#mensajeEncuesta').html('');
                    }, 2000)

                    validador_form_encuesta(data.errors);

                }else{
                    
                    validador_form_encuesta(data.errors);

                    $('#mensajeEncuesta').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeEncuesta').show();
                    
                    setTimeout(function(){
                          $('#mensajeEncuesta').html('');
                    }, 2000)
                    $('#form_agregar_encuesta').reset();

                    
                }

            }
        );
        return false;
    });


    var validador_form_encuesta = function(data)
    {
        $('#form_agregar_encuesta .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {

                    case 'puntualidad_ejecucion':
                    case 'divulgacion_ejecucion':
                    case 'escenario_ejecucion':
                    case 'cumplimiento_ejecucion':
                    case 'veriedad_ejecucion':
                    case 'imagen_ejecucion':
                    case 'seguridad_ejecucion':
                        selector = 'select';
                    break;        

                }
                $('#form_agregar_encuesta '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


        $('#tbl_confirmarActividad').delegate('button[data-funcion="ejecucionDatos"]','click',function (e){ 
        var id = $(this).data('rel'); 
        $('#id_actividadDatosEjecucion').html(id);
        
        $('#tdEjecucion').html('');
        $('#tdEjecucion').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

        $('#tdEjecucionRequisitos').html('');
        $('#tdEjecucionRequisitos').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');


        $('#tdEjecucionEncuesta').html('');
        $('#tdEjecucionEncuesta').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');


         $.get(
            URL+'/obtenerDatosEjecucion/'+id,
            function(data)
            {                

                $('#tdEjecucion').html(data.viewEjecuciones);
                $('#tdEjecucionRequisitos').html(data.viewRequisitos);
                $('#tdEjecucionEncuesta').html(data.viewEncuesta);

                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionActividad' ) ) 
                {
                    $('#tbl_datosEjecucionActividad').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionActividad').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }


                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionRequisitos' ) ) 
                {
                    $('#tbl_datosEjecucionRequisitos').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionRequisitos').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }


                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionEncuesta' ) ) 
                {
                    $('#tbl_datosEjecucionEncuesta').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionEncuesta').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }
                
            }
        );

    }); 



    $('#tbl_datosEjecucionActividad').delegate('button[data-funcion="eliminar"]','click',function (e) {  

        var id = $(this).data('rel'); 
        var $tr = $('#tbl_datosEjecucionActividad').find('tr[data-row="'+id+'"]');

                $.get(
                    URL+'/eliminarEjecucion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        $tr.addClass('danger');
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                            $tr.css('display', 'none');
                        }, 3000);
                    }
                );
    });

    $('#tbl_datosEjecucionRequisitos').delegate('button[data-funcion="eliminarRequisito"]','click',function (e) {  
        var id = $(this).data('rel'); 
        var $tr = $('#tbl_datosEjecucionRequisitos').find('tr[data-row="'+id+'"]');

                $.get(
                    URL+'/eliminarEjecucionRequerimiento/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajerequerimiento').html(data.mensaje);
                        $tr.addClass('danger');
                        setTimeout(function(){
                            $('#'+id+'mensajerequerimiento').html('');
                            $tr.css('display', 'none');
                        }, 3000);
                    }
                );
    });



});