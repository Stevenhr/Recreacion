$(function () {
    
    $('#fechaEjecucionReprogramacion').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
    });

    $('#horaInicioReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });

    $('#horaFinReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });

    $('#horaImpleReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });


     var URL = $('#main_actividad').data('url');
     $('#tbl_confirmarActividad tfoot th').each( function () {
        var title = $(this).text();
        if(title!="Menu" && title!="#"){
          $(this).html( '<input type="text" placeholder="Buscar"/>' );
        }
    } );
 
    // DataTable
    var t = $('#tbl_confirmarActividad').DataTable( {responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'],
        pageLength: 5
    });
 
    // Apply the search
    t.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });


     $('#agregarObservacion').on('click', function(e)
    {   
        var id = $('#id_act_obs').val();
        var mensaje = $('#mensajeObser').val();
        $.get(
            URL+'/agregarObservacionConfirmacion/'+id+'/'+mensaje,
            function(data)
            {
                $('#modalProgramacion').modal('hide');
            });
    });


    $('#tbl_confirmarActividad').delegate('button[data-funcion="programacion"]','click',function (e) {  

        var id = $(this).data('rel'); 
        
        $('#modalBody').hide();
        $('#cargando').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');
        
        $('#modalLocalidadP').html('');
        $('#modalUpzP').html('');
        $('#modalBarrioP').html('');
        $('#modalinstitucionGrupoCP').html('');
        $('#modalCaracteristicasP').html('');
        $('#modalCaracEspecificasP').html('');
        $('#modalResponsableP').html('');
        $('#modalFechaEjecucionP').html('');
        $('#modalHoraInicioP').html('');
        $('#modalHoraFinP').html('');
        $('#modalDireccionEP').html('');
        $('#modalEscenarioEP').html('');
        $('#modalCodigoIP').html('');
        $('#modalLocalidadEP').html('');
        $('#modalUpzEP').html('');
        $('#modalBarrioEP').html('');
        $('#datosModalActividad').html('');

        $.get(
            URL+'/datosprogramacionactividadConfirmar/'+id,
            function(data)
            {
                
                $('#modalLocalidadP').html(data.localidad_comunidad['Localidad']); 
                $('#modalUpzP').html(data.upz_comunidad['Upz']);
                $('#modalBarrioP').html(data.barrio_comunidad['Barrio']);


                $('#modalinstitucionGrupoCP').html(data['vc_institutoGrupoComunidad']);

                $('#modalResponsableP').html(data.responsable['Primer_Apellido']+' '+data.responsable['Segundo_Apellido']+' '+data.responsable['Primer_Nombr']);
                $('#modalFechaEjecucionP').html(data['d_fechaEjecucion']);
                $('#modalHoraInicioP').html(data['t_horaInicio']);
                $('#modalHoraFinP').html(data['t_horaFin']);


                $('#modalDireccionEP').html(data['vc_direccion']);
                $('#modalEscenarioEP').html(data['vc_escenario']);
                $('#modalCodigoIP').html(data['vc_codigoParque']);
                $('#modalLocalidadEP').html(data.localidad_escenario['Localidad']);
                $('#modalUpzEP').html(data.upz_escenario['Upz']);
                $('#modalBarrioEP').html(data.barrio_escenario['Barrio']);

                $('#horaImplementacion').html(data['t_horaImplementacion']);
                $('#puntoEncuentro').html(data['vc_puntoEncuentro']);
                $('#nombreContacto').html(data['vc_personaContacto']);
                $('#rollComunidad').html(data['vc_rollComunidad']);
                $('#telefono').html(data['i_telefono']);

                
                var html = '';
                if(data.datos_actividad.length > 0)
                {
                    var num=1;
                    $.each(data.datos_actividad, function(i, e){
                      html += '<tr><td>'+e.programa['programa']+'</td><td>'+e.actividad['actividad']+'</td><td>'+e.tematica['tematica']+'</td><td>'+e.componente['componente']+'</td></tr>';
                      num++;
                    });
                }
                $('#datosModalActividad').html(html);


                var html2 = '';
                if(data.observaciones.length > 0)
                {
                    var num=1;
                    $.each(data.observaciones, function(i, e){
                      html2 += '<tr><td>'+e.usuario['Primer_Apellido']+' '+e.usuario['Segundo_Apellido']+' '+e.usuario['Primer_Nombre']+'</td><td>'+e['tx_observacion']+'</td><td>'+e['vc_tipoobservacion']+'</td><td>'+e['created_at']+'</td></tr>';
                      num++;
                    });
                }
                $('#ObservacionActividad').html(html2);

                
                $('#cargando').html('');
                $('#modalBody').show();
            }
        );
        $('#id_actividadProgramacion').html(id);
        $('#id_act_obs').val(id);
        $('#mensajeObser').val('');

    }); 


  



    $('#tbl_confirmarActividad').delegate('button[data-funcion="aprobar"]','click',function (e) {  

        var id = $(this).data('rel'); 
        var val = $(this).data('val'); 
        var $tr = $('#tbl_confirmarActividad').find('tr[data-row="'+id+'"]');

            if(val == 0)
            {
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }

                $.get(
                    URL+'/confirmarActivida/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeConfirmacion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeConfirmacion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }
            else if(val == 1)
            {
                var text = $('#tx_observaciones').val();
                

                if(text==="")
                {
                    document.getElementById("tx_observaciones").style.borderColor= 'red';
                    document.getElementById("tx_observaciones").scrollTop +=10;
                    $('#'+id+'mensaje').html("<div class='alert alert-danger' role='alert'>Error: Falta observación</div>");
                    setTimeout(function(){
                        document.getElementById("tx_observaciones").style.borderColor= '';
                        $('#'+id+'mensaje').html('');
                    }, 3000);

                }else{
                    if ($tr.hasClass('warning'))
                    {
                       $tr.removeClass('warning');
                    }
                    if ($tr.hasClass('success'))
                    {
                       $tr.removeClass('success');
                    }

                    $.get(
                        URL+'/noConfirmarActividad/'+id,
                        function(data)
                        {
                            $('#'+id+'mensajeConfirmacion').html(data.mensaje);
                            setTimeout(function(){
                                $('#'+id+'mensajeConfirmacion').html('');
                            }, 3000);
                        }
                    );
                    $tr.addClass('danger');
                }
            }
            else if(val == 2)
            {
             
                $.get(
                    URL+'/buscarActividad/'+id,
                    function(data)
                    {
                        $('#idActividadReProgramacion').val(data.datos['i_pk_id']);
                        $('#id_actividadReProgramacion').html(data.datos['i_pk_id']);

                        $('#fechaRegistoReProgramacion').val(data.datos['created_at']);
                        $('#modalReprogramacionFechaRegistro').html(data.datos['created_at']);

                        $('#horaImpleReprogramacion').val(data.datos['t_horaImplementacion']);
                        $('#modalReprogramacionHorahoraImple').html(data.datos['t_horaImplementacion']);

                        $('#puntoEncuprogramacion').val(data.datos['vc_puntoEncuentro']);
                        $('#modalpuntoEncuprogramacion').html(data.datos['vc_puntoEncuentro']);
                        
                        $('#fechaEjecucionReprogramacion').val(data.datos['d_fechaEjecucion']);
                        $('#modalReprogramacionFechaEjecucionP').html(data.datos['d_fechaEjecucion']);

                        $('#horaInicioReprogramacion').val(data.datos['t_horaInicio']);
                        $('#modalReprogramacionHoraInicioP').html(data.datos['t_horaInicio']);

                        $('#horaFinReprogramacion').val(data.datos['t_horaFin']);
                        $('#modalReprogramacionHoraFinP').html(data.datos['t_horaFin']);
                        
                        $('#direccionReprogramacion').val(data.datos['vc_direccion']);
                        $('#modalReprogramacionDireccionEP').html(data.datos['vc_direccion']);

                        $('#escenarioReprogramacion').val(data.datos['vc_escenario']);
                        $('#modalReprogramacionEscenarioEP').html(data.datos['vc_escenario']);

                        $('#codigoReprogramacion').val(data.datos['vc_codigoParque']);
                        $('#modalReprogramacionCodigoIP').html(data.datos['vc_codigoParque']);

                        $('#modalLocalidadEProgramacion').html(data.datos.localidad_escenario['Localidad']);
                        $('#modalUpzEProgramacion').html(data.datos.upz_escenario['Upz']);
                        $('#modalBarrioEProgramacion').html(data.datos.barrio_escenario['Barrio']);

                    }
                );
            }
    }); 



    //Carga de las Upzs comunidad
    $('#modalReProgramacion').delegate('select[name="localidad_reprogramacion"]','change',function (e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_Reprogramacion"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Reprogramacion"]').html(html).val($('select[name="Id_Barrio_Reprogramacion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };

 
    $('#modalReProgramacion').delegate('select[name="Id_Upz_Reprogramacion"]','change',function (e)
    {
        var otherValue=$(this).find('option:selected').attr('data-othervalue');
        selecionar_barrios_comunidad(otherValue,$('select[name="Id_Barrio_Reprogramacion"]'));
    });
    
    var selecionar_barrios_comunidad = function(id, select)
    { 
       $('select[name="Id_Barrio_Reprogramacion"]').html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_barrio/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                select.html(html).val($('select[name="Id_Barrio_Reprogramacion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data, function(i, eee)
                  {
                            html += '<option value="'+eee['IdBarrio']+'"  >'+eee['Barrio'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', $('select[name="Id_Barrio_Reprogramacion"]').data('value'));
            }
        });
    };


    $('#btn_reprogramar').on('click', function(e)
    {
        var id = $('#idActividadReProgramacion').val();
        $.post(
            URL+'/disponibilidad_reprogramacion',            
            $('#form_agregar_reprogramacion').serialize(),
            function(data)
            {
                var $tr = $('#tbl_confirmarActividad').find('tr[data-row="'+id+'"]');

                if(data.status=="Error")
                {

                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeReprogramacion').show();
                    
                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 2000)

                }
                else if(data.status=="Modificado")
                {
                    
                    $tr.hide();
                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-success"><strong>Bien!</strong> Modificacion de la actividad se ha realizado exitosamente. <strong>La actividad pasa al estado reprogramada.</strong>.</div>');
                    $('#mensajeReprogramacion').show();
                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 5000)
                }
                else
                {
                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong>'+data.menj+
                    '</div>');
                    $('#mensajeReprogramacion').show();

                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 2000)
                }
            }
        );

        return false;
    });

    var validador_form_reprogramacion = function(data)
    {
        $('#form_agregar_reprogramacion .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'localidad_reprogramacion':
                    case 'Id_Upz_Reprogramacion':
                    case 'Id_Barrio_Reprogramacion':
                        selector = 'select';
                    break;      

                    case 'fechaEjecucionReprogramacion':
                    case 'horaInicioReprogramacion':
                    case 'horaFinReprogramacion':
                    case 'direccionReprogramacion':
                    case 'escenarioReprogramacion':
                        selector = 'input';
                    break;       

                }
                $('#form_agregar_reprogramacion '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }



});