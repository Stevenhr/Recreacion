$(function () {
    $('#datetimepicker1').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    var URL = $('#main_actividad').data('url');

 

    $('#btn_buscar_Actividades').on('click', function(e)
    {

    	$('.fechaInicioHiden').val($('#fechaInicio').val());
    	$('.fechaFinHiden').val($('#fechaFin').val());
        $('.idActHiden').val($('#idAct').val());
        $('.Cod_IDRDHiden').val($('#Cod_IDRD').val());
        $('.localidad_comunidadHiden').val($('#localidad_comunidad').val());
        $('.Id_Upz_ComunidadHiden').val($('#Id_Upz_Comunidad').val());
        $('.localidad_EscenarioHiden').val($('#localidad_Escenario').val());
        $('.Id_Upz_EscenarioHiden').val($('#Id_Upz_Escenario').val());
        $('.gestorHiden').val($('#gestor').val());
        $('.resposanbleHiden').val($('#resposanble').val());

        $("#resultadoBusqueda").hide();

        $('#divTotales').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');
        $("#divTotales").show();

        $.post(
            URL+'/reporteBusquedaActividades',            
            $('#form_consulta_actividades').serialize(),
            function(data)
            {
                if(data.status == 'error')
                {
                    validador_datos(data.errors);
                    $('#divTotales').html('');
                }
                else
                {
                    validador_datos(data.errors);

                    $('#actEjecutadas').html(data.datos.numeroActividades);
                    $('#numAsistentes').html(data.datos.numeroTotalAsitentes);

                    $('#lb_i_0a5_f').html(data.datos.ejecuciones.i_0a5_f);
                    $('#lb_i_6a12_f').html(data.datos.ejecuciones.i_6a12_f);
                    $('#lb_i_13a17_f').html(data.datos.ejecuciones.i_13a17_f);
                    $('#lb_i_18a26_f').html(data.datos.ejecuciones.i_18a26_f);
                    $('#lb_i_27a59_f').html(data.datos.ejecuciones.i_27a59_f);
                    $('#lb_i_60_f').html(data.datos.ejecuciones.i_60_f);
                    $('#total_lb_f').html(data.datos.totalAsistentesFemenino);

                    $('#lb_i_0a5_m').html(data.datos.ejecuciones.i_0a5_m);
                    $('#lb_i_6a12_m').html(data.datos.ejecuciones.i_6a12_m);
                    $('#lb_i_13a17_m').html(data.datos.ejecuciones.i_13a17_m);
                    $('#lb_i_18a26_m').html(data.datos.ejecuciones.i_18a26_m);
                    $('#lb_i_27a59_m').html(data.datos.ejecuciones.i_27a59_m);
                    $('#lb_i_60_m').html(data.datos.ejecuciones.i_60_m);
                    $('#total_lb_m').html(data.datos.totalAsistentesMasculino);

                    $('#lb_i_0a5_t').html(data.datos.i_0a5_t);
                    $('#lb_i_6a12_t').html(data.datos.i_6a12_t);
                    $('#lb_i_13a17_t').html(data.datos.i_13a17_t);
                    $('#lb_i_18a26_t').html(data.datos.i_18a26_t);
                    $('#lb_i_27a59_t').html(data.datos.i_27a59_t);
                    $('#lb_i_60_t').html(data.datos.i_60_t);
                    $('#total_lb_t').html(data.datos.totalGeneros);


                    $('#divTotales').html('');
                	
                }
            }
        );
        return false;
    });

    var validador_datos = function(data)
    {
        $('#form_consulta_actividades .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {
                    case 'fechaInicio':
                    case 'fechaFin':
                        selector = 'input';
                    break;               
                }
                $('#form_consulta_actividades '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


     //Carga de las Upzs comunidad
    $('select[name="localidad_comunidad"]').on('change', function(e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_Comunidad"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_admin/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Comunidad"]').html(html).val($('select[name="Id_Barrio_Comunidad"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };


    //Carga de las Upzs comunidad
    $('select[name="localidad_Escenario"]').on('change', function(e)
    {
        selecionar_upz_escenario($(this).val(),$('select[name="Id_Upz_Escenario"]'));
    });

    var selecionar_upz_escenario = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_admin/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Comunidad"]').html(html).val($('select[name="Id_Barrio_Comunidad"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };


});