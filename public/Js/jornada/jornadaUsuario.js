$(function () {
    $('#datetimepicker1').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    var URL = $('#main_actividad').data('url');



    $('#tbl_jornadas tfoot th').each( function () {
        var title = $(this).text();
        if(title!="Menu" && title!="#"){
          $(this).html( '<input type="text" placeholder="Buscar"/>' );
        }
    } );


    $('#btn_buscarJornada').on('click', function(e)
    {
     
        $('#tablaCreadas').html('');
        $('#resultadoBusqueda').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

        $.post(
            URL+'/busquedaJornadaUsuario',     
            $('#formConsultaJornada').serialize(),
            function(data)
            {
                if(data.status == 'error')
                {
                    validador_datos(data.errors);
                    $('#resultadoBusqueda').html('');
                }
                else
                {
                     validador_datos(data.errors);
                	if ( $.fn.dataTable.isDataTable( '#tbl_jornadas' ) ) 
                        {
                            $("#tbl_jornadas").dataTable().fnDestroy();
                        }
                        
                        $('#tablaCreadas').html(data.tabla);

                        t = $('#tbl_jornadas').DataTable( {responsive: true,
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf'],
                            pageLength: 5
                        });
                        
                                             
                        // Apply the search
                        t.columns().every( function () {
                            var that = this;
                            $( 'input', this.footer() ).on( 'keyup change', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        });

                    
                    $("#resultadoBusqueda").html('');
                }
                
            }
        );
        return false;
    });

    var validador_datos = function(data)
    {
        $('#formConsultaJornada .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {
                    case 'fechaInicio':
                    case 'fechaFin':
                        selector = 'input';
                    break;    

                    case 'responsable':
                        selector = 'select';
                    break;           
                }
                $('#formConsultaJornada '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }



});