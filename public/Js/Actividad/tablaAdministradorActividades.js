$(function () {

    $('#datetimepicker1').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    var URL = $('#main_actividad').data('url');



     $('#tbl_resposablePrograma tfoot th').each( function () {
        var title = $(this).text();
        if(title!="Menu" && title!="#"){
          $(this).html( '<input type="text" placeholder="Buscar"/>' );
        }
    } );
 
    // DataTable
    var t = $('#tbl_resposablePrograma').DataTable( {responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'],
        pageLength: 5
    });
 
    // Apply the search
    t.columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });

    $('#btn_buscar_Actividades').on('click', function(e)
    {

    	$('.fechaInicioHiden').val($('#fechaInicio').val());
    	$('.fechaFinHiden').val($('#fechaFin').val());

        $("#resultadoBusqueda").hide();
        $.post(
            URL+'/busquedaActividad',            
            $('#form_consulta_actividades').serialize(),
            function(data)
            {
                if(data.status == 'error')
                {
                    validador_datos(data.errors);
                }
                else
                {
                	$('#uno').html(data.datos.actividadesPorRevisar);
					$('#dos').html(data.datos.actividadesAprobadas);
					$('#tres').html(data.datos.actividadesCanceladas);
					$('#cuatro').html(data.datos.actividadesDenegadas);
                	$("#resultadoBusqueda").show();
                }
                
            }
        );
        return false;
    });

    var validador_datos = function(data)
    {
        $('#form_consulta_actividades .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {
                    case 'fechaInicio':
                    case 'fechaFin':
                        selector = 'input';
                    break;               
                }
                $('#form_consulta_actividades '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


    $('#tbl_resposablePrograma').delegate('a[data-funcion="ejecucion"]','click',function (e) {  

        var id = $(this).data('rel'); 
        $('#id_actividadEjecucion').html(id);

    }); 

    $('#tbl_resposablePrograma').delegate('button[data-funcion="ejecucionRegistrar"]','click',function (e) {  

        var id = $(this).data('rel'); 

        $('#id_actividadEjecucion').html(id);
        $('#id_actividad').val(id);
        $('#id_actividad1').val(id);
        $('#id_actividad2').val(id);

    }); 

    //Carga de las Upzs comunidad
    $('#modalEjecucion').delegate('select[name="localidad_ejecucion"]','change',function (e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_ejecucion"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_ejecucion/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_ejecucion"]').html(html).val($('select[name="Id_Barrio_ejecucion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };


    $('#modalEjecucion').delegate('select[name="Id_Upz_ejecucion"]','change',function (e)
    {
        var otherValue=$(this).find('option:selected').attr('data-othervalue');
        selecionar_barrios_comunidad(otherValue,$('select[name="Id_Barrio_ejecucion"]'));
    });
    
    var selecionar_barrios_comunidad = function(id, select)
    { 
       $('select[name="Id_Barrio_ejecucion"]').html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_barrio_ejecucion/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                select.html(html).val($('select[name="Id_Barrio_ejecucion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data, function(i, eee)
                  {
                            html += '<option value="'+eee['IdBarrio']+'"  >'+eee['Barrio'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', $('select[name="Id_Barrio_ejecucion"]').data('value'));
            }
        });
    };


    $('#modalEjecucion').delegate('#id_agregar_poblacion','click',function (e)
    {
        $.post(
            URL+'/registro_poblacional',            
            $('#form_agregar_poblacion').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {
                    validador_form_reprogramacion2(data.errors);
                    $('#mensajeEjecucion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeEjecucion').show();

                    setTimeout(function(){
                          $('#mensajeEjecucion').html('');
                    }, 2000)

                    

                }else{
                    
                    validador_form_reprogramacion2(data.errors);

                    $('#mensajeEjecucion').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeEjecucion').show();
                    
                    setTimeout(function(){
                          $('#mensajeEjecucion').html('');
                    }, 2000)
                    $('#form_agregar_poblacion').reset();

                    
                }

            }
        );
        return false;
    });


    var validador_form_reprogramacion2 = function(data)
    {
        $('#form_agregar_poblacion .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'entidad_ejecucion':
                    case 'localidad_ejecucion':
                    case 'Id_Upz_ejecucion':
                    case 'Id_Barrio_ejecucion':
                    case 'tipoPoblacion_ejecucion':
                    case 'condicion_ejecucion':
                    case 'situacion_ejecucion':
                    case 'localidad_reprogramacion':
                    case 'Id_Upz_Reprogramacion':
                    case 'Id_Barrio_Reprogramacion':
                        selector = 'select';
                    break;      


                    case 'insGrupoComu_ejecucion':
                    case 'i_0a5_f':
                    case 'i_0a5_m':
                    case 'i_6a12_f':
                    case 'i_6a12_m':
                    case 'i_13a17_f':
                    case 'i_13a17_m':
                    case 'i_18a26_f':
                    case 'i_18a26_m':
                    case 'i_27a59_f':
                    case 'i_27a59_m':
                    case 'i_60_f':
                        selector = 'input';
                    break;       

                }
                $('#form_agregar_poblacion '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


    $('#modalEjecucion').delegate('#id_agregar_requisito','click',function (e)
    {
        $.post(
            URL+'/registro_requisitos',            
            $('#form_agregar_requisitos').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {

                    validador_form_requisitos(data.errors);
                    
                    $('#mensajeRequisito').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeRequisito').show();

                    setTimeout(function(){
                          $('#mensajeRequisito').html('');
                    }, 2000)

                    

                }else{
                    
                    
                    validador_form_requisitos(data.errors);
                    $('#mensajeRequisito').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeRequisito').show();
                    
                    setTimeout(function(){
                          $('#mensajeRequisito').html('');
                    }, 2000)
                    $('#form_agregar_poblacion').reset();
                    
                }

            }
        );
        return false;
    });



     var validador_form_requisitos = function(data)
    {
        $('#form_agregar_requisitos .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'requisito_ejecucion':
                        selector = 'select';
                    break;      

                    case 'causa_ejecucion':
                    case 'accion_ejecucion':
                        selector = 'textarea';
                    break;       

                }
                $('#form_agregar_requisitos '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


    $('#modalEjecucion').delegate('#id_agregar_encuesta','click',function (e)
    {
        $.post(
            URL+'/registro_encuesta',            
            $('#form_agregar_encuesta').serialize(),
            function(data)
            {   
                if(data.status=="Error")
                {
                    $('#mensajeEncuesta').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeEncuesta').show();

                    setTimeout(function(){
                          $('#mensajeEncuesta').html('');
                    }, 2000)

                    validador_form_encuesta(data.errors);

                }else{
                    
                    validador_form_encuesta(data.errors);

                    $('#mensajeEncuesta').html('<div class="alert alert-success">'+
                      '<strong>Bien!</strong> registro exitoso.'+
                    '</div>');
                    $('#mensajeEncuesta').show();
                    
                    setTimeout(function(){
                          $('#mensajeEncuesta').html('');
                    }, 2000)
                    $('#form_agregar_encuesta').reset();

                    
                }

            }
        );
        return false;
    });


    var validador_form_encuesta = function(data)
    {
        $('#form_agregar_encuesta .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {

                    case 'puntualidad_ejecucion':
                    case 'divulgacion_ejecucion':
                    case 'escenario_ejecucion':
                    case 'cumplimiento_ejecucion':
                    case 'veriedad_ejecucion':
                    case 'imagen_ejecucion':
                    case 'seguridad_ejecucion':
                        selector = 'select';
                    break;        

                }
                $('#form_agregar_encuesta '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }

    $('#tbl_resposablePrograma').delegate('button[data-funcion="programacion"]','click',function (e) {  

        var id = $(this).data('rel'); 
        
        $('#modalBody').hide();
        $('#cargando').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');
        
        $('#modalLocalidadP').html('');
        $('#modalUpzP').html('');
        $('#modalBarrioP').html('');
        $('#modalinstitucionGrupoCP').html('');
        $('#modalCaracteristicasP').html('');
        $('#modalCaracEspecificasP').html('');
        $('#modalResponsableP').html('');
        $('#modalFechaEjecucionP').html('');
        $('#modalHoraInicioP').html('');
        $('#modalHoraFinP').html('');
        $('#modalDireccionEP').html('');
        $('#modalEscenarioEP').html('');
        $('#modalCodigoIP').html('');
        $('#modalLocalidadEP').html('');
        $('#modalUpzEP').html('');
        $('#modalBarrioEP').html('');
        $('#datosModalActividad').html('');

        $.get(
            URL+'/datosprogramacionactividad/'+id,
            function(data)
            {
              
                $('#modalLocalidadP').html(data.localidad_comunidad['Localidad']); 
                $('#modalUpzP').html(data.upz_comunidad['Upz']);
                $('#modalBarrioP').html(data.barrio_comunidad['Barrio']);


                $('#modalinstitucionGrupoCP').html(data['vc_institutoGrupoComunidad']);
                    if(data.responsable){
                        $('#modalResponsableP').html(data.responsable['Primer_Apellido']+' '+data.responsable['Segundo_Apellido']+' '+data.responsable['Primer_Nombr']);
                    }
                    else{
                        $('#modalResponsableP').html('');
                    }
                $('#modalFechaEjecucionP').html(data['d_fechaEjecucion']);
                $('#modalHoraInicioP').html(data['t_horaInicio']);
                $('#modalHoraFinP').html(data['t_horaFin']);


                $('#modalDireccionEP').html(data['vc_direccion']);
                $('#modalEscenarioEP').html(data['vc_escenario']);
                $('#modalCodigoIP').html(data['vc_codigoParque']);
                    if(data.localidad_escenario){
                        $('#modalLocalidadEP').html(data.localidad_escenario['Localidad']);
                    }
                    else{
                        $('#modalLocalidadEP').html('');
                    }  


                    if(data.upz_escenario){
                       $('#modalUpzEP').html(data.upz_escenario['Upz']);
                    }
                    else{
                        $('#modalUpzEP').html('');
                    } 


                    if(data.barrio_escenario){
                        $('#modalBarrioEP').html(data.barrio_escenario['Barrio']);
                    }
                    else{
                        $('#modalBarrioEP').html('');
                    } 


                
                

                $('#horaImplementacion').html(data['t_horaImplementacion']);
                $('#puntoEncuentro').html(data['vc_puntoEncuentro']);
                $('#nombreContacto').html(data['vc_personaContacto']);
                $('#rollComunidad').html(data['vc_rollComunidad']);
                $('#telefono').html(data['i_telefono']);

                
                var html = '';
                if(data.datos_actividad.length > 0)
                {
                    var num=1;
                    $.each(data.datos_actividad, function(i, e){
                      html += '<tr><td>'+e.programa['programa']+'</td><td>'+e.actividad['actividad']+'</td><td>'+e.tematica['tematica']+'</td><td>'+e.componente['componente']+'</td></tr>';
                      num++;
                    });
                }
                $('#datosModalActividad').html(html);
                 $('#cargando').html('');
                
                $('#modalBody').show();
            }
        );
        $('#id_actividadProgramacion').html(id);

    }); 



     $('#tbl_resposablePrograma').delegate('button[data-funcion="aprobar"]','click',function (e) {  

        var id = $(this).data('rel'); 
        var val = $(this).data('val'); 
    
            var $tr = $('#tbl_resposablePrograma').find('tr[data-row="'+id+'"]');

            if(val == 0)
            {

                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }

                $.get(
                    URL+'/actividadAprobada/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }
            else if(val == 1)
            {

                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/actividadDevuelta/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('warning');

            }else if(val == 2)
            {
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/actividadCancelada/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('danger');
            }else if(val == 3)
            {
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }

                $.get(
                    URL+'/confirmarActivida/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeConfirmacion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeConfirmacion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }
            else if(val == 4)
            {
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/noConfirmarActividad/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeConfirmacion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeConfirmacion').html('');
                        }, 3000);
                    }
                );
                $tr.addClass('danger');
            }
            else if(val == 5)
            {
             
                $.get(
                    URL+'/buscarActividad/'+id,
                    function(data)
                    {
                        console.log(data);
                        $('#idActividadReProgramacion').val(data.datos['i_pk_id']);
                        $('#id_actividadReProgramacion').html(data.datos['i_pk_id']);

                        $('#fechaRegistoReProgramacion').val(data.datos['created_at']);
                        $('#modalReprogramacionFechaRegistro').html(data.datos['created_at']);

                        $('#horaImpleReprogramacion').val(data.datos['t_horaImplementacion']);
                        $('#modalReprogramacionHorahoraImple').html(data.datos['t_horaImplementacion']);

                        $('#puntoEncuprogramacion').val(data.datos['vc_puntoEncuentro']);
                        $('#modalpuntoEncuprogramacion').html(data.datos['vc_puntoEncuentro']);

                        $('#fechaEjecucionReprogramacion').val(data.datos['d_fechaEjecucion']);
                        $('#modalReprogramacionFechaEjecucionP').html(data.datos['d_fechaEjecucion']);

                        $('#horaInicioReprogramacion').val(data.datos['t_horaInicio']);
                        $('#modalReprogramacionHoraInicioP').html(data.datos['t_horaInicio']);

                        $('#horaFinReprogramacion').val(data.datos['t_horaFin']);
                        $('#modalReprogramacionHoraFinP').html(data.datos['t_horaFin']);
                        
                        $('#direccionReprogramacion').val(data.datos['vc_direccion']);
                        $('#modalReprogramacionDireccionEP').html(data.datos['vc_direccion']);

                        $('#escenarioReprogramacion').val(data.datos['vc_escenario']);
                        $('#modalReprogramacionEscenarioEP').html(data.datos['vc_escenario']);

                        $('#codigoReprogramacion').val(data.datos['vc_codigoParque']);
                        $('#modalReprogramacionCodigoIP').html(data.datos['vc_codigoParque']);

                        $('#modalLocalidadEProgramacion').html(data.datos.localidad_escenario['Localidad']);
                        $('#modalUpzEProgramacion').html(data.datos.upz_escenario['Upz']);
                        $('#modalBarrioEProgramacion').html(data.datos.barrio_escenario['Barrio']);

                    }
                );
            }else if(val == 6)
            {

                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }

                $.get(
                    URL+'/ejecucionAprobadaResposable/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeEjecucion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeEjecucion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }
            else if(val == 7)
            {

                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/ejecucionDenegadaResponsable/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeEjecucion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeEjecucion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('warning');

            }else if(val == 8)
            {
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }

                $.get(
                    URL+'/ejecucionCanceladaResponsable/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeEjecucion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeEjecucion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('danger');
            }else if(val == 100)
            {
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }

                $.get(
                    URL+'/habilitarProgramacion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }else if(val == 101)
            {
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/InhabilitarProgramacion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('warning');
            }else if(val == 200)
            {
                if ($tr.hasClass('warning'))
                {
                   $tr.removeClass('warning');
                }
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }

                $.get(
                    URL+'/habilitarEjecucion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeEjecucion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeEjecucion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('success');
            }else if(val == 201)
            {
                if ($tr.hasClass('danger'))
                {
                   $tr.removeClass('danger');
                }
                if ($tr.hasClass('success'))
                {
                   $tr.removeClass('success');
                }

                $.get(
                    URL+'/InhabilitarEjecucion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajeEjecucion').html(data.mensaje);
                        setTimeout(function(){
                            $('#'+id+'mensajeEjecucion').html('');
                        }, 3000);
                    }
                );

                $tr.addClass('warning');
            }


    });


    $('#fechaEjecucionReprogramacion').datetimepicker({
            viewMode: 'years',
            format: 'YYYY-MM-DD'
    });

    $('#horaInicioReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });

    $('#horaFinReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });

    $('#horaImpleReprogramacion').datetimepicker({
        format: 'HH:mm:ss'
    });


    //Carga de las Upzs comunidad
    $('#modalReProgramacion').delegate('select[name="localidad_reprogramacion"]','change',function (e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_Reprogramacion"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Reprogramacion"]').html(html).val($('select[name="Id_Barrio_Reprogramacion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    }; 



     $('#modalReProgramacion').delegate('select[name="Id_Upz_Reprogramacion"]','change',function (e)
    {
        var otherValue=$(this).find('option:selected').attr('data-othervalue');
        selecionar_barrios_comunidad(otherValue,$('select[name="Id_Barrio_Reprogramacion"]'));
    });
    
    var selecionar_barrios_comunidad = function(id, select)
    { 
       $('select[name="Id_Barrio_Reprogramacion"]').html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_barrio/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                select.html(html).val($('select[name="Id_Barrio_Reprogramacion"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data, function(i, eee)
                  {
                            html += '<option value="'+eee['IdBarrio']+'"  >'+eee['Barrio'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', $('select[name="Id_Barrio_Reprogramacion"]').data('value'));
            }
        });
    };



    $('#btn_reprogramar').on('click', function(e)
    {
        var id = $('#idActividadReProgramacion').val();
        $.post(
            URL+'/disponibilidad_reprogramacion',            
            $('#form_agregar_reprogramacion').serialize(),
            function(data)
            {
                var $tr = $('#tbl_confirmarActividad').find('tr[data-row="'+id+'"]');

                if(data.status=="Error")
                {

                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> campos vacios.'+
                    '</div>');
                    $('#mensajeReprogramacion').show();
                    
                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 2000)

                }
                else if(data.status=="Modificado")
                {
                    
                    $tr.hide();
                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-success"><strong>Bien!</strong> Modificacion de la actividad se ha realizado exitosamente. <strong>La actividad pasa al estado reprogramada.</strong>.</div>');
                    $('#mensajeReprogramacion').show();
                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 5000)
                }
                else
                {
                    validador_form_reprogramacion(data.errors);
                    $('#mensajeReprogramacion').html('<div class="alert alert-danger">'+
                      '<strong>Error!</strong> La fecha que intenta registrar debe ser mayor a la fecha de registro mas 15 dias.'+
                    '</div>');
                    $('#mensajeReprogramacion').show();

                    setTimeout(function(){
                          $('#mensajeReprogramacion').hide();
                    }, 2000)
                }
            }
        );

        return false;
    });


    var validador_form_reprogramacion = function(data)
    {
        $('#form_agregar_reprogramacion .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data){
            if (typeof data[error] !== 'function') {
                switch(error)
                {

                    case 'localidad_reprogramacion':
                    case 'Id_Upz_Reprogramacion':
                    case 'Id_Barrio_Reprogramacion':
                        selector = 'select';
                    break;      

                    case 'fechaEjecucionReprogramacion':
                    case 'horaInicioReprogramacion':
                    case 'horaFinReprogramacion':
                    case 'direccionReprogramacion':
                    case 'escenarioReprogramacion':
                        selector = 'input';
                    break;       

                }
                $('#form_agregar_reprogramacion '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


    //EJECUCION


    $('#tbl_resposablePrograma').delegate('button[data-funcion="ejecucionDatos"]','click',function (e){ 
        var id = $(this).data('rel'); 
        $('#id_actividadDatosEjecucion').html(id);

         $('#tdEjecucion').html('');
        $('#tdEjecucion').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

        $('#tdEjecucionRequisitos').html('');
        $('#tdEjecucionRequisitos').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

        $('#tdEjecucionEncuesta').html('');
        $('#tdEjecucionEncuesta').html('<center><p>Cargando...</p><img class="card-img-top" src="../public/Img/loading.gif" alt="Card image cap" style="position: relative;"></center>');

         $.get(
            URL+'/obtenerDatosGestionEjecucion/'+id,
            function(data)
            {
                

                $('#tdEjecucion').html(data.viewEjecuciones);
                $('#tdEjecucionRequisitos').html(data.viewRequisitos);
                $('#tdEjecucionEncuesta').html(data.viewEncuesta);

                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionActividad' ) ) 
                {
                    $('#tbl_datosEjecucionActividad').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionActividad').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }


                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionRequisitos' ) ) 
                {
                    $('#tbl_datosEjecucionRequisitos').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionRequisitos').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }


                if ( $.fn.dataTable.isDataTable( '#tbl_datosEjecucionEncuesta' ) ) 
                {
                    $('#tbl_datosEjecucionEncuesta').DataTable();
                }
                else 
                {
                    $('#tbl_datosEjecucionEncuesta').DataTable( {
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf'],
                        pageLength: 5
                    });
                }
                
            }
        );

    }); 

    $('#tbl_datosEjecucionActividad').delegate('button[data-funcion="eliminar"]','click',function (e) {  

        var id = $(this).data('rel'); 
        var $tr = $('#tbl_datosEjecucionActividad').find('tr[data-row="'+id+'"]');

                $.get(
                    URL+'/eliminarEjecucion/'+id,
                    function(data)
                    {
                        $('#'+id+'mensaje').html(data.mensaje);
                        $tr.addClass('danger');
                        setTimeout(function(){
                            $('#'+id+'mensaje').html('');
                            $tr.css('display', 'none');
                        }, 3000);
                    }
                );
    });

    $('#tbl_datosEjecucionRequisitos').delegate('button[data-funcion="eliminarRequisito"]','click',function (e) {  
        var id = $(this).data('rel'); 
        var $tr = $('#tbl_datosEjecucionRequisitos').find('tr[data-row="'+id+'"]');

                $.get(
                    URL+'/eliminarEjecucionRequerimiento/'+id,
                    function(data)
                    {
                        $('#'+id+'mensajerequerimiento').html(data.mensaje);
                        $tr.addClass('danger');
                        setTimeout(function(){
                            $('#'+id+'mensajerequerimiento').html('');
                            $tr.css('display', 'none');
                        }, 3000);
                    }
                );
    });


});