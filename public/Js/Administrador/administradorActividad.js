$(function () {
    $('#datetimepicker1').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    $('#datetimepicker2').datetimepicker({
    	format: 'YYYY-MM-DD'
    });
    var URL = $('#main_actividad').data('url');

 

    $('#btn_buscar_Actividades').on('click', function(e)
    {

    	$('.fechaInicioHiden').val($('#fechaInicio').val());
    	$('.fechaFinHiden').val($('#fechaFin').val());
        $('.idActHiden').val($('#idAct').val());
        $('.Cod_IDRDHiden').val($('#Cod_IDRD').val());
        $('.localidad_comunidadHiden').val($('#localidad_comunidad').val());
        $('.Id_Upz_ComunidadHiden').val($('#Id_Upz_Comunidad').val());
        $('.localidad_EscenarioHiden').val($('#localidad_Escenario').val());
        $('.Id_Upz_EscenarioHiden').val($('#Id_Upz_Escenario').val());
        $('.gestorHiden').val($('#gestor').val());
        $('.resposanbleHiden').val($('#resposanble').val());

        $("#resultadoBusqueda").hide();
        $.post(
            URL+'/adminBusquedaActividad',            
            $('#form_consulta_actividades').serialize(),
            function(data)
            {
                if(data.status == 'error')
                {
                    validador_datos(data.errors);
                }
                else
                {
                    validador_datos(data.errors);

                    //programacion
                	$('#label-1').html(data.datos.ProgramacionIncompleta);
                    
                    if(data.datos.ProgramacionIncompleta>0)
                        $('#card1').css({background:"#b0ec6e38"});
                    else
                        $('#card1').css({background:"#ffffff"});

					$('#label0').html(data.datos.ProgramacionPendiente);
                    if(data.datos.ProgramacionPendiente>0)
                        $('#card2').css({background:"#b0ec6e38"});
                    else
                        $('#card2').css({background:"#ffffff"});

					$('#label1').html(data.datos.ProgramacionAprobado);
                    if(data.datos.ProgramacionAprobado>0)
                        $('#card3').css({background:"#b0ec6e38"});
                    else
                        $('#card3').css({background:"#ffffff"});

                    $('#label2').html(data.datos.ProgramacionDevuelto);
                    if(data.datos.ProgramacionDevuelto>0)
                        $('#card4').css({background:"#b0ec6e38"});
                    else
                        $('#card4').css({background:"#ffffff"});

                    $('#label3').html(data.datos.ProgramacionCancelado);
                    if(data.datos.ProgramacionCancelado>0)
                        $('#card5').css({background:"#b0ec6e38"});
                    else
                        $('#card5').css({background:"#ffffff"});


                    //confirmacion
                    $('#label44').html(data.datos.ConfirmacionConfirmadoSinAprobacion);
                    if(data.datos.ConfirmacionConfirmadoSinAprobacion>0)
                        $('#card6').css({background:"#b0ec6e38"});
                    else
                        $('#card6').css({background:"#ffffff"});

                    $('#label4').html(data.datos.ConfirmacionConfirmado);
                    if(data.datos.ConfirmacionConfirmado>0)
                        $('#card7').css({background:"#b0ec6e38"});
                    else
                        $('#card7').css({background:"#ffffff"});

                    

                    $('#label5').html(data.datos.ConfirmacionCancelada);
                    if(data.datos.ConfirmacionCancelada>0)
                        $('#card8').css({background:"#b0ec6e38"});
                    else
                        $('#card8').css({background:"#ffffff"});

					$('#label6').html(data.datos.ConfirmacionReprogramada);
                    if(data.datos.ConfirmacionReprogramada>0)
                        $('#card9').css({background:"#b0ec6e38"});
                    else
                        $('#card9').css({background:"#ffffff"});


                    //Ejecucion
                    $('#label70').html(data.datos.EjecucionSinAproProgramacion);
                    if(data.datos.EjecucionSinAproProgramacion>0)
                        $('#card10').css({background:"#b0ec6e38"});
                    else
                        $('#card10').css({background:"#ffffff"});

                    $('#label71').html(data.datos.EjecucionSinConfirmacion);
                    if(data.datos.EjecucionSinConfirmacion>0)
                        $('#card11').css({background:"#b0ec6e38"});
                    else
                        $('#card11').css({background:"#ffffff"});

                    $('#label72').html(data.datos.EjecucionSinAproProgramacionNiConfirm);
                    if(data.datos.EjecucionSinAproProgramacionNiConfirm>0)
                        $('#card12').css({background:"#b0ec6e38"});
                    else
                        $('#card12').css({background:"#ffffff"});

                    $('#label7').html(data.datos.EjecucionEjecutada);
                    if(data.datos.EjecucionEjecutada>0)
                        $('#card13').css({background:"#b0ec6e38"});
                    else
                        $('#card13').css({background:"#ffffff"});

                    $('#label8').html(data.datos.EjecucionCancelada);
                    if(data.datos.EjecucionCancelada>0)
                        $('#card14').css({background:"#b0ec6e38"});
                    else
                        $('#card14').css({background:"#ffffff"});

                    $('#label9').html(data.datos.EjecucionAprobada);
                    if(data.datos.EjecucionAprobada>0)
                        $('#card15').css({background:"#b0ec6e38"});
                    else
                        $('#card15').css({background:"#ffffff"});

                    $('#label10').html(data.datos.EjecucionCanceladaEjecutada);
                    if(data.datos.EjecucionCanceladaEjecutada>0)
                        $('#card16').css({background:"#b0ec6e38"});
                    else
                        $('#card16').css({background:"#ffffff"});

                    $('#label11').html(data.datos.EjecucionDenegada);
                    if(data.datos.EjecucionDenegada>0)
                        $('#card17').css({background:"#b0ec6e38"});
                    else
                        $('#card17').css({background:"#ffffff"});

                    $('#labelTotal').html(data.datos.ActividadesTotal);

                	$("#resultadoBusqueda").show();

                }
                
            }
        );
        return false;
    });

    var validador_datos = function(data)
    {
        $('#form_consulta_actividades .form-group').removeClass('has-error');
        var selector = '';
        for (var error in data)
        {
            if (typeof data[error] !== 'function') 
            {
                switch(error)
                {
                    case 'fechaInicio':
                    case 'fechaFin':
                        selector = 'input';
                    break;               
                }
                $('#form_consulta_actividades '+selector+'[name="'+error+'"]').closest('.form-group').addClass('has-error');
            }
        }
    }


     //Carga de las Upzs comunidad
    $('select[name="localidad_comunidad"]').on('change', function(e)
    {
        selecionar_upz_comunidad($(this).val(),$('select[name="Id_Upz_Comunidad"]'));
    });

    var selecionar_upz_comunidad = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_admin/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Comunidad"]').html(html).val($('select[name="Id_Barrio_Comunidad"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };


    //Carga de las Upzs comunidad
    $('select[name="localidad_Escenario"]').on('change', function(e)
    {
        selecionar_upz_escenario($(this).val(),$('select[name="Id_Upz_Escenario"]'));
    });

    var selecionar_upz_escenario = function(id,select)
    { 
        select.html('<option value="">Cargando...</option>');
        $.ajax({
            url: URL+'/select_upz_admin/'+id,
            data: {},
            dataType: 'json',
            success: function(data)
            {
                var html = '<option value="">Seleccionar</option>'; 
                $('select[name="Id_Barrio_Comunidad"]').html(html).val($('select[name="Id_Barrio_Comunidad"]').data('value'));

                  var html = '<option value="">Seleccionar</option>';
                  $.each(data.upzs, function(i, eee)
                  {
                            html += '<option value="'+eee['cod_upz']+'" data-othervalue="'+eee['cod_upz']+'">'+eee['Upz'].toUpperCase()+'</option>';
                  });   
                  select.html(html);
                  select.selectpicker('refresh');
                  select.selectpicker('val', select.data('value'));
            }
        });
    };


});