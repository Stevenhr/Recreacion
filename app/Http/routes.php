<?php

/*
|--------------------------------------------------------------------------
| Routes Usuarios
|--------------------------------------------------------------------------
|Rutas para la gestion de los usuarios.
*/
Route::get('/personas', '\Idrd\Usuarios\Controllers\PersonaController@index');
Route::get('/personas/service/obtener/{id}', '\Idrd\Usuarios\Controllers\PersonaController@obtener');
Route::get('/personas/service/buscar/{key}', '\Idrd\Usuarios\Controllers\PersonaController@buscar');
Route::get('/personas/service/ciudad/{id_pais}', '\Idrd\Usuarios\Controllers\LocalizacionController@buscarCiudades');
Route::post('/personas/service/procesar/', '\Idrd\Usuarios\Controllers\PersonaController@procesar');

Route::get('/actividad_usuario/{identificacion?}', function ($identificacion = null) {
    return view('idrd.usuarios.persona_actividades', [
        'seccion' => 'Actividades',
        'identificacion' => $identificacion
    ]);
});

Route::get('/usuario_tipo', function () { return view('persona_tipoPersona'); });
Route::get('/asignarActividad', '\Idrd\Usuarios\Controllers\AsignarActividadController@asignarActividades');
Route::get('/actividadesModulo', '\Idrd\Usuarios\Controllers\AsignarActividadController@moduloActividades');
Route::get('/actividadesPersona/{id}', '\Idrd\Usuarios\Controllers\AsignarActividadController@personaActividades');
Route::any('PersonasActividadesProceso', '\Idrd\Usuarios\Controllers\AsignarActividadController@PersonasActividadesProceso');
Route::get('/parques/service/buscar/{key}', '\Idrd\Parques\Controllers\ParqueController@buscar');


Route::any('/', 'MainController@index');
Route::any('/logout', 'MainController@logout');





//rutas con filtro de autenticación
Route::group(['middleware' => ['web']], function () {
	
/*
|--------------------------------------------------------------------------
| Routes Actividad
|--------------------------------------------------------------------------
|Rutas para la gestion de los usuarios.
*/
Route::get('/welcome', 'MainController@welcome');

Route::group(['prefix' => 'actividad', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/registroActividad', [
        'uses' => $controller.'ActividadController@inicio',
        'as' => 'proceso_actividad'
    ]);

    Route::get('select_actividad/{id}',[
        'uses' => $controller.'ActividadController@select_actividad',
        'as' => 'selecionar_actividad'
    ]);

    Route::get('select_tematica/{id}',[
        'uses' => $controller.'ActividadController@select_tematica',
        'as' => 'selecionar_tematica'
    ]);

    Route::get('select_componente/{id}',[
        'uses' => $controller.'ActividadController@select_componente',
        'as' => 'selecionar_componente'
    ]);

    Route::get('select_upz/{id}',[
        'uses' => $controller.'ActividadController@select_upz',
        'as' => 'selecionar_upz'
    ]);

    Route::get('select_barrio/{id}',[
        'uses' => $controller.'ActividadController@select_barrio',
        'as' => 'selecionar_barrio'
    ]);

    Route::get('select_caracteristicas_especificas_poblacion/{id}',[
        'uses' => $controller.'ActividadController@select_caracteristicas_especificas_poblacion',
        'as' => 'seleccionar_caracteristicas_especificas_poblacion'
    ]);

    Route::post('disponibilidad_acopanante',[
        'uses' => $controller.'ActividadController@disponibilidad_acopanante',
        'as' => 'disponibilidad_acopanante'
    ]);

    Route::post('validaPasos',[
        'uses' => $controller.'ActividadController@validaPasos',
        'as' => 'validaPasos'
    ]);

     Route::post('validarDatosActividad',[
        'uses' => $controller.'ActividadController@validarDatosActividad',
        'as' => 'validadatosactividad'
    ]);

    Route::get('eliminarDatosActividad/{id}',[
        'uses' => $controller.'ActividadController@eliminarDatosActividad',
        'as' => 'eliminadatosactividad'
    ]);

    Route::get('validardatosactividadregistrados/{id}',[
        'uses' => $controller.'ActividadController@validardatosactividadregistrados',
        'as' => 'validardatosactividadregistrados'
    ]);

    Route::post('validardatosactividadregistradosPasoIII',[
        'uses' => $controller.'ActividadController@validardatosactividadregistradospasoIII',
        'as' => 'validardatosactividadregistradosPasoIII'
    ]);

    Route::post('validardatosactividadregistradosPasoIV',[
        'uses' => $controller.'ActividadController@validardatosactividadregistradosPasoIV',
        'as' => 'validardatosactividadregistradosPasoIV'
    ]);

    Route::post('registroActividadPasoV',[
        'uses' => $controller.'ActividadController@registroActividadPasoV',
        'as' => 'registroActividadPasoV'
    ]);

    Route::post('getCaracterisiticasEspecificas',[
        'uses' => $controller.'ActividadController@getCaracterisiticasEspecificas',
        'as' => 'getCaracterisiticasEspecificas'
    ]);

    Route::get('service/buscar/{id}',[
        'uses' => $controller.'ActividadController@buscar',
        'as' => 'buscarDatosParques'
    ]);

    Route::post('getAcompananteLocalidad',[
        'uses' => $controller.'ActividadController@getAcompananteLocalidad',
        'as' => 'getAcompananteLocalidad'
    ]);

    Route::post('getAcompananteOtraLocalidad',[
        'uses' => $controller.'ActividadController@getAcompananteOtraLocalidad',
        'as' => 'getAcompananteOtraLocalidad'
    ]);

    Route::post('setAcompanante',[
        'uses' => $controller.'ActividadController@setAcompanante',
        'as' => 'setAcompanante'
    ]);

    Route::post('getValidacionAcomp',[
        'uses' => $controller.'ActividadController@getValidacionAcomp',
        'as' => 'getValidacionAcomp'
    ]);

});


Route::group(['prefix' => 'misActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/mis_actividades', [
        'uses' => $controller.'MisActividadesController@inicio',
        'as' => 'mis_actividades'
    ]);

    Route::any('/busquedaActividad', [
        'uses' => $controller.'MisActividadesController@busquedaActividad',
        'as' => 'busquedaActividad'
    ]);

    Route::any('/actividadesResposableProgramaPendientes', [
        'uses' => $controller.'MisActividadesController@actividadesResposableProgramaPendientes',
        'as' => 'actividadesResposableProgramaPendientes'
    ]);

    Route::get('datosprogramacionactividad/{id}',[
        'uses' => $controller.'MisActividadesController@datosprogramacionactividad',
        'as' => 'datosprogramacionactividad'
    ]);



    Route::get('actividadAprobada/{id}/{mensaje}',[
        'uses' => $controller.'MisActividadesController@actividadAprobada',
        'as' => 'actividadAprobada'
    ]);

    Route::get('actividadDevuelta/{id}/{mensaje}',[
        'uses' => $controller.'MisActividadesController@actividadDevuelta',
        'as' => 'actividadDevuelta'
    ]);

    Route::get('actividadCancelada/{id}/{mensaje}',[
        'uses' => $controller.'MisActividadesController@actividadCancelada',
        'as' => 'actividadCancelada'
    ]);

});


Route::group(['prefix' => 'administracionActividad', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Administrador\\Controllers\\";

    Route::any('/administracionActividad', [
        'uses' => $controller.'AdministracionActividadController@inicio',
        'as' => 'administracionActividad'
    ]);


    Route::any('/adminBusquedaActividad', [
        'uses' => $controller.'AdministracionActividadController@adminBusquedaActividad',
        'as' => 'adminBusquedaActividad'
    ]);


    Route::any('/actividadesFiltros', [
        'uses' => $controller.'AdministracionActividadController@actividadesFiltros',
        'as' => 'actividadesFiltros'
    ]);



    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('select_upz_admin/{id}',[
        'uses' => $controller.'ActividadController@select_upz',
        'as' => 'select_upz_admin'
    ]);


});


Route::group(['prefix' => 'tablaAdministradorActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Administrador\\Controllers\\";

    Route::get('confirmarActivida/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@confirmarActivida',
        'as' => 'confirmarActivida'
    ]);

    Route::get('noConfirmarActividad/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@noConfirmarActividad',
        'as' => 'noConfirmarActividad'
    ]);


    Route::get('buscarActividad/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@buscarActividad',
        'as' => 'buscarActividad'
    ]);

    Route::get('actividadAprobada/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@actividadAprobada',
        'as' => 'actividadAprobada'
    ]);

    Route::get('actividadDevuelta/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@actividadDevuelta',
        'as' => 'actividadDevuelta'
    ]);

    Route::get('actividadCancelada/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@actividadCancelada',
        'as' => 'actividadCancelada'
    ]);

    Route::post('disponibilidad_reprogramacion',[
        'uses' => $controller.'TablaAdministradorActividadesController@disponibilidad_reprogramacion',
        'as' => 'disponibilidad_reprogramacion'
    ]);

    Route::get('habilitarProgramacion/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@habilitarProgramacion',
        'as' => 'habilitarProgramacion'
    ]);

    Route::get('InhabilitarProgramacion/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@InhabilitarProgramacion',
        'as' => 'InhabilitarProgramacion'
    ]);

    Route::get('habilitarEjecucion/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@habilitarEjecucion',
        'as' => 'habilitarEjecucion'
    ]);

    Route::get('InhabilitarEjecucion/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@InhabilitarEjecucion',
        'as' => 'InhabilitarEjecucion'
    ]);

    Route::get('ejecucionAprobadaResposable/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@ejecucionAprobadaResposable',
        'as' => 'ejecucionAprobadaResposable'
    ]);

    Route::get('ejecucionDenegadaResponsable/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@ejecucionDenegadaResponsable',
        'as' => 'ejecucionDenegadaResponsable'
    ]);

    Route::get('ejecucionCanceladaResponsable/{id}',[
        'uses' => $controller.'TablaAdministradorActividadesController@ejecucionCanceladaResponsable',
        'as' => 'ejecucionCanceladaResponsable'
    ]);

    $controller1 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('select_upz/{id}',[
        'uses' => $controller1.'ConfirmarActividadesController@select_upz',
        'as' => 'selecionar_upz'
    ]);

    Route::get('select_barrio/{id}',[
        'uses' => $controller1.'ConfirmarActividadesController@select_barrio',
        'as' => 'selecionar_barrio'
    ]);

    $controller2 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('obtenerDatosGestionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosGestionEjecucion'
    ]);  

    $controller4 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('datosprogramacionactividad/{id}',[
        'uses' => $controller4.'MisActividadesController@datosprogramacionactividad',
        'as' => 'datosprogramacionactividad'
    ]);

    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::post('registro_poblacional',[
        'uses' => $controller.'EjecutarActividadesController@registro_poblacional',
        'as' => 'registro_poblacional'
    ]);

    Route::post('registro_requisitos',[
        'uses' => $controller.'EjecutarActividadesController@registro_requisitos',
        'as' => 'registro_requisitos'
    ]);

    Route::post('registro_encuesta',[
        'uses' => $controller.'EjecutarActividadesController@registro_encuesta',
        'as' => 'registro_encuesta'
    ]);

    Route::get('eliminarEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucion',
        'as' => 'eliminarEjecucion'
    ]);

    Route::get('eliminarEjecucionRequerimiento/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucionRequerimiento',
        'as' => 'eliminarEjecucionRequerimiento'
    ]);


});



Route::group(['prefix' => 'confirmarActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/confirmar_actividades', [
        'uses' => $controller.'ConfirmarActividadesController@inicio',
        'as' => 'confirmar_actividades'
    ]);

    Route::any('/confirmarBusquedaActividad', [
        'uses' => $controller.'ConfirmarActividadesController@confirmarBusquedaActividad',
        'as' => 'confirmarBusquedaActividad'
    ]);

     Route::any('/actividadesConfirmarResponsable', [
        'uses' => $controller.'ConfirmarActividadesController@actividadesConfirmarResponsable',
        'as' => 'actividadesConfirmarResponsable'
    ]);

    Route::get('actividadConfirmada/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@actividadConfirmada',
        'as' => 'actividadConfirmada'
    ]);

    Route::get('datosprogramacionactividadConfirmar/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@datosprogramacionactividadConfirmar',
        'as' => 'datosprogramacionactividadConfirmar'
    ]);

    Route::get('agregarObservacionConfirmacion/{id}/{mensaje}',[
        'uses' => $controller.'ConfirmarActividadesController@agregarObservacionConfirmacion',
        'as' => 'agregarObservacionConfirmacion'
    ]);


    Route::get('confirmarActivida/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@confirmarActivida',
        'as' => 'confirmarActivida'
    ]);

    Route::get('noConfirmarActividad/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@noConfirmarActividad',
        'as' => 'noConfirmarActividad'
    ]);


    Route::get('buscarActividad/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@buscarActividad',
        'as' => 'buscarActividad'
    ]);


    Route::get('select_upz/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@select_upz',
        'as' => 'selecionar_upz'
    ]);

    Route::get('select_barrio/{id}',[
        'uses' => $controller.'ConfirmarActividadesController@select_barrio',
        'as' => 'selecionar_barrio'
    ]);

    Route::post('disponibilidad_reprogramacion',[
        'uses' => $controller.'ConfirmarActividadesController@disponibilidad_reprogramacion',
        'as' => 'disponibilidad_reprogramacion'
    ]);

});


Route::group(['prefix' => 'ejecutarActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/ejecutar_actividades', [
        'uses' => $controller.'EjecutarActividadesController@inicio',
        'as' => 'ejecutar_actividades'
    ]);

    Route::any('/ejecutarBusquedaActividad', [
        'uses' => $controller.'EjecutarActividadesController@ejecutarBusquedaActividad',
        'as' => 'ejecutarBusquedaActividad'
    ]);

    Route::any('/actividadesPorEjecutarResponsable', [
        'uses' => $controller.'EjecutarActividadesController@actividadesPorEjecutarResponsable',
        'as' => 'actividadesPorEjecutarResponsable'
    ]);

    Route::get('datosprogramacionactividadEjecutar/{id}',[
        'uses' => $controller.'EjecutarActividadesController@datosprogramacionactividadEjecutar',
        'as' => 'datosprogramacionactividadEjecutar'
    ]);

    Route::get('select_upz_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_upz_ejecucion',
        'as' => 'select_upz_ejecucion'
    ]);

    Route::get('select_barrio_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_barrio_ejecucion',
        'as' => 'select_barrio_ejecucion'
    ]);

    Route::post('registro_poblacional',[
        'uses' => $controller.'EjecutarActividadesController@registro_poblacional',
        'as' => 'registro_poblacional'
    ]);

    Route::post('registro_requisitos',[
        'uses' => $controller.'EjecutarActividadesController@registro_requisitos',
        'as' => 'registro_requisitos'
    ]);

    Route::post('registro_encuesta',[
        'uses' => $controller.'EjecutarActividadesController@registro_encuesta',
        'as' => 'registro_encuesta'
    ]);

    Route::get('obtenerDatosEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosEjecucion'
    ]);

    Route::get('obtenerDatosProgramacion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@obtenerDatosProgramacion',
        'as' => 'obtenerDatosProgramacion'
    ]);

    Route::get('eliminarEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucion',
        'as' => 'eliminarEjecucion'
    ]);

    Route::get('eliminarEjecucionRequerimiento/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucionRequerimiento',
        'as' => 'eliminarEjecucionRequerimiento'
    ]);

});


Route::group(['prefix' => 'jornadasActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Jornadas\\Controllers\\";

    Route::any('/jornadas_actividades', [
        'uses' => $controller.'JornadaController@inicio',
        'as' => 'jornadas_actividades'
    ]);

    Route::post('busquedaJornada',[
        'uses' => $controller.'JornadaController@busquedaJornada',
        'as' => 'busquedaJornada'
    ]);
});


Route::group(['prefix' => 'jornadasActividadesusuario', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Jornadas\\Controllers\\";

    Route::any('/jornadas_actividades_usuario', [
        'uses' => $controller.'JornadaControllerUsuario@inicio',
        'as' => 'jornadas_actividades_usuario'
    ]);

    Route::post('busquedaJornadaUsuario',[
        'uses' => $controller.'JornadaControllerUsuario@busquedaJornadaUsuario',
        'as' => 'busquedaJornadaUsuario'
    ]);
});


Route::group(['prefix' => 'misProgramacionesCreadas', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/programacion_creada', [
        'uses' => $controller.'MisProgramacionesCreadasController@inicio',
        'as' => 'programacion_creada'
    ]);

    Route::post('tablaActividadCreada',[
        'uses' => $controller.'MisProgramacionesCreadasController@tablaActividadCreada',
        'as' => 'tablaActividadCreada'
    ]);

    Route::get('datosprogramacionactividad/{id}',[
        'uses' => $controller.'MisProgramacionesCreadasController@datosprogramacionactividad',
        'as' => 'datosprogramacionactividad'
    ]);

    Route::get('agregarObservacionProgramacion/{id}/{mensaje}',[
        'uses' => $controller.'MisProgramacionesCreadasController@agregarObservacionProgramacion',
        'as' => 'agregarObservacionProgramacion'
    ]);
    
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('elimina/{id}', [
        'uses' => $controller.'ActividadController@elimina',
        'as' => 'elimina'
    ]);

});


Route::group(['prefix' => 'habbilitacionActividades', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::any('/habilitacion_actividades', [
        'uses' => $controller.'ActividadesHabilitadasController@inicio',
        'as' => 'habilitacion_actividades'
    ]);

    Route::post('tablaActividadHabilitadas',[
        'uses' => $controller.'ActividadesHabilitadasController@tablaActividadHabilitadas',
        'as' => 'tablaActividadHabilitadas'
    ]);

    Route::get('select_upz_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_upz_ejecucion',
        'as' => 'select_upz_ejecucion'
    ]);

    Route::get('select_barrio_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_barrio_ejecucion',
        'as' => 'select_barrio_ejecucion'
    ]);

    Route::post('registro_requisitos',[
        'uses' => $controller.'EjecutarActividadesController@registro_requisitos',
        'as' => 'registro_requisitos'
    ]);

    Route::post('registro_encuesta',[
        'uses' => $controller.'EjecutarActividadesController@registro_encuesta',
        'as' => 'registro_encuesta'
    ]);

    Route::get('obtenerDatosEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosEjecucion'
    ]);
    

    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::post('registro_poblacional',[
        'uses' => $controller.'EjecutarActividadesController@registro_poblacional',
        'as' => 'registro_poblacional'
    ]);

    Route::get('eliminarEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucion',
        'as' => 'eliminarEjecucion'
    ]);

    Route::get('eliminarEjecucionRequerimiento/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucionRequerimiento',
        'as' => 'eliminarEjecucionRequerimiento'
    ]);

});

Route::group(['prefix' => '', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";

    Route::post('modificar', [
        'uses' => $controller.'ActividadController@modifica',
        'as' => 'modificar'
    ]);

    Route::get('elimina/{id}', [
        'uses' => $controller.'ActividadController@elimina',
        'as' => 'elimina'
    ]);

});

Route::group(['prefix' => 'miGestionEjecucionGestor', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Ejecucion\\Controllers\\";

    Route::any('/gestionMisEjecucionesGestor', [
        'uses' => $controller.'MiGestionEjecucionController@inicio',
        'as' => 'gestionMisEjecucionesGestor'
    ]);

    Route::any('/busquedaMiGestionEjecuciones', [
        'uses' => $controller.'MiGestionEjecucionController@busquedaMiGestionEjecuciones',
        'as' => 'busquedaMiGestionEjecuciones'
    ]);

    Route::any('/ejecucionesGestion', [
        'uses' => $controller.'MiGestionEjecucionController@ejecucionesGestion',
        'as' => 'ejecucionesGestion'
    ]);

    $controller2 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('obtenerDatosGestionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosGestionEjecucion'
    ]);

    Route::get('datosProgramacionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@datosprogramacionactividadEjecutar',
        'as' => 'datosProgramacionEjecucion'
    ]);


});



Route::group(['prefix' => 'miGestionEjecucionResponsable', 'middleware' => 'auth'], function()
{
    $controller = "\\App\\Modulos\\Ejecucion\\Controllers\\";

    Route::any('/gestionMisEjecucionesResposanble', [
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@inicio',
        'as' => 'gestionMisEjecucionesResposanble'
    ]);

    Route::any('/busquedaMiGestionEjecucionesResponsable', [
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@busquedaMiGestionEjecuciones',
        'as' => 'busquedaMiGestionEjecucionesResponsable'
    ]);

    Route::any('/ejecucionesGestionResponsable', [
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@ejecucionesGestion',
        'as' => 'ejecucionesGestionResponsable'
    ]);

    $controller2 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('obtenerDatosGestionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosGestionEjecucion'
    ]);

    Route::get('datosProgramacionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@datosprogramacionactividadEjecutar',
        'as' => 'datosProgramacionEjecucion'
    ]);

});


Route::group(['prefix' => 'gestionEjecucion', 'middleware' => 'auth'], function()
{
   $controller = "\\App\\Modulos\\Ejecucion\\Controllers\\";
   Route::get('ejecucionAprobadaResposable/{id}/{mensaje}',[
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@ejecucionAprobadaResposable',
        'as' => 'ejecucionAprobadaResposable'
    ]);

    Route::get('ejecucionDenegadaResponsable/{id}/{mensaje}',[
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@ejecucionDenegadaResponsable',
        'as' => 'ejecucionDenegadaResponsable'
    ]);

    Route::get('ejecucionCanceladaResponsable/{id}/{mensaje}',[
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@ejecucionCanceladaResponsable',
        'as' => 'ejecucionCanceladaResponsable'
    ]);

    Route::get('agregarObservacionEjecucion/{id}/{mensaje}',[
        'uses' => $controller.'MiGestionEjecucionControllerResponsble@agregarObservacionEjecucion',
        'as' => 'agregarObservacionEjecucion'
    ]);

     $controller2 = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('obtenerDatosGestionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@obtenerDatosEjecucion',
        'as' => 'obtenerDatosGestionEjecucion'
    ]);

     $controller = "\\App\\Modulos\\Ejecucion\\Controllers\\";
    Route::get('datosProgramacionEjecucion/{id}',[
        'uses' => $controller2.'EjecutarActividadesController@datosprogramacionactividadEjecutar',
        'as' => 'datosProgramacionEjecucion'
    ]);

    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('obtenerDatosProgramacion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@obtenerDatosProgramacion',
        'as' => 'obtenerDatosProgramacion'
    ]);

    Route::get('select_upz_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_upz_ejecucion',
        'as' => 'select_upz_ejecucion'
    ]);

    Route::get('select_barrio_ejecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@select_barrio_ejecucion',
        'as' => 'select_barrio_ejecucion'
    ]);

    $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::post('registro_poblacional',[
        'uses' => $controller.'EjecutarActividadesController@registro_poblacional',
        'as' => 'registro_poblacional'
    ]);

    Route::post('registro_requisitos',[
        'uses' => $controller.'EjecutarActividadesController@registro_requisitos',
        'as' => 'registro_requisitos'
    ]);

    Route::post('registro_encuesta',[
        'uses' => $controller.'EjecutarActividadesController@registro_encuesta',
        'as' => 'registro_encuesta'
    ]);

    Route::get('eliminarEjecucion/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucion',
        'as' => 'eliminarEjecucion'
    ]);

    Route::get('eliminarEjecucionRequerimiento/{id}',[
        'uses' => $controller.'EjecutarActividadesController@eliminarEjecucionRequerimiento',
        'as' => 'eliminarEjecucionRequerimiento'
    ]);


});


Route::group(['prefix' => 'usuarios', 'middleware' => 'auth'], function()
{
    Route::get('distribuir', '\App\Modulos\Usuario\Controllers\DistribucionController@index');
    Route::post('asignarRol', '\App\Modulos\Usuario\Controllers\DistribucionController@asignarRol');
    Route::post('cargarRol', '\App\Modulos\Usuario\Controllers\DistribucionController@cargarRol');
    Route::post('cargarRoles', '\App\Modulos\Usuario\Controllers\DistribucionController@cargarRoles');
});



Route::group(['prefix' => 'reportes', 'middleware' => 'auth'], function()
{
   $controller = "\\App\\Modulos\\Reportes\\Controllers\\";
   Route::get('general',[
        'uses' => $controller.'ReporteGeneralController@inicio',
        'as' => 'general'
    ]);

   Route::any('/reporteBusquedaActividades', [
        'uses' => $controller.'ReporteGeneralController@reporteBusquedaActividades',
        'as' => 'reporteBusquedaActividades'
    ]);

   $controller = "\\App\\Modulos\\ActividadRecreativa\\Controllers\\";
    Route::get('select_upz_admin/{id}',[
        'uses' => $controller.'ActividadController@select_upz',
        'as' => 'select_upz_admin'
    ]);

});


});