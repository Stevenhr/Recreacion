<?php

namespace App\Modulos\Usuario\Controllers;

use App\Http\Controllers\Controller;
use App\Localidad;
use App\Modulos\Programa\Programa;
use App\Modulos\Configuracion\Configuracion;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Persona;
use Idrd\Usuarios\Repo\PersonaInterface;
use Illuminate\Http\Request;

class DistribucionController extends Controller {

    protected $Usuario;
    protected $repositorio_personas;

    public function __construct(PersonaInterface $repositorio_personas)
    {
        if (isset($_SESSION['Usuario']))
            $this->Usuario = $_SESSION['Usuario'];

        $this->repositorio_personas = $repositorio_personas;
    }

    public function index()
    {
        $data = [
            'perfiles' => Configuracion::getPerfilesForSelect(),
            'localidades' => Localidad::all(),
            'programa'=>Programa::all()
        ];

        return view('idrd.usuarios.distribuir', $data);
    }

    public function asignarRol(Request $request)
    {
        
        if($request->input('id_perfil')==1 || $request->input('id_perfil')==2 )
        {  
            $configuraciones = ConfiguracionPersona::where('i_fk_id_persona', $request->input('id_persona'))
                              ->where('i_id_tipo_persona', $request->input('id_perfil'));

            $configuraciones->delete();

            foreach ($request->input('localidades') as $localidad)
            {
                $configuracion = new ConfiguracionPersona;
                $configuracion->i_fk_id_persona = $request->input('id_persona');
                $configuracion->i_id_tipo_persona = $request->input('id_perfil');
                $configuracion->i_id_localidad = $localidad;
                $configuracion->save();
            }
        }

        if($request->input('id_perfil')==3 || $request->input('id_perfil')==4 )
        {
            $configuraciones = ConfiguracionPersona::where('i_fk_id_persona', $request->input('id_persona'))
                              ->where('i_id_tipo_persona', $request->input('id_perfil'));

            $configuraciones->delete();

            foreach ($request->input('localidades') as $programa)
            {
                $configuracion = new ConfiguracionPersona;
                $configuracion->i_fk_id_persona = $request->input('id_persona');
                $configuracion->i_id_tipo_persona = $request->input('id_perfil');
                $configuracion->i_fk_programa = $programa;
                $configuracion->save();
            }
        }

        return response()->json(['status' => 'success']);
    }

    public function cargarRol(Request $request)
    {
        $configuraciones = ConfiguracionPersona::with('localidad')
                                ->where('i_fk_id_persona', $request->input('id_persona'))
                                ->where('i_id_tipo_persona', $request->input('id_perfil'))
                                ->get();

        return response()->json($configuraciones);
    }

    public function cargarRoles(Request $request)
    {
        $configuraciones = ConfiguracionPersona::with('localidad','persona','programa')
                                ->where('i_fk_id_persona', $request->input('id_persona'))
                                ->get();

        return response()->json($configuraciones);
    }
}