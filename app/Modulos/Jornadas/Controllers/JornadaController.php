<?php

namespace App\Modulos\Jornadas\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Localidad;
use App\Persona;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;
use Carbon;
use DateTime;

class JornadaController extends Controller
{
    //
    public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		return view('Jornada.jornada');
	}

	public function busquedaJornada(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
			return $this->tablaActividadCreada($request->all());
        }
    }


    public function tablaActividadCreada($request)
	{
		
		$actividades = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates')
		->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])
		->orWhere('i_estadoAprobaEjecucion','=',Configuracion::EJECUCION_APROBADA)
		->where('i_fk_usuario',$_SESSION['Usuario'][0])
		->orderBy('d_fechaEjecucion', 'asc')
		->orderBy('t_horaInicio', 'asc')
		->get();
		
		$opcion="";
		$color="";
		$style="";
		

				$tabla='';
		            $num=1;
		            if($actividades!=''){
		                foreach ($actividades as $actividad) {
		                    	
		                    	$tabla=$tabla.'<tr class="something" data-row="'.$actividad['i_pk_id'].'">';
			                        $tabla=$tabla.'<td class="col-md-1">'.$num.'</td>';
			                        $tabla=$tabla.'<td class="col-md-13"><b><p class="text-info text-center" style="font-size: 15px">'.$actividad['i_pk_id'].' </p></b></td>';
			                        $tabla=$tabla.'<td >'.$actividad['d_fechaEjecucion'].'</td>';
			                        $tabla=$tabla.'<td>'.$actividad['t_horaInicio'].'</td>';
			                        $tabla=$tabla.'<td>'.$actividad['t_horaFin'].'</td>';

			                        	
				                       
								
										//Jornada mañana
										$Intervalo11 = strtotime('2009-01-31'.' 04:00:00');
										$Intervalo12 = strtotime('2009-01-31'.' 12:00:00');
										$jornadaMan=0;
										$horaInicio = strtotime('2009-01-31'.$actividad['t_horaInicio']);
										$horaFin =strtotime('2009-01-31'.$actividad['t_horaFin']);

										
										if($horaInicio>=$Intervalo11  && $horaFin<=$Intervalo12){
											
											$horaInicio=date('H:i:s',$horaInicio);
											$horaFin=date('H:i:s',$horaFin);

											$horaInicio1 = new \Carbon\Carbon($horaInicio);
											$horaFin1 =new \Carbon\Carbon($horaFin);
											$jornadaMan=$horaInicio1->diffInHours($horaFin1);
										}else if($horaInicio<$Intervalo11  && $horaFin<=$Intervalo12){
											if($horaFin>$Intervalo11){
												$Intervalo=date('H:i:s',$Intervalo11);
												$horaFin=date('H:i:s',$horaFin);

												$Intervalo1 = new \Carbon\Carbon($Intervalo);
												$horaFin1 =new \Carbon\Carbon($horaFin);
												$jornadaMan=$Intervalo1->diffInHours($horaFin1);
											}
										}else if($horaInicio>=$Intervalo11  && $horaFin>$Intervalo12){
											if($horaInicio<$Intervalo12){
												$horaInicio=date('H:i:s',$horaInicio);
												$Intervalo=date('H:i:s',$Intervalo12);

												$horaInicio1 = new \Carbon\Carbon($horaInicio);
												$Intervalo1 =new \Carbon\Carbon($Intervalo);
												$jornadaMan=$horaInicio1->diffInHours($Intervalo1);
											}
										}else if($horaInicio<$Intervalo11  && $horaFin>$Intervalo12){
											
											$Intervaloi=date('H:i:s',$Intervalo11);
											$Intervalof=date('H:i:s',$Intervalo12);

											$Intervalo1_ = new \Carbon\Carbon($Intervaloi);
											$Intervalo12_ =new \Carbon\Carbon($Intervalof);
											$jornadaMan=$Intervalo1_->diffInHours($Intervalo12_);
										}


										//Jornada Tarde
										$Intervalo21 = strtotime('2009-01-31'.' 12:00:00');
										$Intervalo22 = strtotime('2009-01-31'.' 18:00:00');
										$jornadaTar=0;
										
										$horaInicio = strtotime('2009-01-31'.$actividad['t_horaInicio']);
										$horaFin =strtotime('2009-01-31'.$actividad['t_horaFin']);

										if($horaInicio>=$Intervalo21  && $horaFin<=$Intervalo22){
											
											$horaInicio=date('H:i:s',$horaInicio);
											$horaFin=date('H:i:s',$horaFin);

											$horaInicio1 = new \Carbon\Carbon($horaInicio);
											$horaFin1 =new \Carbon\Carbon($horaFin);
											$jornadaTar=$horaInicio1->diffInHours($horaFin1);
										
										}else if($horaInicio<$Intervalo21  && $horaFin<=$Intervalo22){
											if($horaFin>$Intervalo21){
												$Intervalo=date('H:i:s',$Intervalo21);
												$horaFin=date('H:i:s',$horaFin);

												$Intervalo1 = new \Carbon\Carbon($Intervalo);
												$horaFin1 =new \Carbon\Carbon($horaFin);
												$jornadaTar=$Intervalo1->diffInHours($horaFin1);
											}
										}else if($horaInicio>=$Intervalo21  && $horaFin>$Intervalo22){
											if($horaInicio<$Intervalo22){
												$horaInicio=date('H:i:s',$horaInicio);
												$Intervalo=date('H:i:s',$Intervalo22);

												$horaInicio1 = new \Carbon\Carbon($horaInicio);
												$Intervalo1 =new \Carbon\Carbon($Intervalo);
												$jornadaTar=$horaInicio1->diffInHours($Intervalo1);
											}
										}else if($horaInicio<$Intervalo21  && $horaFin>$Intervalo22){
											
												$horaInicio=date('H:i:s',$Intervalo21);
												$Intervalo =date('H:i:s',$Intervalo22);

												$horaInicio1 = new \Carbon\Carbon($horaInicio);
												$Intervalo1 =new \Carbon\Carbon($Intervalo);
												$jornadaTar=$horaInicio1->diffInHours($Intervalo1);
											
										}



										//Jornada Tarde
										$Intervalo31 = strtotime('2009-01-31'.' 18:00:00');
										$Intervalo32 = strtotime('2009-01-31'.' 24:00:00');
										$jornadaNoche=0;
										
										$horaInicio = strtotime('2009-01-31'.$actividad['t_horaInicio']);
										$horaFin =strtotime('2009-01-31'.$actividad['t_horaFin']);

										if($horaInicio>=$Intervalo31  && $horaFin<=$Intervalo32){
											
											$horaInicio=date('H:i:s',$horaInicio);
											$horaFin=date('H:i:s',$horaFin);

											$horaInicio1 = new \Carbon\Carbon($horaInicio);
											$horaFin1 =new \Carbon\Carbon($horaFin);
											$jornadaNoche=$horaInicio1->diffInHours($horaFin1);
										
										}else if($horaInicio<$Intervalo31  && $horaFin<=$Intervalo32){
											if($horaFin>$Intervalo31){
												$Intervalo=date('H:i:s',$Intervalo31);
												$horaFin=date('H:i:s',$horaFin);

												$Intervalo1 = new \Carbon\Carbon($Intervalo);
												$horaFin1 =new \Carbon\Carbon($horaFin);
												$jornadaNoche=$Intervalo1->diffInHours($horaFin1);
											}
										}else if($horaInicio>=$Intervalo31  && $horaFin>$Intervalo32){
											if($horaInicio<$Intervalo32){
												$horaInicio=date('H:i:s',$horaInicio);
												$Intervalo=date('H:i:s',$Intervalo32);

												$horaInicio1 = new \Carbon\Carbon($horaInicio);
												$Intervalo1 =new \Carbon\Carbon($Intervalo);
												$jornadaNoche=$horaInicio1->diffInHours($Intervalo1);
											}
										}else if($horaInicio<$Intervalo31  && $horaFin>$Intervalo32){
											
												$horaInicio=date('H:i:s',$Intervalo31);
												$Intervalo =date('H:i:s',$Intervalo32);

												$horaInicio1 = new \Carbon\Carbon($horaInicio);
												$Intervalo1 =new \Carbon\Carbon($Intervalo);
												$jornadaNoche=$horaInicio1->diffInHours($Intervalo1);
										}

										$tabla=$tabla.'<td>'.$jornadaMan.' </td>';
										$tabla=$tabla.'<td>'.$jornadaTar.' </td>';
										$tabla=$tabla.'<td>'.$jornadaNoche.' </td>';



			                        	if($actividad->datosActividad->count()>0){
			                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->programa['programa'].'</td>';
			                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->actividad['actividad'].'</td>';
			                        	}else{
			                        		$tabla=$tabla.'<td>N.R</td>';
			                        		$tabla=$tabla.'<td>N.R</td>';
			                        	}
			                        $tabla=$tabla.'<td>';
			                        	foreach ($actividad->datosActividad as $datoTC){
			                        		$tabla=$tabla.'<ul>
											 	<li>'.$datoTC->tematica['tematica'].'</li>
											</ul>';
			                        	}
			                        $tabla=$tabla.'</td>';
			                        $tabla=$tabla.'<td>';
			                        	foreach ($actividad->datosActividad as $datoTC){
			                        		$tabla=$tabla.'<ul>
											 	<li>'.$datoTC->componente['componente'].'</li>
											</ul>';
			                        	}
			                        $tabla=$tabla.'</td>';
			                        $tabla=$tabla.'<td>'.$actividad->gestor['Primer_Apellido'].' '.$actividad->gestor['Segundo_Apellido'].' '.$actividad->gestor['Primer_Nombre'].'</td>';
			                        $tabla=$tabla.'<td>';
			                        	$tabla=$tabla.'<label>Responsable:</label><br>';

			                        	$tabla=$tabla.' '.$actividad->responsable['Primer_Apellido'].' '.$actividad->responsable['Segundo_Apellido'].' <br> '.$actividad->responsable['Primer_Nombre'].'';
			                        	$tabla=$tabla.'<br><label>Acompantes:</label><br>';
			                        	foreach ($actividad->acompanates as $acompanante){
			                        		$tabla=$tabla.'<ul>
											 	<li>'.$acompanante->usuario['Primer_Apellido'].' '.$acompanante->usuario['Segundo_Apellido'].' '.$acompanante->usuario['Primer_Nombre'].'</li>
											</ul>';
			                        	}
			                        $tabla=$tabla.'</td>';
			                    $tabla=$tabla.'</tr>';
		                    $num++; 
		                }
		            }
		
		return response()->json(array('tabla' => $tabla));
   
    }




}
