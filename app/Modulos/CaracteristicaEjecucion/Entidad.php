<?php

namespace App\Modulos\CaracteristicaEjecucion;

use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    //
    protected $table = 'entidades';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_entidad','i_estado'];
	protected $connection = ''; 
	public $timestamps = true;

}

