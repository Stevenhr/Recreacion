<?php

namespace App\Modulos\CaracteristicaEjecucion;

use Illuminate\Database\Eloquent\Model;

class Situacion extends Model
{
    //
    protected $table = 'situaciones';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_situacion','i_estado'];
	protected $connection = ''; 
	public $timestamps = true;
}

