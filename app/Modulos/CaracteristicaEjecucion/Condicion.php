<?php

namespace App\Modulos\CaracteristicaEjecucion;

use Illuminate\Database\Eloquent\Model;

class Condicion extends Model
{
    //
    protected $table = 'condiciones';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_condicion','i_estado'];
	protected $connection = ''; 
	public $timestamps = true;

}