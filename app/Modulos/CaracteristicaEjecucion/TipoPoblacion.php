<?php

namespace App\Modulos\CaracteristicaEjecucion;

use Illuminate\Database\Eloquent\Model;

class TipoPoblacion extends Model
{
    //
    protected $table = 'tipopoblaciones';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_tipo','i_estado'];
	protected $connection = ''; 
	public $timestamps = true;

}

