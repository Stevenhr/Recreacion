<?php

namespace App\Modulos\CaracteristicaEjecucion;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    //
    protected $table = 'requisitos';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_requisito','i_estado'];
	protected $connection = ''; 
	public $timestamps = true;

}
