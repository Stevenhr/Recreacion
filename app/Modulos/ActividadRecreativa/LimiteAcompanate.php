<?php

namespace App\Modulos\ActividadRecreativa;

use Illuminate\Database\Eloquent\Model;

class LimiteAcompanate extends Model
{
    //
    protected $table = 'personaactividad';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_actividad','i_numAcompa','i_limiteInferior','i_limiteSuperior'];
	protected $connection = ''; 
	public $timestamps = false;


}
