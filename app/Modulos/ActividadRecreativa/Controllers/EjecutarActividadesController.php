<?php 

namespace App\Modulos\ActividadRecreativa\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;
use App\Localidad;
use App\Modulos\CaracteristicaEjecucion\Entidad;
use App\Modulos\CaracteristicaEjecucion\TipoPoblacion;
use App\Modulos\CaracteristicaEjecucion\Condicion;
use App\Modulos\CaracteristicaEjecucion\Situacion;
use App\Modulos\CaracteristicaEjecucion\Requisito;
use App\Modulos\Ejecucion\Ejecucion;
use App\Modulos\Ejecucion\EjecucionRequisito;
use App\Modulos\Ejecucion\EjecucionEncuesta;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use Carbon;

class EjecutarActividadesController extends Controller
{
	public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		return view('MisActividades.ejecucionActividades');
	}

	public function ejecutarBusquedaActividad(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails()  && $request['idAct']=='')
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
        	if(empty ($request['idAct']))
			{
				$actividades = ActividadRecreativa::where('i_fk_usuarioResponsable',$_SESSION['Usuario'][0])
				->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])
				->get();
			}else{
				$actividades = ActividadRecreativa::where('i_pk_id',$request['idAct'])->get();
			}

			$todas1=$actividades->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_usuarioConfirmaPrograma',0);
			$todas2=$actividades->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_usuarioConfirmaPrograma',0);
			$todas3=$actividades->whereIn('i_estadoConfirmaPrograma',[Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA])->where('i_estadoRegistroEjecucion',0);
			
			$datos =[
				'actividadesPendieProgra'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_usuarioConfirmaPrograma',0)->count(),
				'actividadesSinConfirmacion'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_usuarioConfirmaPrograma',0)->count(),
				'actividadesConfirmadas'=>$actividades->whereIn('i_estadoConfirmaPrograma',[Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA])->where('i_estadoRegistroEjecucion',0)->count(),
				'actividadesEjecutadas'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->count(),
				'actividadesCanceladasEjecutadas'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA_CANCELADA)->count(),
				'actividadesTodas'=>$actividades->whereIn('i_estadoRegistroEjecucion', [Configuracion::EJECUTADA_CANCELADA,Configuracion::EJECUTADA])->union($todas1)->union($todas2)->union($todas3)->count(),
			];

            return response()->json(array('status' => 'ok', 'errors' =>'', 'datos'=>$datos));
        }
    }


    public function actividadesPorEjecutarResponsable(Request $request)
	{

		if($request['opcion']==Configuracion::CONFIRMADO){
			$opcion2=Configuracion::CONFIRMADO_REPROGRAMADA;
		}else{
			$opcion2='';
		}

		$actividades = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates');
		$actividades = $actividades->where('i_fk_usuarioResponsable',$_SESSION['Usuario'][0]);
		$actividades = $actividades->whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']]);
		
		
		
		$opcion="";
		$color="";
		$style="";
		
		
		if($request['opcion']==Configuracion::PENDIENTE){
			$opcion="Actividades que no han sido revisadas ni confirmadas.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades =$actividades->whereIn('i_estadoAprobaPrograma',[$request['opcion'],$opcion2])->where('i_usuarioConfirmaPrograma',0);

		}else if($request['opcion']==Configuracion::APROBADO){
			$opcion="Actividades que no han sido confirmadas por el responsable de actividad.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades =$actividades->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_usuarioConfirmaPrograma',0);

		}else if($request['opcion']==Configuracion::CONFIRMADO){
			$opcion="Actividades en espera de ejecución.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades =$actividades->whereIn('i_estadoConfirmaPrograma',[Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA])->where('i_estadoRegistroEjecucion',0);

		}else if($request['opcion']==Configuracion::EJECUTADA){
			$opcion="Actividades ejecutadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades =$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA);

		}else if($request['opcion']==Configuracion::EJECUTADA_CANCELADA){
			$opcion="Actividades canceladas en ejecución.";
			$color="success";
			$style ="color: white;background-color: #c71c22;";
			$actividades =$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA_CANCELADA);

		}else if($request['opcion']==100){  
 
	        $opcion="Todas las actividades.";  
	        $color="default";  
	        $style ="color: black;background-color: grey;";  
	        $actividades=$actividades->whereIn('i_estadoRegistroEjecucion', [Configuracion::EJECUTADA_CANCELADA,Configuracion::EJECUTADA]);  
 			
 			$actividades->where(function($query) {
				$query->whereIn('i_estadoRegistroEjecucion', [Configuracion::EJECUTADA_CANCELADA,Configuracion::EJECUTADA]);
			})->orWhere(function ($query){
				$query->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_usuarioConfirmaPrograma',0);
			})->orWhere(function ($query){
				$query->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_usuarioConfirmaPrograma',0);
			})->orWhere(function ($query){
				$query->whereIn('i_estadoConfirmaPrograma',[Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA])->where('i_estadoRegistroEjecucion',0);
			}); 



    	}else{
			$opcion="";
			$color="";
			$style="";
		}

		$actividades =$actividades->get();

		$Localidades = Localidad::all();
		$entidades = Entidad::all();
		$tipoPoblaciones = TipoPoblacion::all();
		$condiciones = Condicion::all();
		$situaciones = Situacion::all();
		$requisitos = Requisito::all();


		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
			'Localidades'=>$Localidades,
			'entidades'=>$entidades,
			'tipoPoblaciones'=>$tipoPoblaciones,
			'condiciones'=>$condiciones,
			'situaciones'=>$situaciones,
			'requisitos'=>$requisitos,

		];

		return view('MisActividades.tablaEjecutarActividad',$datos);
   
    }


    public function datosprogramacionactividadEjecutar(Request $request, $id)
	{
		$actividad = ActividadRecreativa::with(['datosActividad' => function($query) {
			$query->with('programa', 'actividad','tematica','componente');
		} ,'acompanates','localidad_comunidad','upz_comunidad','barrio_comunidad','responsable','localidad_escenario', 'upz_escenario', 'barrio_escenario','observaciones.usuario'])
		->find($id);

		return response()->json($actividad);
	}


	public function select_upz_ejecucion(Request $request, $id)
	{
		
		$Locali_recreador=ConfiguracionPersona::where('i_id_localidad',$id)->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_ACTIVIDAD)->get();
		$upzs = Upz::where('IdLocalidad',$id)->get();

		$datos=[
		'upzs'=>$upzs,
		'responsables'=>$Locali_recreador
		];
		return response()->json($datos);
	}

	public function select_barrio_ejecucion(Request $request, $id)
	{
		$upzs = Barrio::where('CodUpz',$id)->get();
		return response()->json($upzs);
	}

	public function registro_poblacional(Request $request)
	{
		
		$messages = [
			'insGrupoComu_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'entidad_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'localidad_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'Id_Upz_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'Id_Barrio_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'tipoPoblacion_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'condicion_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'situacion_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'i_0a5_f'  => 'El campo :attribute se encuentra vacio.',
			'i_0a5_m'  => 'El campo :attribute se encuentra vacio.',
			'i_6a12_f'  => 'El campo :attribute se encuentra vacio.',
			'i_6a12_m'  => 'El campo :attribute se encuentra vacio.',
			'i_13a17_f'  => 'El campo :attribute se encuentra vacio.',
			'i_13a17_m'  => 'El campo :attribute se encuentra vacio.',
			'i_18a26_f'  => 'El campo :attribute se encuentra vacio.',
			'i_18a26_m'  => 'El campo :attribute se encuentra vacio.',
			'i_27a59_f'  => 'El campo :attribute se encuentra vacio.',
			'i_27a59_m'  => 'El campo :attribute se encuentra vacio.',
			'i_60_f'  => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'insGrupoComu_ejecucion' =>'required',
			'entidad_ejecucion' =>'required',
			'localidad_ejecucion' =>'required',
			'Id_Upz_ejecucion' =>'required',
			'Id_Barrio_ejecucion' =>'required',
			'tipoPoblacion_ejecucion' =>'required',
			'condicion_ejecucion' =>'required',
			'situacion_ejecucion' =>'required',
			'i_0a5_f' =>'required',
			'i_0a5_m' =>'required',
			'i_6a12_f' =>'required',
			'i_6a12_m' =>'required',
			'i_13a17_f' =>'required',
			'i_13a17_m' =>'required',
			'i_18a26_f' =>'required',
			'i_18a26_m' =>'required',
			'i_27a59_f' =>'required',
			'i_27a59_m' =>'required',
			'i_60_f' =>'required',

    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'Error', 'errors' => $validator->errors()));
        }
        else
        {

    		$suma = $request['i_0a5_f']+$request['i_0a5_m']+$request['i_6a12_f']+$request['i_6a12_m']+$request['i_13a17_f']+$request['i_13a17_m']+$request['i_18a26_f']+$request['i_18a26_m']+$request['i_27a59_f']+$request['i_27a59_m']+$request['i_60_f'];

    		if ($suma==0)
	        {
	            return response()->json(array('status' => 'Error2', 'errors' => $validator->errors()));
	        }else{
	        	$actividades = ActividadRecreativa::find($request['id_actividad']);
	        	$actividades['i_estadoRegistroEjecucion']=Configuracion::EJECUTADA;
	        	$actividades['i_usuarioRegistroEjecucion']=$_SESSION['Usuario'][0];
	        	$actividades->save();

				$ejecucion = new Ejecucion;
				$ejecucion['i_fk_id_actividad']=$request['id_actividad'];
				$ejecucion['vc_poblacion']=$request['insGrupoComu_ejecucion'];
				$ejecucion['i_fk_id_entidad']=$request['entidad_ejecucion'];
				$ejecucion['i_fk_id_localidad']=$request['localidad_ejecucion'];
				$ejecucion['i_fk_id_upz']=$request['Id_Upz_ejecucion'];
				$ejecucion['i_fk_id_barrio']=$request['Id_Barrio_ejecucion'];
				$ejecucion['i_fk_id_tipo']=$request['tipoPoblacion_ejecucion'];
				$ejecucion['i_fk_id_condicion']=$request['condicion_ejecucion'];
				$ejecucion['i_fk_id_situacion']=$request['situacion_ejecucion'];

				$ejecucion['i_0a5_f']=$request['i_0a5_f'];
				$ejecucion['i_0a5_m']=$request['i_0a5_m'];
				$ejecucion['i_6a12_f']=$request['i_6a12_f'];
				$ejecucion['i_6a12_m']=$request['i_6a12_m'];
				$ejecucion['i_13a17_f']=$request['i_13a17_f'];
				$ejecucion['i_13a17_m']=$request['i_13a17_m'];
				$ejecucion['i_18a26_f']=$request['i_18a26_f'];
				$ejecucion['i_18a26_m']=$request['i_18a26_m'];
				$ejecucion['i_27a59_f']=$request['i_27a59_f'];
				$ejecucion['i_27a59_m']=$request['i_27a59_m'];
				$ejecucion['i_60_f']=$request['i_60_f'];
				
				$ejecucion->save();
				 return response()->json(array('status' => 'Bien', 'errors' => $validator->errors()));
			}
		}
	}


	public function registro_requisitos(Request $request)
	{

		$messages = [
			'requisito_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'causa_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'accion_ejecucion'  => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'requisito_ejecucion' =>'required',
			'causa_ejecucion' =>'required',
			'accion_ejecucion' =>'required',
    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'Error', 'errors' => $validator->errors()));
        }
        else
        {

			$actividades = ActividadRecreativa::find($request['id_actividad1']);
        	$actividades['i_estadoRegistroEjecucion']=Configuracion::EJECUTADA;
        	$actividades['i_usuarioRegistroEjecucion']=$_SESSION['Usuario'][0];
        	$actividades->save();

			$ejecucionRequisito = new EjecucionRequisito;
			$ejecucionRequisito['i_fk_id_actividad']= $request['id_actividad1'];
			$ejecucionRequisito['i_fk_id_requisito']= $request['requisito_ejecucion'];
			$ejecucionRequisito['tx_causa']= $request['causa_ejecucion'];
			$ejecucionRequisito['tx_accion']= $request['accion_ejecucion'];
			$ejecucionRequisito->save();
			return response()->json(array('status' => 'Bien', 'errors' => $validator->errors()));
		}
	}


	public function registro_encuesta(Request $request)
	{

		$messages = [
			'puntualidad_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'divulgacion_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'escenario_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'cumplimiento_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'veriedad_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'imagen_ejecucion'  => 'El campo :attribute se encuentra vacio.',
			'seguridad_ejecucion'  => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'puntualidad_ejecucion' =>'required',
			'divulgacion_ejecucion' =>'required',
			'escenario_ejecucion' =>'required',
			'cumplimiento_ejecucion' =>'required',
			'veriedad_ejecucion' =>'required',
			'imagen_ejecucion' =>'required',
			'seguridad_ejecucion' =>'required',
    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'Error', 'errors' => $validator->errors()));
        }
        else
        {

			$ejecucionEncuesta= EjecucionEncuesta::where('i_fk_id_actividad',$request['id_actividad2'])->first();

			if(count($ejecucionEncuesta)<=0){

				$ejecucionEncuesta=new EjecucionEncuesta;
			}
			

        	$actividades = ActividadRecreativa::find($request['id_actividad2']);
        	$actividades['i_estadoRegistroEjecucion']=Configuracion::EJECUTADA;
        	$actividades['i_usuarioRegistroEjecucion']=$_SESSION['Usuario'][0];
        	$actividades->save();


			$ejecucionEncuesta['i_fk_id_actividad']= $request['id_actividad2'];
			$ejecucionEncuesta['i_puntulidad']= $request['puntualidad_ejecucion'];
			$ejecucionEncuesta['i_divulgacion']= $request['divulgacion_ejecucion'];
			$ejecucionEncuesta['i_escenariomontaje']= $request['escenario_ejecucion'];
			$ejecucionEncuesta['i_cumplimineto']= $request['cumplimiento_ejecucion'];
			$ejecucionEncuesta['i_variedad']= $request['veriedad_ejecucion'];
			$ejecucionEncuesta['i_imageninstitucional']= $request['imagen_ejecucion'];
			$ejecucionEncuesta['i_seguridad']= $request['seguridad_ejecucion'];
			$ejecucionEncuesta->save();
			return response()->json(array('status' => 'Bien', 'errors' => $validator->errors()));
		}
	}

	public function obtenerDatosProgramacion(Request $request, $id)
	{

		$data=ActividadRecreativa::find($id);

		return response()->json($data);
	}

	public function obtenerDatosEjecucion(Request $request, $id)
	{
		
		//Datos de la población registrados
		$ejecucion = Ejecucion::with('entidad','localidad','upz','barrio')->where('i_fk_id_actividad',$id)->get();

		$tdEjecucion="";
		$num=1;
		$total1=0;
		foreach ($ejecucion as $key => $value) 
		{
			$total=$value['i_0a5_f']+$value['i_0a5_m']+$value['i_6a12_f']+$value['i_6a12_m']+$value['i_13a17_f']+$value['i_13a17_m']+$value['i_18a26_f']+$value['i_18a26_m']+$value['i_27a59_f']+$value['i_27a59_m']+$value['i_60_f']+$value['i_60_m'];

			$tdEjecucion=$tdEjecucion."<tr  data-row='".$value['i_pk_id']."'>";
			$tdEjecucion=$tdEjecucion."<td>".$num."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['vc_poblacion']."</td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->entidad['vc_entidad']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->localidad['Localidad']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->upz['Upz']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->barrio['Barrio']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->tipoPoblacion['vc_tipo']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->condicion['vc_condicion']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->situacion['vc_situacion']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_0a5_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_0a5_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_6a12_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_6a12_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_13a17_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_13a17_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_18a26_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_18a26_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_27a59_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_27a59_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_60_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_60_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$total."</td>";
			$tdEjecucion=$tdEjecucion."<td><div id='".$value['i_pk_id']."mensaje' ></div><button type='button' class='btn btn-danger btn-xs' data-rel='".$value['i_pk_id']."' data-funcion='eliminar'><span class='glyphicon glyphicon-remove'></span></button></td>";
			$tdEjecucion=$tdEjecucion."</tr>";
			$total1=$total1+$total;
			$num++;

			
		}

			$tdEjecucion=$tdEjecucion."<tr>";
			
			$tdEjecucion=$tdEjecucion."<th colspan='9' class='text-right'><h5>TOTALES</h5></td>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'>90000</th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_0a5_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_0a5_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_6a12_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_6a12_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_13a17_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_13a17_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_18a26_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_18a26_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_27a59_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_27a59_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_60_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_60_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th class='success'><h4>".$total1."</h4></th>";
			$tdEjecucion=$tdEjecucion."<th></th>";

			$tdEjecucion=$tdEjecucion."</tr>";


		//Datos de los requisitos incumplidos
		$ejecucionRequisito=EjecucionRequisito::with('requisito')->where('i_fk_id_actividad',$id)->get();
		$tdEjecucionRequisito="";
		$num2=1;

		foreach ($ejecucionRequisito as $key => $value) {
			$tdEjecucionRequisito=$tdEjecucionRequisito."<tr data-row='".$value['i_pk_id']."'>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$num2."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value->requisito['vc_requisito']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value['tx_causa']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value['tx_accion']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td><div id='".$value['i_pk_id']."mensajerequerimiento' ></div><button type='button' class='btn btn-danger btn-xs' data-rel='".$value['i_pk_id']."' data-funcion='eliminarRequisito'><span class='glyphicon glyphicon-remove'></span></button></td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."</tr>";
			$num2++;
		}

		//Datos de las encuestas
		$ejecucionEncuesta= EjecucionEncuesta::where('i_fk_id_actividad',$id)->first();
		$tdEjecucionEncuesta="";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<tr>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_puntulidad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_divulgacion']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_escenariomontaje']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_cumplimineto']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_variedad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_imageninstitucional']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_seguridad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."</tr>";

		$data=[
			'viewEncuesta'=>$tdEjecucionEncuesta,
			'viewRequisitos'=>$tdEjecucionRequisito,
			'viewEjecuciones'=>$tdEjecucion,
		];

		return response()->json($data);
	}


	public function obtenerDatosEjecucionConsulta(Request $request, $id)
	{
		
		//Datos de la población registrados
		$ejecucion = Ejecucion::with('entidad','localidad','upz','barrio')->where('i_fk_id_actividad',$id)->get();

		$tdEjecucion="";
		$num=1;
		$total1=0;
		foreach ($ejecucion as $key => $value) 
		{
			$total=$value['i_0a5_f']+$value['i_0a5_m']+$value['i_6a12_f']+$value['i_6a12_m']+$value['i_13a17_f']+$value['i_13a17_m']+$value['i_18a26_f']+$value['i_18a26_m']+$value['i_27a59_f']+$value['i_27a59_m']+$value['i_60_f']+$value['i_60_m'];

			$tdEjecucion=$tdEjecucion."<tr  data-row='".$value['i_pk_id']."'>";
			$tdEjecucion=$tdEjecucion."<td>".$num."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['vc_poblacion']."</td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->entidad['vc_entidad']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->localidad['Localidad']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->upz['Upz']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->barrio['Barrio']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->tipoPoblacion['vc_tipo']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->condicion['vc_condicion']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value->situacion['vc_situacion']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_0a5_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_0a5_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_6a12_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_6a12_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_13a17_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_13a17_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_18a26_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_18a26_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_27a59_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_27a59_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_60_f']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$value['i_60_m']."<br></td>";
			$tdEjecucion=$tdEjecucion."<td>".$total."</td>";
			$tdEjecucion=$tdEjecucion."</tr>";
			$total1=$total1+$total;
			$num++;

			
		}

			$tdEjecucion=$tdEjecucion."<tr>";
			
			$tdEjecucion=$tdEjecucion."<th colspan='9' class='text-right'><h5>TOTALES</h5></td>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'>90000</th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th style='display: none;'></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_0a5_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_0a5_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_6a12_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_6a12_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_13a17_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_13a17_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_18a26_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_18a26_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_27a59_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_27a59_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_60_f')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th><h5>".$ejecucion->sum('i_60_m')."</h5></th>";
			$tdEjecucion=$tdEjecucion."<th class='success'><h4>".$total1."</h4></th>";

			$tdEjecucion=$tdEjecucion."</tr>";


		//Datos de los requisitos incumplidos
		$ejecucionRequisito=EjecucionRequisito::with('requisito')->where('i_fk_id_actividad',$id)->get();
		$tdEjecucionRequisito="";
		$num2=1;

		foreach ($ejecucionRequisito as $key => $value) {
			$tdEjecucionRequisito=$tdEjecucionRequisito."<tr data-row='".$value['i_pk_id']."'>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$num2."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value->requisito['vc_requisito']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value['tx_causa']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."<td>".$value['tx_accion']."</td>";
			$tdEjecucionRequisito=$tdEjecucionRequisito."</tr>";
			$num2++;
		}

		//Datos de las encuestas
		$ejecucionEncuesta= EjecucionEncuesta::where('i_fk_id_actividad',$id)->first();
		$tdEjecucionEncuesta="";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<tr>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_puntulidad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_divulgacion']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_escenariomontaje']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_cumplimineto']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_variedad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_imageninstitucional']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."<td>".$ejecucionEncuesta['i_seguridad']."</td>";
		$tdEjecucionEncuesta=$tdEjecucionEncuesta."</tr>";

		$data=[
			'viewEncuesta'=>$tdEjecucionEncuesta,
			'viewRequisitos'=>$tdEjecucionRequisito,
			'viewEjecuciones'=>$tdEjecucion,
		];

		return response()->json($data);
	}

	public function eliminarEjecucion(Request $request, $id)
	{
		$ejecucion = Ejecucion::find($id);

		$ejecucion->delete();

		return response()->json(array('mensaje'=>'<div class="alert alert-danger"><strong>DATO ELIMINADO:</strong>Dato elimiando exitosamente.</div>'));
	}

	public function eliminarEjecucionRequerimiento(Request $request, $id)
	{
		$ejecucionRequis = EjecucionRequisito::find($id);

		$ejecucionRequis->delete();

		return response()->json(array('mensaje'=>'<div class="alert alert-danger"><strong>DATO ELIMINADO:</strong>Dato elimiando exitosamente.</div>'));
	}
	
}
