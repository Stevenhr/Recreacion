<?php 

namespace App\Modulos\ActividadRecreativa\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use App\Modulos\Observaciones\Observaciones;
use Validator;


class MisProgramacionesCreadasController extends Controller
{
	public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		$Locali_gestor=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])->where('i_id_tipo_persona',Configuracion::GESTOR)->get();
		
		$locali[]="";
		$i=0;
		foreach ($Locali_gestor as $key) {
			$locali[$i]=$key['i_id_localidad'];
			$i++;
		}
		$resposanblesActividad = ConfiguracionPersona::with('persona')->whereIn('i_id_tipo_persona',[Configuracion::RESPOSANBLE_ACTIVIDAD,Configuracion::GESTOR])->whereIn('i_id_localidad',$locali)->groupBy('i_fk_id_persona')->get();

		$acompañantesActividad = ConfiguracionPersona::with('persona')->whereIn('i_id_tipo_persona',[Configuracion::RESPOSANBLE_ACTIVIDAD])->groupBy('i_fk_id_persona')->get();

		$datos=[
            "resosablesActividad"=>$resposanblesActividad,
            "acompanantes"=>$acompañantesActividad
		];

		return view('MisActividades.misProgramacionesCreadas',$datos);
	}

	public function tablaActividadCreada(Request $request)
	{
		
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
		],$messages);
	    
	    if ($validator->fails() && $request['id_actividad']=='')
	    {
	        return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
	    }
	    else
	    {	
	    

	    	$queryB = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates');

	    	$queryB=$queryB->where('i_fk_usuario',intval($_SESSION['Usuario'][0]));

			if(!empty($request['fechaInicio'])){
				
				$queryB=$queryB->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']]);
				
				if(empty($request['id_actividad']) && empty($request['responsable']) && empty($request['acompanante'])){
					$queryB->orWhere('i_estadoAprobaPrograma','=',Configuracion::INCOMPLETA); //trae todo
				}
			}

			if(!empty($request['id_actividad'])){

				$queryB=$queryB->where('i_pk_id',$request['id_actividad']);

			}

			if(!empty($request['responsable'])){
				$queryB=$queryB->where('i_fk_usuarioResponsable',$request['responsable']);
			}

			if(!empty($request['acompanante'])){
				$queryB=$queryB->whereHas('acompanates', function ($query) use ($request) {
			        $query->where('i_fk_usuario',$request['acompanante']);
			    });
			}

			$actividades=$queryB->get();
			
			$actividades=$actividades->where('i_fk_usuario',intval($_SESSION['Usuario'][0]));

			$opcion="";
			$color="";
			$style="";
					
					
					//dd($_SESSION['Usuario'][0]);
					$tabla='';
			            $num=1;
			            if($actividades!=''){
			                foreach ($actividades as $actividad) {
			                    	
			                    	$tabla=$tabla.'<tr class="something" data-row="'.$actividad['i_pk_id'].'">';
				                        $tabla=$tabla.'<td class="col-md-1">'.$num.'</td>';
				                        $tabla=$tabla.'<td class="col-md-13"><b><p class="text-info text-center" style="font-size: 15px">'.$actividad['i_pk_id'].' </p></b></td>';
				                        $tabla=$tabla.'<td >'.$actividad['d_fechaEjecucion'].'</td>';
				                        $tabla=$tabla.'<td >'.$actividad['created_at'].'</td>';
				                        $tabla=$tabla.'<td>'.$actividad['t_horaInicio'].'</td>';
				                        $tabla=$tabla.'<td>'.$actividad['t_horaFin'].'</td>';
				                        	if($actividad->datosActividad->count()>0){
				                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->programa['programa'].'</td>';
				                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->actividad['actividad'].'</td>';
				                        	}else{
				                        		$tabla=$tabla.'<td>N.R</td>';
				                        		$tabla=$tabla.'<td>N.R</td>';
				                        	}
				                        $tabla=$tabla.'<td>';
				                        	$tabla=$tabla.'<label>Tematicas:</label><br>';
				                        	foreach ($actividad->datosActividad as $datoTC){
				                        		$tabla=$tabla.'<ul>
												 	<li>'.$datoTC->tematica['tematica'].'</li>
												</ul>';
				                        	}
				                        	$tabla=$tabla.'<label>Componente:</label>';
				                        	foreach ($actividad->datosActividad as $datoTC){
				                        		$tabla=$tabla.'<ul>
												  	<li>'.$datoTC->componente['componente'].'</li>
												</ul>';
				                        	}
				                        $tabla=$tabla.'</td>';
				                        $tabla=$tabla.'<td>'.$actividad->gestor['Primer_Apellido'].' '.$actividad->gestor['Segundo_Apellido'].' <br> '.$actividad->gestor['Primer_Nombre'].'</td>';
				                        $tabla=$tabla.'<td>';
				                        	$tabla=$tabla.'<label>Responsable:</label><br>';

				                        	$tabla=$tabla.' '.$actividad->responsable['Primer_Apellido'].' '.$actividad->responsable['Segundo_Apellido'].' '.$actividad->responsable['Primer_Nombre'].'';
				                        	$tabla=$tabla.'<br><label>Acompañantes:</label><br>';
				                        	foreach ($actividad->acompanates as $acompanante){
				                        		$tabla=$tabla.'<ul>
												 	<li>'.$acompanante->usuario['Primer_Apellido'].' '.$acompanante->usuario['Segundo_Apellido'].' '.$acompanante->usuario['Primer_Nombre'].'</li>
												</ul>';
				                        	}
				                        $tabla=$tabla.'</td>';
				                        $tabla=$tabla.'<th>';
				                        
				                        $tabla=$tabla.'Programación:';
				                        	
				                        	switch($actividad['i_estadoAprobaPrograma']){

				                        		case Configuracion::INCOMPLETA:
				                        			$tabla=$tabla.' <span class="label label-danger"> Incompleta</span>
				                        							<div id="'.$actividad['i_pk_id'].'mensaje"></div>
				                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
				                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
					                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
					                        							<button type="submit" class="btn btn-link btn-xs"   >
																			<span class="glyphicon glyphicon-edit"></span>  Editar
																		</button>
																	</form>

																		                        					
																	<button type="button" data-funcion="eliminar" data-rel="'.$actividad['i_pk_id'].'" class="btn btn-link btn-xs">
																	<span class="glyphicon glyphicon-trash"></span>  Eliminar
																	</button>
																	';
																	
				                        							
				                        		break;

				                        		case Configuracion::PENDIENTE:
				                        			$tabla=$tabla.'<span class="label label-info"><b>Pendiente de revisión</span>';
				                        		break;

				                        		case Configuracion::APROBADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b></b> Aprobada</span>';
				                        		break;

				                        		case Configuracion::DEVUELTO:
				                        			$tabla=$tabla.' <span class="label label-warning"><b></b> Denegada</span>
				                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
				                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
					                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
					                        							<button type="submit" class="btn btn-link btn-xs"   >
																			<span class="glyphicon glyphicon-edit"></span>  Editar
																		</button>
																	</form>';
																	
				                        					
				                        		break;

				                        		case Configuracion::CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Confirmación:';
				                        	switch($actividad['i_estadoConfirmaPrograma']){
				                        		case Configuracion::CONFIRMADO:
				                        			$tabla=$tabla.'<span class="label label-success"><b><b>Programación:</b> Confirmada</span>';
				                        		break;

				                        		case Configuracion::CONFIRMADO_CANCELADO:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada</span>';
				                        		break;
	  
				                        		case Configuracion::CONFIRMADO_REPROGRAMADA:
				                        			$tabla=$tabla.'<span class="label label-success">Confirmada/ Reprogramada</span>';
				                        		break;
				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}


				                        	$tabla=$tabla.'<br><br>Ejecución:';
				                        	switch($actividad['i_estadoRegistroEjecucion']){
				                        		case Configuracion::EJECUTADA:
				                        			$tabla=$tabla.'<span class="label label-success">Registrada.</span>';
				                        		break;

				                        		case Configuracion::EJECUTADA_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger">Cancelada al registrar.</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;
				                        	}

				                        	$tabla=$tabla.'<br><br>Estado ejecución:';
				                        	switch($actividad['i_estadoAprobaEjecucion']){
				                        		case Configuracion::EJECUCION_APROBADA:
				                        			$tabla=$tabla.'<span class="label label-success"> Aprobada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_CANCELADA:
				                        			$tabla=$tabla.'<span class="label label-danger"> Cancelada</span>';
				                        		break;

				                        		case Configuracion::EJECUCION_DENEGADA:
				                        			$tabla=$tabla.'<span class="label label-warning"> Denegada</span>';
				                        		break;

				                        		default:
				                        			$tabla=$tabla.'<span class="label label-danger"><b>No registrada</span>';
				                        		break;

				                        	}

				                        	$tabla=$tabla.'<td> <ul class="list-group">
											  <li class="list-group-item"><button type="button" class="btn btn-link btn-xs" data-rel="'.$actividad['i_pk_id'].'"  data-toggle="modal" data-funcion="programacion" data-target="#modalProgramacion"><span class="glyphicon glyphicon-eye-open"></span> Programaciòn </button></li>
											</ul> </td>';

					                        

				                        $tabla=$tabla.'</th>
				                    </tr>';
			                    $num++; 
			                }
			            }
			
			return response()->json(array('tabla' => $tabla));
		}
   
    }


	public function validaPasos(Request $request)
	{
		$messages = [
		    'localidad_comunidad.required'    => 'El campo :attribute se encuentra vacio.',
		    'Id_Upz_Comunidad.required'    => 'El campo :attribute se encuentra vacio.',
		    'Id_Barrio_Comunidad.required' => 'El campo :attribute se encuentra vacio.',
		    'institucion_g_c.required'      => 'El campo :attribute se encuentra vacio.',
		    'caracteristicaPoblacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'caracteristicaEspecifica.required'      => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'localidad_comunidad' => 'required',
			'Id_Upz_Comunidad' => 'required',
			'Id_Barrio_Comunidad' => 'required',
			'institucion_g_c' => 'required',
			'caracteristicaPoblacion' => 'required',
			'caracteristicaEspecifica' => 'required',
    	],$messages);
        
        if ($validator->fails())
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        
        if($request->input('id') == '0')
        {
            return $this->crear_datos_comunidad($request->all());
        }
        else
        {
            return $this->modificar_datos_comunidad($request->all());  
        }
    }

    public function datosprogramacionactividad(Request $request, $id)
	{
		$actividad = ActividadRecreativa::with(['datosActividad' => function($query) {
			$query->with('programa', 'actividad','tematica','componente');
		} ,'acompanates','localidad_comunidad','upz_comunidad','barrio_comunidad','responsable','localidad_escenario', 'upz_escenario', 'barrio_escenario','observaciones.usuario'])
		->find($id);

		return response()->json($actividad);
	}


	public function agregarObservacionProgramacion(Request $request, $id, $mensaje)
	{
		$observacion = new  Observaciones();
		$observacion['i_fk_id_actividad']=$id;
		$observacion['tx_observacion']=$mensaje;
		$observacion['i_fk_usuario']=$_SESSION['Usuario'][0];
		$observacion['vc_tipoobservacion']="Observación del gestor en programación";
		$observacion->save();

		return response()->json($observacion);
	}

}