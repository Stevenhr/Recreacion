<?php 

namespace App\Modulos\ActividadRecreativa\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;
use App\Localidad;
use App\Modulos\CaracteristicaEjecucion\Entidad;
use App\Modulos\CaracteristicaEjecucion\TipoPoblacion;
use App\Modulos\CaracteristicaEjecucion\Condicion;
use App\Modulos\CaracteristicaEjecucion\Situacion;
use App\Modulos\CaracteristicaEjecucion\Requisito;
use App\Modulos\Ejecucion\Ejecucion;
use App\Modulos\Ejecucion\EjecucionRequisito;
use App\Modulos\Ejecucion\EjecucionEncuesta;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use Carbon;

class ActividadesHabilitadasController extends Controller
{
	public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		$Localidades = Localidad::all();
		$entidades = Entidad::all();
		$tipoPoblaciones = TipoPoblacion::all();
		$condiciones = Condicion::all();
		$situaciones = Situacion::all();
		$requisitos = Requisito::all();


		$datos=[
			'Localidades'=>$Localidades,
			'entidades'=>$entidades,
			'tipoPoblaciones'=>$tipoPoblaciones,
			'condiciones'=>$condiciones,
			'situaciones'=>$situaciones,
			'requisitos'=>$requisitos,

		];
		return view('MisActividades.actividadesHabilitadas',$datos);
	}


	public function tablaActividadHabilitadas(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		    'Tipoactividad.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
			'Tipoactividad' => 'required',
    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
			$qb = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates')->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])->where('i_habilitacion','=',$request['Tipoactividad']);
			
			if($request['Tipoactividad']==2)
			{
				$qb->where('i_fk_usuario', $_SESSION['Usuario'][0]);
			}else if($request['Tipoactividad']==3)
			{
				$qb->where('i_fk_usuarioResponsable', $_SESSION['Usuario'][0]);
			}
			
			$actividades = $qb->get();
			
			$opcion="";
			$color="";
			$style="";
		

				$tabla='';
		            $num=1;
		            if($actividades!=''){
		                foreach ($actividades as $actividad) {
		                    	
		                    	$tabla=$tabla.'<tr class="something" data-row="'.$actividad['i_pk_id'].'">';
			                        $tabla=$tabla.'<td class="col-md-1">'.$num.'</td>';
			                        $tabla=$tabla.'<td class="col-md-13"><b><p class="text-info text-center" style="font-size: 15px">'.$actividad['i_pk_id'].' </p></b></td>';
			                        $tabla=$tabla.'<td >'.$actividad['d_fechaEjecucion'].'</td>';
			                        $tabla=$tabla.'<td>'.$actividad['t_horaInicio'].'</td>';
			                        $tabla=$tabla.'<td>'.$actividad['t_horaFin'].'</td>';
			                        	if($actividad->datosActividad->count()>0){
			                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->programa['programa'].'</td>';
			                        		$tabla=$tabla.'<td>'.$actividad->datosActividad[0]->actividad['actividad'].'</td>';
			                        	}else{
			                        		$tabla=$tabla.'<td>N.R</td>';
			                        		$tabla=$tabla.'<td>N.R</td>';
			                        	}
			                        $tabla=$tabla.'<td>';
			                        	$tabla=$tabla.'<label>Tematicas:</label><br>';
			                        	foreach ($actividad->datosActividad as $datoTC){
			                        		$tabla=$tabla.'<ul>
											 	<li>'.$datoTC->tematica['tematica'].'</li>
											</ul>';
			                        	}
			                        	$tabla=$tabla.'<label>Componente:</label>';
			                        	foreach ($actividad->datosActividad as $datoTC){
			                        		$tabla=$tabla.'<ul>
											  	<li>'.$datoTC->componente['componente'].'</li>
											</ul>';
			                        	}
			                        $tabla=$tabla.'</td>';
			                        $tabla=$tabla.'<td>'.$actividad->responsable['Primer_Apellido'].' '.$actividad->responsable['Segundo_Apellido'].' '.$actividad->responsable['Primer_Nombre'].'</td>';
			                        $tabla=$tabla.'<td>';
			                        	$tabla=$tabla.'<label>Responsable:</label><br>';

			                        	$tabla=$tabla.' '.$actividad->gestor['Primer_Apellido'].' '.$actividad->gestor['Segundo_Apellido'].' <br> '.$actividad->gestor['Primer_Nombre'].'';
			                        	$tabla=$tabla.'<br><label>Acompantes:</label><br>';
			                        	foreach ($actividad->acompanates as $acompanante){
			                        		$tabla=$tabla.'<ul>
											 	<li>'.$acompanante->usuario['Primer_Apellido'].' '.$acompanante->usuario['Segundo_Apellido'].' '.$acompanante->usuario['Primer_Nombre'].'</li>
											</ul>';
			                        	}
			                        $tabla=$tabla.'</td>';
			                        $tabla=$tabla.'<th class="text-center">';
			                        	
			                        	switch($actividad['i_estado']){

			                        		case Configuracion::INCOMPLETA:
			                        			$tabla=$tabla.' <span class="label label-danger"><b>PROGRAMACION:</b> INCOMPLETA</span>';
																
			                        							
			                        		break;

			                        		case Configuracion::PENDIENTE:
			                        			$tabla=$tabla.'<span class="label label-info"><b>Programación: pendiente de revisión</span>';
			                        		break;

			                        		case Configuracion::APROBADO:
			                        			$tabla=$tabla.'<span class="label label-success"><b>Programación:</b> APROBADA</span>';
			                        		break;

			                        		case Configuracion::DEVUELTO:
			                        			$tabla=$tabla.' <span class="label label-warning"><b>Programación:</b> DENEGADA</span>
			                        							<form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
			                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
				                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
				                        							<button type="submit" class="btn btn-link btn-xs"   >
																		<span class="glyphicon glyphicon-edit"></span>  Editar
																	</button>
																</form>';
																
			                        					
			                        		break;

			                        		case Configuracion::CANCELADO:
			                        			$tabla=$tabla.'<span class="label label-danger"><b>Programación:</b> CANCELADA</span>';
			                        		break;

			                        		case Configuracion::CONFIRMADO:
			                        			$tabla=$tabla.'<span class="label label-success"><b><b>Programación:</b> CONFIRMADA</span>';
			                        		break;

			                        		case Configuracion::CONFIRMADO_CANCELADO:
			                        			$tabla=$tabla.'<span class="label label-danger">Cancelado en confirmación</span>';
			                        		break;
  
			                        		case Configuracion::CONFIRMADO_REPROGRAMADA:
			                        			$tabla=$tabla.'<span class="label label-success">Confirmado con reprogramación</span>';
			                        		break;

			                        		case Configuracion::EJECUTADA:
			                        			$tabla=$tabla.'<span class="label label-default">Ejecución: Con ejecución registrada.</span>';
			                        		break;

			                        		case Configuracion::EJECUTADA_CANCELADA:
			                        			$tabla=$tabla.'<span class="label label-danger">Ejecución: Cancelada al registrar ejecución.</span>';
			                        		break;

			                        		case Configuracion::EJECUCION_APROBADA:
			                        			$tabla=$tabla.'<span class="label label-success"><b>Ejecución:</b> APROBADA</span>';
			                        		break;

			                        		case Configuracion::EJECUCION_CANCELADA:
			                        			$tabla=$tabla.'<span class="label label-danger"><b>Ejecución:</b> CANCELADA</span>';
			                        		break;

			                        		case Configuracion::EJECUCION_DENEGADA:
			                        			$tabla=$tabla.'<span class="label label-warning"><b>Ejecución:</b> DENEGADA</span>';
			                        		break;

			                        		default:
			                        			$tabla=$tabla.'<span class="label label-danger"><b>Error</span>';
			                        		break;

			                        	}


			                        $tabla=$tabla.'</th>';

			                        if($request['Tipoactividad']==2)
			                        {
				                        $tabla=$tabla.'<th><form method="POST" action="'.route('modificar').'" accept-charset="UTF-8" target="_blank">
	                        							    <input name="_token" type="hidden" value="Ip0nv7gRMsXlGjZU2FcRdfKDBWIZqk055XerclLi">
		                        							<input type="hidden" name="id_act_mod" value="'.$actividad['i_pk_id'].'"> 
		                        							<button type="submit" class="btn btn-link btn-xs"   >
																<span class="glyphicon glyphicon-edit"></span>  Editar programación
															</button>
														</form>';
				                    	$tabla=$tabla.'</th>';
				                    }else{
				                    	$tabla=$tabla.'<th>N.A</th>';
				                    }


				                    if($request['Tipoactividad']==3)
			                        {
				                        $tabla=$tabla.'<th>
				                       		<ul class="list-group">
											  <li class="list-group-item"><button type="button" class="btn btn-success btn-xs btn-block" data-rel="'.$actividad['i_pk_id'].'" data-funcion="ejecucion" data-toggle="modal" data-target="#modalEjecucion"><span class="glyphicon glyphicon-pencil"></span> Registro ejecución </button></li>
											</ul>	
											<ul class="list-group">  
											  <li class="list-group-item"><button type="button" class="btn btn-success btn-xs btn-block" data-rel="'.$actividad['i_pk_id'].'" data-funcion="ejecucionDatos" data-toggle="modal" data-target="#modalDatosEjecucion"><span class="glyphicon glyphicon-list-alt"></span> Datos registrados </button></li>
											</ul>';
				                    	$tabla=$tabla.'</th>';
				                    }else{
				                    	$tabla=$tabla.'<th>N.A</th>';
				                    }

		                    $num++; 
		                }
		            }
		
					return response()->json(array('tabla' => $tabla));
        }
    }


	
}