<?php 

namespace App\Modulos\ActividadRecreativa\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Observaciones\Observaciones;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;


class MisActividadesController extends Controller
{
	public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',intval($_SESSION['Usuario'][0]))
        	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)
        	->get();

        $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique()->all();

        $persona_pertene_programa=ConfiguracionPersona::whereIn('i_fk_programa',$tipo_programa)
        	->groupby('i_fk_id_persona')
        	->get();
        
        $data=[
        	"persona_pertene_programa"=>$persona_pertene_programa
        ];
        	
		return view('MisActividades.misActividades',$data);
	}


	public function busquedaActividad(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails() && $request['id_actividad']=='')
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {

        	$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',intval($_SESSION['Usuario'][0]))
        	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)

        	->get();

        	$tipo_programa=$persona_programa->pluck('i_fk_programa')->unique()->all();

        	$actividades='';
			$actividades = ActividadRecreativa::whereHas('datosActividad',function($query) use ($tipo_programa){
				$query->whereIn('i_fk_programa',$tipo_programa);
			});

			if($request['id_actividad']){
				$actividades=$actividades->where('i_pk_id',$request['id_actividad']);
			}else{
				$actividades=$actividades->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']]);
			}

			if($request['responsable'])
				$actividades=$actividades->where('i_fk_usuarioResponsable',$request['responsable']);
			

			$actividades=$actividades->get();


			$datos =[
				'actividadesPorRevisar'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::PENDIENTE)->count(),
				'actividadesAprobadas'=>$actividades->whereIn('i_estadoAprobaPrograma', [Configuracion::APROBADO,Configuracion::CONFIRMADO])->count(),
				'actividadesDenegadas'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::DEVUELTO)->count(),
				'actividadesCanceladas'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::CANCELADO)->count(),
				'todasActividades'=>$actividades->whereIn('i_estadoAprobaPrograma',[Configuracion::CANCELADO,Configuracion::DEVUELTO,Configuracion::DEVUELTO,Configuracion::APROBADO,Configuracion::PENDIENTE])->count(),
			];

            return response()->json(array('status' => 'ok', 'errors' => '', 'datos'=>$datos));
        }
    }


    public function actividadesResposableProgramaPendientes(Request $request)
	{

		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
    	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)
    	->get();


    	    $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique()->all();
    	

			$actividades = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates')->whereHas('datosActividad',function($query) use ($tipo_programa){
				$query->whereIn('i_fk_programa',$tipo_programa);
			});

			if($request['idHiden']){
				$actividades=$actividades->where('i_pk_id',$request['idHiden']);
			}else{
				$actividades=$actividades->whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']]);
			}
			$actividades=$actividades->get();
		
		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::PENDIENTE){
			$opcion="Actividades en espera de revisón.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::PENDIENTE);

		}else if($request['opcion']==Configuracion::APROBADO){
			$opcion="Actividades aprobadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->whereIn('i_estadoAprobaPrograma',[Configuracion::APROBADO,Configuracion::CONFIRMADO]);

		}else if($request['opcion']==Configuracion::DEVUELTO){
			$opcion="Actividades denegadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::DEVUELTO);

		}else if($request['opcion']==Configuracion::CANCELADO){
			$opcion="Actividades canceladas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::CANCELADO);

		}else if($request['opcion']==100){
			$opcion="Todas las actividades";
			$color="default";
			$style ="color: black;background-color: grey;";
			$actividades=$actividades->whereIn('i_estadoAprobaPrograma',[Configuracion::CANCELADO,Configuracion::DEVUELTO,Configuracion::DEVUELTO,Configuracion::APROBADO,Configuracion::PENDIENTE]);
		

		}else{
			$opcion="";
			$color="";
			$style="";
		}

		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
		];
		
		return view('MisActividades.tablaMisActividades',$datos);
   
    }

    public function datosprogramacionactividad(Request $request, $id)
	{
		$actividad = ActividadRecreativa::with(['datosActividad' => function($query) {
			$query->with('programa', 'actividad','tematica','componente');
		} ,'acompanates','localidad_comunidad','upz_comunidad','barrio_comunidad','responsable','localidad_escenario', 'upz_escenario', 'barrio_escenario','observaciones.usuario'])
		->find($id);

		return response()->json($actividad);
	}

	public function actividadAprobada(Request $request, $id, $mensaje)
	{

		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaPrograma']=Configuracion::APROBADO;
		$actividad['i_usuarioAprobaPrograma']=$_SESSION['Usuario'][0];
		$actividad->save();

		if($mensaje){
			$obj = new Observaciones();
			$obj['i_fk_id_actividad']=$id;
			$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
			$obj['tx_observacion']=$mensaje;
			$obj['vc_tipoobservacion']=Configuracion::OBSER_PROGRAMACION.' '.Configuracion::toString(Configuracion::APROBADO);
			$obj->save();
		}


		return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));

	}

	public function actividadDevuelta(Request $request, $id, $mensaje)
	{
		
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaPrograma']=Configuracion::DEVUELTO;
		$actividad['i_usuarioAprobaPrograma']=$_SESSION['Usuario'][0];
		$actividad->save();
		
		$obj = new Observaciones();
		$obj['i_fk_id_actividad']=$id;
		$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
		$obj['tx_observacion']=$mensaje;
		$obj['vc_tipoobservacion']=Configuracion::OBSER_PROGRAMACION.' '.Configuracion::toString(Configuracion::DEVUELTO);
		$obj->save();

		return response()->json(array('mensaje' => '<span class="label label-warning">Devuelto</span>'));

	}

	public function actividadCancelada(Request $request, $id , $mensaje)
	{
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaPrograma']=Configuracion::CANCELADO;
		$actividad['i_usuarioAprobaPrograma']=$_SESSION['Usuario'][0];
		$actividad->save();


		$obj = new Observaciones();
		$obj['i_fk_id_actividad']=$id;
		$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
		$obj['tx_observacion']=$mensaje;
		$obj['vc_tipoobservacion']=Configuracion::OBSER_PROGRAMACION.' '.Configuracion::toString(Configuracion::CANCELADO);
		$obj->save();

		

		return response()->json(array('mensaje' => '<span class="label label-danger">Cancelada</span>'));
	}
	
}