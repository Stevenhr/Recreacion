<?php 

namespace App\Modulos\ActividadRecreativa\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use App\Modulos\Observaciones\Observaciones;
use Validator;
use App\Localidad;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use Carbon;

class ConfirmarActividadesController extends Controller
{
	public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		return view('MisActividades.confirmacionActividades');
	}


	public function confirmarBusquedaActividad(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails() && $request['idAct']=='' )
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
        	if(empty ($request['idAct']))
			{
				$actividades = ActividadRecreativa::where('i_fk_usuarioResponsable',$_SESSION['Usuario'][0])
				->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])
				->get();
			}else{
				$actividades = ActividadRecreativa::where('i_pk_id',$request['idAct'])->get();
			}

			$todaAct1=$actividades->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_estadoConfirmaPrograma', 0);

			$datos =[
				'actividadesPendiAproPrograma'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_estadoConfirmaPrograma',0)->count(),
				'actividadesPorConfirmar'=>$actividades->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_estadoConfirmaPrograma',0)->count(),
				'actividadesConfirmadas'=>$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO)->count(),
				'actividadesCanceladasConfirmar'=>$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_CANCELADO)->count(),
				'actividadesReprogramadaConfirmar'=>$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_REPROGRAMADA)->count(),
				'actividadesReprogramadaTodas'=>$actividades->whereIn('i_estadoAprobaPrograma',[Configuracion::APROBADO,Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA,Configuracion::CONFIRMADO_CANCELADO])->union($todaAct1)->count(),
			];

            return response()->json(array('status' => 'ok', 'errors' => '', 'datos'=>$datos));
        }
    }

    public function actividadesConfirmarResponsable(Request $request)
	{

		$actividades = ActividadRecreativa::with('datosActividad.programa','datosActividad.actividad','datosActividad.tematica','datosActividad.componente','acompanates');
		$actividades =$actividades->where('i_fk_usuarioResponsable',$_SESSION['Usuario'][0]);
		$actividades =$actividades->whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']]);
		
		
		
		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::PENDIENTE){
			$opcion="Actividades en espera de confirmaciòn, sin revisión del responsable de programa.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades =$actividades->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_estadoConfirmaPrograma',0);

		}else if($request['opcion']==Configuracion::APROBADO){
			$opcion="Actividades en espera de confirmaciòn, con revisión del responsable de programa.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades =$actividades->where('i_estadoAprobaPrograma', Configuracion::APROBADO)->where('i_estadoConfirmaPrograma',0);

		}else if($request['opcion']==Configuracion::CONFIRMADO){
			$opcion="Actividades confirmadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades =$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO);

		}else if($request['opcion']==Configuracion::CONFIRMADO_REPROGRAMADA){
			$opcion="Actividades reprogramadas y confirmadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades =$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_REPROGRAMADA);

		}else if($request['opcion']==Configuracion::CONFIRMADO_CANCELADO){
			$opcion="Actividades canceladas en confirmación.";
			$color="success";
			$style ="color: white;background-color: #c71c22;";
			$actividades =$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_CANCELADO);

		}else if($request['opcion']==100){ 

	      $opcion="Todas las actividades."; 
	      $color="default"; 
	      $style ="color: black;background-color: grey;"; 
	      $actividades=$actividades->where(function($query) {
				$query->whereIn('i_estadoAprobaPrograma', [Configuracion::APROBADO,Configuracion::CONFIRMADO,Configuracion::CONFIRMADO_REPROGRAMADA,Configuracion::CONFIRMADO_CANCELADO]);
			})->orWhere(function ($query){
				$query->where('i_estadoAprobaPrograma', Configuracion::PENDIENTE)->where('i_estadoConfirmaPrograma',0);
			}); 

		}else{
			$opcion="";
			$color="";
			$style="";
		}

		$actividades =$actividades->get();
		$Localidades = Localidad::all();

		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
			'Localidades'=>$Localidades,
		];
		
		return view('MisActividades.tablaConfirmarActividad',$datos);
   
    }

    public function select_upz(Request $request, $id)
	{
		
		$Locali_recreador=ConfiguracionPersona::where('i_id_localidad',$id)->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_ACTIVIDAD)->get();
		$upzs = Upz::where('IdLocalidad',$id)->get();

		$datos=[
		'upzs'=>$upzs,
		'responsables'=>$Locali_recreador
		];
		return response()->json($datos);
	}

	public function select_barrio(Request $request, $id)
	{
		$upzs = Barrio::where('CodUpz',$id)->get();
		return response()->json($upzs);
	}

    public function datosprogramacionactividadConfirmar(Request $request, $id)
	{
		$actividad = ActividadRecreativa::with(['datosActividad' => function($query) {
			$query->with('programa', 'actividad','tematica','componente');
		} ,'acompanates','localidad_comunidad','upz_comunidad','barrio_comunidad','responsable','localidad_escenario', 'upz_escenario', 'barrio_escenario','observaciones.usuario'])
		->find($id);

		return response()->json($actividad);
	}

	public function confirmarActivida(Request $request, $id)
    {
        $actividad = ActividadRecreativa::find($id);
        $actividad['i_estadoConfirmaPrograma']=Configuracion::CONFIRMADO;
        $actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
        $actividad->save();
        $observacion = new  Observaciones();
		$observacion['i_fk_id_actividad']=$id;
		$observacion['tx_observacion']="Confirmación de la actividad";
		$observacion['i_fk_usuario']=$_SESSION['Usuario'][0];
		$observacion['vc_tipoobservacion']="Observación del responsable de la actividad en la confirmación";
		$observacion->save();

        return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));
    }

    public function noConfirmarActividad(Request $request, $id)
    {
        $actividad = ActividadRecreativa::find($id);
        $actividad['i_estadoConfirmaPrograma']=Configuracion::CONFIRMADO_CANCELADO;
        $actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
        $actividad->save();
        return response()->json(array('mensaje' => '<span class="label label-danger">cancelada</span>'));
    }

    public function buscarActividad(Request $request, $id)
    {
        $actividad = ActividadRecreativa::with(['localidad_escenario', 'upz_escenario', 'barrio_escenario'])->find($id);
        return response()->json(array('datos' =>$actividad));
    }

    public function disponibilidad_reprogramacion(Request $request)
	{
		$messages = [
		    'fechaEjecucionReprogramacion.required'    => 'El campo :attribute se encuentra vacio.',
		    'horaInicioReprogramacion.required'    => 'El campo :attribute se encuentra vacio.',
		    'horaFinReprogramacion.required' => 'El campo :attribute se encuentra vacio.',
		    'direccionReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'escenarioReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'horaImpleReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'puntoEncuprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'localidad_reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'Id_Upz_Reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'Id_Barrio_Reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaEjecucionReprogramacion'=>'required',
			'horaInicioReprogramacion'=>'required',
			'horaFinReprogramacion'=>'required',
			'direccionReprogramacion'=>'required',
			'escenarioReprogramacion'=>'required',
			'horaImpleReprogramacion'=>'required',
			'puntoEncuprogramacion'=>'required',
			'localidad_reprogramacion'=>'required',
			'Id_Upz_Reprogramacion'=>'required',
			'Id_Barrio_Reprogramacion'=>'required',
    	],$messages);

    	
    	$carbon = new \Carbon\Carbon();
    	$dia = date_create($request['fechaRegistoReProgramacion']);
		$date = $carbon->createFromFormat('Y-m-d', date_format($dia, 'Y-m-d'));
		$registrada = $date->addDay(Configuracion::DIAS_PROGRAMACION);

		$carbon = new \Carbon\Carbon();
		$dia2 = date_create($request['fechaEjecucionReprogramacion']);
		$ahora = $carbon->createFromFormat('Y-m-d', date_format($dia2, 'Y-m-d'));

		$hora_implementacion =  strtotime($request['horaImpleReprogramacion']);
		$horainicio =  strtotime($request['horaInicioReprogramacion']);

        if ($validator->fails())
        {
            return response()->json(array('status' => 'Error1', 'errors' => $validator->errors(), 'menj'=>' La fecha que intenta registrar debe ser mayor a la fecha de registro mas 15 días'));
        }

        if ($hora_implementacion > $horainicio){
        	return response()->json(array('status' => 'Error2', 'errors' => $validator->errors(), 'menj'=>' La hora de implementación debe ser menor a la hora de inicio.'));
        }

        if ($ahora->gte($registrada))
        {
        	$actividad = ActividadRecreativa::find($request['idActividadReProgramacion']);
	        	$actividad['i_estadoConfirmaPrograma']=Configuracion::CONFIRMADO_REPROGRAMADA;
	        	$actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
	        	$actividad['d_fechaEjecucion']=$request['fechaEjecucionReprogramacion'];
	        	$actividad['t_horaInicio']=$request['horaInicioReprogramacion'];
	        	$actividad['t_horaFin']=$request['horaFinReprogramacion'];
	        	$actividad['t_horaImplementacion']=$request['horaImpleReprogramacion'];
	        	$actividad['vc_puntoEncuentro']=$request['puntoEncuprogramacion'];
	        	$actividad['vc_direccion']=$request['direccionReprogramacion'];
	        	$actividad['vc_escenario']=$request['escenarioReprogramacion'];
	        	if($actividad['vc_codigoParque']!="")
	        	{
	        		$actividad['vc_codigoParque']=$request['codigoReprogramacion'];
	        	}	        	
	        	$actividad['i_fk_localidadEscenario']=$request['localidad_reprogramacion'];
	        	$actividad['i_fk_upzEscenario']=$request['Id_Upz_Reprogramacion'];
	        	$actividad['i_fk_barrioEscenario']=$request['Id_Barrio_Reprogramacion'];
        	$actividad->save();
            return response()->json(array('status' => 'Modificado'));
        }
        else
        {
        	return response()->json(array('status' => 'NoModificado'));
        }
       
    }    

    public function agregarObservacionConfirmacion(Request $request, $id, $mensaje)
	{
		$observacion = new  Observaciones();
		$observacion['i_fk_id_actividad']=$id;
		$observacion['tx_observacion']=$mensaje;
		$observacion['i_fk_usuario']=$_SESSION['Usuario'][0];
		$observacion['vc_tipoobservacion']="Observación del responsable de la actividad en la confirmación";
		$observacion->save();

		return response()->json($observacion);
	}
	
}