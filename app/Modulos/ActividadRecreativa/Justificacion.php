<?php

namespace App\Modulos\ActividadRecreativa;

use Illuminate\Database\Eloquent\Model;

class Justificacion extends Model
{
    //
    protected $table = 'justificacion';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_justificacion','i_clasificacion'];
	protected $connection = ''; 
	public $timestamps = false;


}
