<?php

namespace App\Modulos\Observaciones;

use Illuminate\Database\Eloquent\Model;

class Observaciones extends Model
{
    //
    protected $table = 'observaciones';
	protected $primaryKey = 'i_pk_observacion';
	protected $fillable = ['i_fk_id_actividad','i_fk_usuario','tx_observacion','vc_tipoobservacion'];
	protected $connection = ''; 
	public $timestamps = true;

	public function usuario()
    {
        return $this->belongsTo('App\Persona','i_fk_usuario');
    }

}
