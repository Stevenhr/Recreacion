<?php

namespace App\Modulos\Configuracion;


class Configuracion
{
    
    
// ESTADOS DEL PROCESO 
    // Gestor
    const INCOMPLETA = -1; 
    const PENDIENTE = 0;

    // Responsable del programa
    const APROBADO = 1;// Aprobado por resposanble del programa 
    const DEVUELTO = 2; //Estado para devolver con observaciones para su arreglo. Eje. Devolver visita le permite al interventor modificar la visita. 
    const CANCELADO = 3; // No se permite modificar nada cuando este cancelado. 

    // Resposable de la actividad 
    const CONFIRMADO = 4; // Confirmada. 
    const CONFIRMADO_CANCELADO = 5; // Cancelada en la confirmacion. 
    const CONFIRMADO_REPROGRAMADA = 6; // Reprogramada y confirmada
    const EJECUTADA = 7; // Actividad con registro de la ejecucion, por aprobacion de ejecucion. 
    

    // Resposanble del programa
    const EJECUTADA_CANCELADA = 8; // Actividad cancelada en la ejecucion
    const EJECUCION_APROBADA = 9; // Actividad aprobada en apobracion de ejecucion 
    const EJECUCION_CANCELADA = 10; // Actividad cancelada en la aprobacion de ejecucion
    const EJECUCION_DENEGADA = 11; // Actividad denegada en la aprobacion de la ejecucion

// Dias validos para porder programar
    const DIAS_PROGRAMACION=15;

    //bloque de dias de la confirmación
    const DIA_ANTEPENULTIMO_CONFIRMACION=4;
    const DIA_ULTIMO_PROGRAMACION=1;
// USUARIOS
	const RESPOSANBLE_ACTIVIDAD = 1; // Solo una localidad, Recreador encargado de ejecutar la actividad
    const GESTOR = 2; // Varias localidades, persona encargada de crear la actividad y asignarcela a un RESPOSANBLE_ACTIVIDAD
    const RESPOSANBLE_PROGRAMA = 3; // Persona encargada de aprobar la actividad creada por el GESTOR
    const OBSERVADOR = 4; // Da observaciones a todas las actividades.
    const ADMINISTRADOR = 5; // Administrador de sistema

    //Tipos de observacion
    const OBSER_PROGRAMACION = "Programación"; // Programación
    const OBSER_EJECUCION = "Ejecución"; // ejecución
    const OBSER_CONFIRMACION = "Confirmación"; // ejecución

    public static function getArrayForSelect()
    {
        return [
          self::PENDIENTE   =>  self::toString(self::PENDIENTE),
          self::APROBADO    =>  self::toString(self::APROBADO),
          self::DEVUELTO    =>  self::toString(self::DEVUELTO),
          self::CANCELADO   =>  self::toString(self::CANCELADO),
        ];
    }

    public static function getPerfilesForSelect()
    {
        return [
            self::RESPOSANBLE_ACTIVIDAD => 'RESPOSANBLE ACTIVIDAD',
            self::GESTOR => 'GESTOR',
            self::RESPOSANBLE_PROGRAMA => 'RESPOSANBLE PROGRAMA',
            self::OBSERVADOR => 'OBSERVADOR'
            //self::ADMINISTRADOR => 'ADMINISTRADOR'
        ];
    }

    static function toString($codigo)
    {
        $estado = '';
        switch ($codigo) {
            case 0:
                $estado = 'Pendiente de revisión.';
            break;
            case 1:
                $estado = 'Aprobado';
            break;
            case 2:
                $estado = 'Devuelto';
            break;
            case 3:
                $estado = 'Cancelado';
            break;
            default:
                $estado = 'otra';
            break;
        }

        return $estado;
    }
}