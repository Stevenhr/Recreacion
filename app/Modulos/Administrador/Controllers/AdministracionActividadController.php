<?php

namespace App\Modulos\Administrador\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Localidad;
use App\Persona;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use App\Modulos\CaracteristicaEjecucion\Entidad;
use App\Modulos\CaracteristicaEjecucion\TipoPoblacion;
use App\Modulos\CaracteristicaEjecucion\Condicion;
use App\Modulos\CaracteristicaEjecucion\Situacion;
use App\Modulos\CaracteristicaEjecucion\Requisito;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;

class AdministracionActividadController extends Controller
{
    //
    public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		$Localidades = Localidad::all();
		$gestores = ConfiguracionPersona::with('persona')->where('i_id_tipo_persona',Configuracion::GESTOR)->get();
		$resposanbles = ConfiguracionPersona::with('persona')->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_ACTIVIDAD)->get();

		$datos=[
            "localidades"=>$Localidades,
            "gestores"=>$gestores->unique('i_fk_id_persona'),
            "resposanbles"=>$resposanbles->unique('i_fk_id_persona'),
		];

		return view('MisActividades.administradorActividad',$datos);
	}

	public function adminBusquedaActividad(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails() && $request['idAct']=='')
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
			
			if(empty ($request['idAct']))
			{
				$qb = ActividadRecreativa::whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])->orWhere('d_fechaEjecucion',null);
						
				if($request['localidad_comunidad']!=''){
					$qb->where('i_fk_localidadComunidad','=',$request['localidad_comunidad']);
				}

				if($request['Id_Upz_Comunidad']!=''){
					$qb->where('i_fk_upzComunidad','=',$request['Id_Upz_Comunidad']);
				}

				if($request['Cod_IDRD']!=''){
					$qb->where('vc_codigoParque','=',$request['Cod_IDRD']);
				}

				if($request['localidad_Escenario']!=''){
					$qb->where('i_fk_localidadEscenario','=',$request['localidad_Escenario']);
				}

				if($request['Id_Upz_Escenario']!=''){
					$qb->where('i_fk_upzEscenario','=',$request['Id_Upz_Escenario']);
				}

				if($request['gestor']!=''){
					$qb->where('i_fk_usuario','=',$request['gestor']);
				}

				if($request['resposanble']!=''){
					$qb->where('i_fk_usuarioResponsable','=',$request['resposanble']);
				}
			}else{
					$qb = ActividadRecreativa::where('i_pk_id',$request['idAct']);
			}

			$actividades = $qb->get();	
			//dd($actividades->count());
			

			$datos =[
				
				//Programacion
				'ProgramacionIncompleta'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::INCOMPLETA)->count(),
				'ProgramacionPendiente'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::PENDIENTE)->count(),
				'ProgramacionAprobado'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::APROBADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				'ProgramacionDevuelto'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::DEVUELTO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				'ProgramacionCancelado'=>$actividades->where('i_estadoAprobaPrograma',Configuracion::CANCELADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),

				//Confirmacion
				'ConfirmacionConfirmadoSinAprobacion'=>$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				'ConfirmacionConfirmado'=>$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				'ConfirmacionCancelada'=>$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_CANCELADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				'ConfirmacionReprogramada'=>$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_REPROGRAMADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										})->count(),
				
				//Ejecucion
				'EjecucionSinAproProgramacion'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoAprobaEjecucion"] == 0);
										})->count(),

				'EjecucionSinConfirmacion'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoAprobaEjecucion"] == 0);
										})->count(), //$actividades->where('i_estado', Configuracion::EJECUTADA)->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','0')->count(),

				'EjecucionSinAproProgramacionNiConfirm'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 &&$value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoAprobaEjecucion"] == 0);
										})->count(),

				'EjecucionEjecutada'=>$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoAprobaEjecucion"] == 0);
										})->count(),//->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','<>','0')->count(),


				//Calificación Ejecucion
				'EjecucionCancelada'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUTADA_CANCELADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										})->count(),
				'EjecucionAprobada'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_APROBADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										})->count(),
				'EjecucionCanceladaEjecutada'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_CANCELADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										})->count(),
				'EjecucionDenegada'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_DENEGADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										})->count(),
				'ActividadesTotal'=>$actividades->count()
			];
			//dd($datos);
            return response()->json(array('status' => 'ok', 'errors' => '', 'datos'=>$datos));
        }
    }


    public function actividadesFiltros(Request $request)
	{
		
		if(empty ($request['idActHiden']))
			{

				$qb = ActividadRecreativa::whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']])->orWhere('d_fechaEjecucion',null);
				

				if($request['localidad_comunidadHiden']!=''){
					$qb->where('i_fk_localidadComunidad',$request['localidad_comunidadHiden']);
				}

				if($request['Id_Upz_ComunidadHiden']!=''){
					$qb->where('i_fk_upzComunidad',$request['Id_Upz_ComunidadHiden']);
				}

				if($request['Cod_IDRDHiden']!=''){
					$qb->where('vc_codigoParque',$request['Cod_IDRDHiden']);
				}

				if($request['localidad_EscenarioHiden']!=''){
					$qb->where('i_fk_localidadEscenario',$request['localidad_EscenarioHiden']);
				}

				if($request['Id_Upz_EscenarioHiden']!=''){
					$qb->where('i_fk_upzEscenario',$request['Id_Upz_EscenarioHiden']);
				}

				if($request['gestorHiden']!=''){
					$qb->where('i_fk_usuario',$request['gestorHiden']);
				}

				if($request['resposanbleHiden']!=''){
					$qb->where('i_fk_usuarioResponsable',$request['resposanbleHiden']);
				}
			}else{
					$qb = ActividadRecreativa::where('i_pk_id',$request['idActHiden']);
			}

			
			$actividades = $qb->get();

		
		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::INCOMPLETA){
			$opcion="Actividades incompletas.";
			$color="warning";
			$style ="color: black;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::INCOMPLETA);


		}else if($request['opcion']==Configuracion::PENDIENTE){
			$opcion="Actividades en espera de revisón.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::PENDIENTE);

		}else if($request['opcion']==Configuracion::APROBADO){
			$opcion="Actividades aprobadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::APROBADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});

		}else if($request['opcion']==Configuracion::DEVUELTO){
			$opcion="Actividades denegadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::DEVUELTO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});

		}else if($request['opcion']==Configuracion::CANCELADO){
			$opcion="Actividades canceladas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaPrograma',Configuracion::CANCELADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});




		}else if($request['opcion']==44){
			$opcion="Actividades confirmadas por el responsable de la actividad sin la revisión y aprobación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});

		}else if($request['opcion']==Configuracion::CONFIRMADO){
			$opcion="Actividades confirmadas por el responsable de la actividad.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoConfirmaPrograma',Configuracion::CONFIRMADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});//->where('i_usuarioAprobaPrograma','<>',0);

		}else if($request['opcion']==Configuracion::CONFIRMADO_CANCELADO){
			$opcion="Actividades canceladas en confirmación.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_CANCELADO)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});

		}else if($request['opcion']==Configuracion::CONFIRMADO_REPROGRAMADA){
			$opcion="Actividades reprogramadas en confirmación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoConfirmaPrograma', Configuracion::CONFIRMADO_REPROGRAMADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] == 0 && $value["i_estadoAprobaEjecucion"] == 0 );
										});




		}else if($request['opcion']==70){
			$opcion="Actividad sin revisión y aprobación de la programación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoAprobaEjecucion"] == 0);
										});//->where('i_usuarioConfirmaPrograma','<>','0');

		}else if($request['opcion']==71){
			$opcion="Actividad sin confirmación del resposanble de la actividad.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoAprobaEjecucion"] == 0);
										});// ->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','0');

		}else if($request['opcion']==72){
			$opcion="Actividad sin aprobación de la programación ni confirmación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] == 0 &&$value["i_estadoAprobaPrograma"] == 0 && $value["i_estadoAprobaEjecucion"] == 0);
										});

		}else if($request['opcion']==Configuracion::EJECUTADA){
			$opcion="Actividades ejecutadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion', Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoAprobaEjecucion"] == 0);
										});//->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','<>','0');

		}else if($request['opcion']==Configuracion::EJECUTADA_CANCELADA){
			$opcion="Actividades ejecutadas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUTADA_CANCELADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										});

		}else if($request['opcion']==Configuracion::EJECUCION_APROBADA){
			$opcion="Actividades ejecutadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_APROBADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										});

		}else if($request['opcion']==Configuracion::EJECUCION_CANCELADA){
			$opcion="Actividades ejecutadas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_CANCELADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										});

		}else if($request['opcion']==Configuracion::EJECUCION_DENEGADA){
			$opcion="Actividades ejecutadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_DENEGADA)->filter(function ($value, $key) {
			   							 return   ($value["i_estadoConfirmaPrograma"] != 0 &&$value["i_estadoAprobaPrograma"] != 0 && $value["i_estadoRegistroEjecucion"] != 0);
										});

		}else{
			$opcion="";
			$color="";
			$style="";
		}

		$Localidades = Localidad::all();
		$entidades = Entidad::all();
		$tipoPoblaciones = TipoPoblacion::all();
		$condiciones = Condicion::all();
		$situaciones = Situacion::all();
		$requisitos = Requisito::all();

		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
			'Localidades'=>$Localidades,
			'entidades'=>$entidades,
			'tipoPoblaciones'=>$tipoPoblaciones,
			'condiciones'=>$condiciones,
			'situaciones'=>$situaciones,
			'requisitos'=>$requisitos,
		];
		
		return view('AdminitracionActividad.adminitracionActividad',$datos);
   
    }


   
}
