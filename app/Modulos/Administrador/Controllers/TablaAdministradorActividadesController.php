<?php

namespace App\Modulos\Administrador\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Localidad;
use App\Persona;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;

class TablaAdministradorActividadesController extends Controller
{
    //
    public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

    public function confirmarActivida(Request $request, $id)
    {
        $actividad = ActividadRecreativa::find($id);
        if(!is_null($actividad['d_fechaEjecucion'])){
	        $actividad['i_estado']=Configuracion::CONFIRMADO;
	        $actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
	        $actividad->save();
	        return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));	     
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para aprobación</span>'));
		}
    }

    public function noConfirmarActividad(Request $request, $id)
    {
        $actividad = ActividadRecreativa::find($id);
        if(!is_null($actividad['d_fechaEjecucion'])){
	        $actividad['i_estado']=Configuracion::CONFIRMADO_CANCELADO;
	        $actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
	        $actividad->save();
	        return response()->json(array('mensaje' => '<span class="label label-danger">cancelada</span>'));
        }else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para aprobación</span>'));
		}
    }

    public function buscarActividad(Request $request, $id)
    {
        $actividad = ActividadRecreativa::with(['localidad_escenario', 'upz_escenario', 'barrio_escenario'])->find($id);
        return response()->json(array('datos' =>$actividad));
    }

    public function actividadAprobada(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);

		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::APROBADO;
			$actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para aprobación</span>'));
		}

		
	}

	public function actividadDevuelta(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::DEVUELTO;
			$actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-warning">Devuelto</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para la devolución</span>'));
		}
	}

	public function actividadCancelada(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::CANCELADO;
			$actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-danger">Cancelada</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para la devolución</span>'));
		}
	}

	 public function disponibilidad_reprogramacion(Request $request)
	{
		$messages = [
		    'fechaEjecucionReprogramacion.required'    => 'El campo :attribute se encuentra vacio.',
		    'horaInicioReprogramacion.required'    => 'El campo :attribute se encuentra vacio.',
		    'horaFinReprogramacion.required' => 'El campo :attribute se encuentra vacio.',
		    'direccionReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'escenarioReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'localidad_reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'horaImpleReprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'puntoEncuprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'Id_Upz_Reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		    'Id_Barrio_Reprogramacion.required'      => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaEjecucionReprogramacion'=>'required',
			'horaInicioReprogramacion'=>'required',
			'horaFinReprogramacion'=>'required',
			'direccionReprogramacion'=>'required',
			'escenarioReprogramacion'=>'required',
			'horaImpleReprogramacion'=>'required',
			'puntoEncuprogramacion'=>'required',
			'localidad_reprogramacion'=>'required',
			'Id_Upz_Reprogramacion'=>'required',
			'Id_Barrio_Reprogramacion'=>'required',
    	],$messages);

    	
    	$carbon = new \Carbon\Carbon();
    	$dia = date_create($request['fechaRegistoReProgramacion']);
		$date = $carbon->createFromFormat('Y-m-d', date_format($dia, 'Y-m-d'));
		$registrada = $date->addDay(Configuracion::DIAS_PROGRAMACION);

		$carbon = new \Carbon\Carbon();
		$dia2 = date_create($request['fechaEjecucionReprogramacion']);
		$ahora = $carbon->createFromFormat('Y-m-d', date_format($dia2, 'Y-m-d'));

        if ($validator->fails())
        {
            return response()->json(array('status' => 'Error', 'errors' => $validator->errors()));
        }

        /*if ($ahora->gte($registrada))
        {*/
        	$actividad = ActividadRecreativa::find($request['idActividadReProgramacion']);
	        	$actividad['i_estado']=Configuracion::CONFIRMADO_REPROGRAMADA;
	        	$actividad['i_usuarioConfirmaPrograma']=$_SESSION['Usuario'][0];
	        	$actividad['d_fechaEjecucion']=$request['fechaEjecucionReprogramacion'];
	        	$actividad['t_horaInicio']=$request['horaInicioReprogramacion'];
	        	$actividad['t_horaFin']=$request['horaFinReprogramacion'];
	        	$actividad['t_horaImplementacion']=$request['horaImpleReprogramacion'];
	        	$actividad['vc_puntoEncuentro']=$request['puntoEncuprogramacion'];
	        	$actividad['vc_direccion']=$request['direccionReprogramacion'];
	        	$actividad['vc_escenario']=$request['escenarioReprogramacion'];
	        	if($actividad['vc_codigoParque']!="")
	        	{
	        		$actividad['vc_codigoParque']=$request['codigoReprogramacion'];
	        	}	        	
	        	$actividad['i_fk_localidadEscenario']=$request['localidad_reprogramacion'];
	        	$actividad['i_fk_upzEscenario']=$request['Id_Upz_Reprogramacion'];
	        	$actividad['i_fk_barrioEscenario']=$request['Id_Barrio_Reprogramacion'];
        	$actividad->save();
            return response()->json(array('status' => 'Modificado'));
        /*}
        else
        {
        	return response()->json(array('status' => 'NoModificado'));
        }*/
       
    }  

    public function habilitarProgramacion(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_habilitacion']=2;
		$actividad->save();
		return response()->json(array('mensaje' => '<span class="label label-default">Habilitada!!!</span>'));
	}

	public function InhabilitarProgramacion(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_habilitacion']=1;
		$actividad->save();
		return response()->json(array('mensaje' => '<span class="label label-default">Inhabilitada!!!</span>'));
	}


	public function habilitarEjecucion(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_habilitacion']=3;
		$actividad->save();
		return response()->json(array('mensaje' => '<span class="label label-default">Habilitada!!!</span>'));
	}

	public function InhabilitarEjecucion(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		$actividad['i_habilitacion']=1;
		$actividad->save();
		return response()->json(array('mensaje' => '<span class="label label-default">Inhabilitada!!!</span>'));
	}



	public function ejecucionAprobadaResposable(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::EJECUCION_APROBADA;
			$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para la devolución</span>'));
		}
	}

	public function ejecucionDenegadaResponsable(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::EJECUCION_DENEGADA;
			$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-warning">Devuelto</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para la devolución</span>'));
		}
	}

	public function ejecucionCanceladaResponsable(Request $request, $id)
	{
		$actividad = ActividadRecreativa::find($id);
		if(!is_null($actividad['d_fechaEjecucion'])){
			$actividad['i_estado']=Configuracion::EJECUCION_CANCELADA;
			$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
			$actividad->save();
			return response()->json(array('mensaje' => '<span class="label label-danger">Cancelada</span>'));
		}else{
			return response()->json(array('mensaje' => '<span class="label label-danger">Falata información para la devolución</span>'));
		}
	}

}
