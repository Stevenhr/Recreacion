<?php 

namespace App\Modulos\Ejecucion\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use App\Modulos\Observaciones\Observaciones;
use Validator;


class MiGestionEjecucionControllerResponsble extends Controller
{

	public function inicio()
	{	
		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',intval($_SESSION['Usuario'][0]))
        	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)
        	->get();

        $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique()->all();

        $persona_pertene_programa=ConfiguracionPersona::whereIn('i_fk_programa',$tipo_programa)
        	->groupby('i_fk_id_persona')
        	->get();
        
        $data=[
        	"persona_pertene_programa"=>$persona_pertene_programa
        ];

		return view('MisActividades.gestionEjecucionResponsable',$data);
	}

	public function busquedaMiGestionEjecuciones(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails() && $request['idAct']=='')
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {

			$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
		    	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)
		    	->get();

		    	$idPrograma=$persona_programa->pluck('i_fk_programa')->unique()->all();


		    $actividades= ActividadRecreativa::with(['datosActividad'=>function ($query) use ($idPrograma)
		                    {
		                        $query->whereIn('i_fk_programa',$idPrograma)->get();
		                    }]);
			
			if(!empty ($request['idAct']))
				$actividades=$actividades->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']]);

			if($request['idAct'])
				$actividades = $actividades->where('i_pk_id',$request['idAct']);
			
			if($request['responsable'])
				$actividades=$actividades->where('i_fk_usuarioResponsable',$request['responsable']);
			
			$actividades->get();


			$datos =[
				'actividadesPorRevisar'=>$actividades->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0)->count(),
				'actividadesAprobadas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_APROBADA)->count(),
				'actividadesDenegadas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_DENEGADA)->count(),
				'actividadesCanceladas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_CANCELADA)->count(),
				'actividadesTodas'=>$actividades->whereIn('i_estadoAprobaEjecucion',[Configuracion::EJECUCION_CANCELADA,Configuracion::EJECUCION_DENEGADA,Configuracion::EJECUCION_APROBADA])->count(),
			];

            return response()->json(array('status' => 'ok', 'errors' => $validator->errors(), 'datos'=>$datos));
        }
    }

    public function ejecucionesGestion(Request $request)
	{

    	$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
    	->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_PROGRAMA)
    	->get();

    	$idPrograma=$persona_programa->pluck('i_fk_programa')->unique()->all();

		$actividades = ActividadRecreativa::with(['datosActividad'=>function ($query) use ($idPrograma)
                    {
                        $query->whereIn('i_fk_programa',$idPrograma)->get();
                    }])
		->whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']])
		->get();
		
		

		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::EJECUTADA){ //Ejecucion pendiente
			$opcion="Ejecuciones en espera de revisón.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0);

		}else if($request['opcion']==Configuracion::EJECUCION_APROBADA){
			$opcion="Ejecuciones aprobadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_APROBADA);

		}else if($request['opcion']==Configuracion::EJECUCION_DENEGADA){
			$opcion="Ejecuciones denegadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_DENEGADA);

		}else if($request['opcion']==Configuracion::EJECUCION_CANCELADA){
			$opcion="Ejecuciones canceladas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_CANCELADA);

		}else if($request['opcion']==100){  
 
	        $opcion="Todas las actividades.";  
	        $color="default";  
	        $style ="color: black;background-color: grey;";  
	        $actividades=$actividades->whereIn('i_estadoAprobaEjecucion',[Configuracion::EJECUCION_CANCELADA,Configuracion::EJECUCION_DENEGADA,Configuracion::EJECUCION_APROBADA]);  
	        
	    }else{
			$opcion="";
			$color="";
			$style="";
			$actividades="";
		}

		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
		];
		
		return view('MisActividades.tablaGestionEjecucion',$datos);
   
    }



    public function ejecucionAprobadaResposable(Request $request, $id, $mensaje)
	{
		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
    	->get();
        $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique();

		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaEjecucion']=Configuracion::EJECUCION_APROBADA;
		$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
		$actividad['vc_tipoPerosnaEjecu']=$tipo_programa;
		$actividad->save();

		if($mensaje){
			$obj = new Observaciones();
			$obj['i_fk_id_actividad']=$id;
			$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
			$obj['tx_observacion']=$mensaje;
			$obj['vc_tipoobservacion']=Configuracion::OBSER_EJECUCION.' '.Configuracion::toString(Configuracion::APROBADO);
			$obj->save();
		}

		return response()->json(array('mensaje' => '<span class="label label-success">Aprobado</span>'));
	}

	public function ejecucionDenegadaResponsable(Request $request, $id, $mensaje)
	{
		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
    	->get();
        $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique();

		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaEjecucion']=Configuracion::EJECUCION_DENEGADA;
		$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
		$actividad['vc_tipoPerosnaEjecu']=$tipo_programa;
		$actividad->save();

		$obj = new Observaciones();
		$obj['i_fk_id_actividad']=$id;
		$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
		$obj['tx_observacion']=$mensaje;
		$obj['vc_tipoobservacion']=Configuracion::OBSER_EJECUCION.' '.Configuracion::toString(Configuracion::DEVUELTO);
		$obj->save();

		return response()->json(array('mensaje' => '<span class="label label-warning">Devuelto</span>'));
	}

	public function ejecucionCanceladaResponsable(Request $request, $id, $mensaje)
	{
		$persona_programa=ConfiguracionPersona::where('i_fk_id_persona',$_SESSION['Usuario'][0])
    	->get();
        $tipo_programa=$persona_programa->pluck('i_fk_programa')->unique();

		$actividad = ActividadRecreativa::find($id);
		$actividad['i_estadoAprobaEjecucion']=Configuracion::EJECUCION_CANCELADA;
		$actividad['i_usuarioAprobaEjecucion']=$_SESSION['Usuario'][0];
		$actividad['vc_tipoPerosnaEjecu']=$tipo_programa;
		$actividad->save();

		$obj = new Observaciones();
		$obj['i_fk_id_actividad']=$id;
		$obj['i_fk_usuario']=$_SESSION['Usuario'][0];
		$obj['tx_observacion']=$mensaje;
		$obj['vc_tipoobservacion']=Configuracion::OBSER_EJECUCION.' '.Configuracion::toString(Configuracion::EJECUCION_CANCELADA);
		$obj->save();

		return response()->json(array('mensaje' => '<span class="label label-danger">Cancelada</span>'));
	}

	public function agregarObservacionEjecucion(Request $request, $id, $mensaje)
	{
		$observacion = new  Observaciones();
		$observacion['i_fk_id_actividad']=$id;
		$observacion['tx_observacion']=$mensaje;
		$observacion['i_fk_usuario']=$_SESSION['Usuario'][0];
		$observacion['vc_tipoobservacion']="Observación del gestor en ejecución";
		$observacion->save();

		return response()->json($observacion);
	}


}