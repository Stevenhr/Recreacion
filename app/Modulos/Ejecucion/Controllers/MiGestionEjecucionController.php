<?php 

namespace App\Modulos\Ejecucion\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use App\Localidad;
use App\Modulos\CaracteristicaEjecucion\Entidad;
use App\Modulos\CaracteristicaEjecucion\TipoPoblacion;
use App\Modulos\CaracteristicaEjecucion\Condicion;
use App\Modulos\CaracteristicaEjecucion\Situacion;
use App\Modulos\CaracteristicaEjecucion\Requisito;
use Validator;


class MiGestionEjecucionController extends Controller
{

	public function inicio()
	{
		return view('MisActividades.gestionEjecucion');
	}

	public function busquedaMiGestionEjecuciones(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails() && $request['idAct']=='')
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {

			if(empty ($request['idAct']))
			{
				$actividades = ActividadRecreativa::where('i_fk_usuario',$_SESSION['Usuario'][0])
				->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']])
				->get();
			}else{
				$actividades = ActividadRecreativa::where('i_pk_id',$request['idAct'])->get();
			}

			$todaAct1=$actividades->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0);
			$datos =[
				'actividadesPorRevisar'=>$actividades->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0)->count(),
				'actividadesAprobadas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_APROBADA)->count(),
				'actividadesDenegadas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_DENEGADA)->count(),
				'actividadesCanceladas'=>$actividades->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_CANCELADA)->count(),
				'actividadesTodas'=>$actividades->whereIn('i_estadoAprobaEjecucion',[Configuracion::EJECUCION_CANCELADA,Configuracion::EJECUCION_DENEGADA,Configuracion::EJECUCION_APROBADA])->union($todaAct1)->count(),
			];

            return response()->json(array('status' => 'ok', 'errors' => '', 'datos'=>$datos));
        }
    }

    public function ejecucionesGestion(Request $request)
	{

    	$actividades = ActividadRecreativa::where('i_fk_usuario',$_SESSION['Usuario'][0])
		->whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']]);
		

		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::EJECUTADA){ //Ejecucion pendiente
			$opcion="Ejecuciones en espera de revisón.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades=$actividades->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0);

		}else if($request['opcion']==Configuracion::EJECUCION_APROBADA){
			$opcion="Ejecuciones aprobadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_APROBADA);

		}else if($request['opcion']==Configuracion::EJECUCION_DENEGADA){
			$opcion="Ejecuciones denegadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_DENEGADA);

		}else if($request['opcion']==Configuracion::EJECUCION_CANCELADA){
			$opcion="Ejecuciones canceladas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estadoAprobaEjecucion',Configuracion::EJECUCION_CANCELADA);

		}else if($request['opcion']==100){  
 
	        $opcion="Todas las actividades.";  
	        $color="default";  
	        $style ="color: black;background-color: grey;";  
	        //$actividades=$actividades->whereIn('i_estadoAprobaEjecucion',[Configuracion::EJECUCION_CANCELADA,Configuracion::EJECUCION_DENEGADA,Configuracion::EJECUCION_APROBADA])

	        $actividades=$actividades->where(function($query) {
				$query->whereIn('i_estadoAprobaEjecucion',[Configuracion::EJECUCION_CANCELADA,Configuracion::EJECUCION_DENEGADA,Configuracion::EJECUCION_APROBADA]);
			})->orWhere(function ($query){
				$query->where('i_estadoRegistroEjecucion',Configuracion::EJECUTADA)->where('i_estadoAprobaEjecucion',0);
			}); 

	    }else{
			$opcion="";
			$color="";
			$style="";
			$actividades="";
		}

		$actividades =$actividades->get();
		$Localidades = Localidad::all();
		$entidades = Entidad::all();
		$tipoPoblaciones = TipoPoblacion::all();
		$condiciones = Condicion::all();
		$situaciones = Situacion::all();
		$requisitos = Requisito::all();


		$datos=[

			'Localidades'=>$Localidades,
			'entidades'=>$entidades,
			'tipoPoblaciones'=>$tipoPoblaciones,
			'condiciones'=>$condiciones,
			'situaciones'=>$situaciones,
			'requisitos'=>$requisitos,

			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
		];
		
		return view('MisActividades.tablaGestionEjecucion',$datos);
   
    }


}