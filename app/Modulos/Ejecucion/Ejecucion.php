<?php

namespace App\Modulos\Ejecucion;

use Illuminate\Database\Eloquent\Model;

class Ejecucion extends Model
{
    //
    protected $table = 'ejecucion';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['i_fk_id_actividad','vc_poblacion','i_fk_id_entidad','i_fk_id_localidad','i_fk_id_upz','i_fk_id_barrio','i_fk_id_tipo','i_fk_id_condicion','i_fk_id_situacion','i_0a5_f','i_0a5_m','i_6a12_f','i_6a12_m','i_13a17_f','i_13a17_m','i_18a26_f','i_18a26_m','i_27a59_f','i_27a59_m','i_60_f','i_60_m'];
	protected $connection = ''; 
	public $timestamps = true;

	public function entidad() 
	{
        return $this->belongsTo('App\Modulos\CaracteristicaEjecucion\Entidad', 'i_fk_id_entidad'); 
    }	

    public function localidad() 
    {
        return $this->belongsTo('App\Modulos\Parques\Localidad', 'i_fk_id_localidad'); 
    }

    public function upz() 
    {
        return $this->belongsTo('App\Modulos\Parques\Upz', 'i_fk_id_upz'); 
    }

    public function barrio() 
    {
        return $this->belongsTo('App\Modulos\Parques\Barrio', 'i_fk_id_barrio'); 
    }

    public function tipoPoblacion() 
    {
        return $this->belongsTo('App\Modulos\CaracteristicaEjecucion\TipoPoblacion', 'i_fk_id_tipo'); 
    }

    public function condicion() 
    {
        return $this->belongsTo('App\Modulos\CaracteristicaEjecucion\Condicion', 'i_fk_id_condicion'); 
    }

    public function situacion() 
    {
        return $this->belongsTo('App\Modulos\CaracteristicaEjecucion\Situacion', 'i_fk_id_situacion'); 
    }

    public function actividad()
    {
        return $this->belongsTo('App\Modulos\ActividadRecreativa\ActividadRecreativa','i_fk_id_actividad');
    }

}
