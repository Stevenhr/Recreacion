<?php

namespace App\Modulos\Ejecucion;

use Illuminate\Database\Eloquent\Model;

class EjecucionEncuesta extends Model
{
    //
    protected $table = 'ejecucionencuesta';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['i_fk_id_actividad','i_puntulidad','i_divulgacion','i_escenariomontaje','i_cumplimineto','i_variedad','i_imageninstitucional','i_seguridad'];
	protected $connection = ''; 
	public $timestamps = true;

}
