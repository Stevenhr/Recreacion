<?php

namespace App\Modulos\Ejecucion;

use Illuminate\Database\Eloquent\Model;

class EjecucionRequisito extends Model
{
    //
    protected $table = 'ejecucionrequisitos';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['i_fk_id_actividad','i_fk_id_requisito','tx_causa','tx_accion'];
	protected $connection = ''; 
	public $timestamps = true;

	public function requisito() 
	{
        return $this->belongsTo('App\Modulos\CaracteristicaEjecucion\Requisito', 'i_fk_id_requisito'); 
    }

}
