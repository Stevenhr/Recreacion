<?php

namespace App\Modulos\Reportes\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Localidad;
use App\Persona;
use App\Modulos\Parques\Upz;
use App\Modulos\Parques\Barrio;
use App\Modulos\Programa\Programa;
use App\Modulos\Actividad\Actividad;
use App\Modulos\Tematica\Tematica;
use App\Modulos\Componente\Componente;
use App\Modulos\ActividadRecreativa\ActividadRecreativa;
use App\Modulos\Usuario\ConfiguracionPersona;
use App\Modulos\Configuracion\Configuracion;
use App\Http\Controllers\Controller;
use Idrd\Usuarios\Repo\PersonaInterface;
use Validator;
use DB;

class ReporteGeneralController extends Controller
{
    //
    public function __construct(PersonaInterface $repositorio_personas)
	{
		$this->repositorio_personas = $repositorio_personas;
	}

	public function inicio()
	{
		$Localidades = Localidad::all();
		$gestores = ConfiguracionPersona::with('persona')->where('i_id_tipo_persona',Configuracion::GESTOR)->get();
		$resposanbles = ConfiguracionPersona::with('persona')->where('i_id_tipo_persona',Configuracion::RESPOSANBLE_ACTIVIDAD)->get();
		$programa= Programa::all();
		$tematica= Tematica::all();
		$componente= Componente::all();

		$actividades= Actividad::all();
		$datos=[
            "localidades"=>$Localidades,
            "gestores"=>$gestores->unique('i_fk_id_persona'),
            "resposanbles"=>$resposanbles->unique('i_fk_id_persona'),
            "programas"=>$programa,
            "actividades"=>$actividades,
            "tematicas"=>$tematica,
            "componentes"=>$componente,
		];

		return view('Reportes.viewReporteGeneral',$datos);
	}

	public function reporteBusquedaActividades(Request $request)
	{
		$messages = [
		    'fechaInicio.required'    => 'El campo :attribute se encuentra vacio.',
		    'fechaFin.required'    => 'El campo :attribute se encuentra vacio.',
		];

		$validator = Validator::make($request->all(),
	    [
			'fechaInicio' => 'required',
			'fechaFin' => 'required',
    	],$messages);
        
        if ($validator->fails())
        {
            return response()->json(array('status' => 'error', 'errors' => $validator->errors()));
        }
        else
        {
        		$id_programa=$request['programa'];
        		$actividad=$request['actividad'];
        		$tematica=$request['tematica'];
        		$componente=$request['componente'];

				$qb = ActividadRecreativa::with('datosActividad','ejecuciones')->whereHas('datosActividad', function ($query) use ($id_programa, $actividad, $tematica,$componente) {
	            	if($id_programa!=''){
	            		$query->where('i_fk_programa',$id_programa);
	            	}
	            	if($actividad!=''){
	            		$query->where('i_fk_actividad',$actividad);
	            	}
	            	if($tematica!=''){
	            		$query->where('i_fk_tematica',$tematica);
	            	}
	            	if($componente!=''){
	            		$query->where('i_fk_componente',$componente);
	            	}
	        	})->whereBetween('d_fechaEjecucion',[$request['fechaInicio'],$request['fechaFin']]);
						
				if($request['localidad_comunidad']!=''){
					$qb->where('i_fk_localidadComunidad','=',$request['localidad_comunidad']);
				}

				if($request['Id_Upz_Comunidad']!=''){
					$qb->where('i_fk_upzComunidad','=',$request['Id_Upz_Comunidad']);
				}

				if($request['Cod_IDRD']!=''){
					$qb->where('vc_codigoParque','=',$request['Cod_IDRD']);
				}

				if($request['localidad_Escenario']!=''){
					$qb->where('i_fk_localidadEscenario','=',$request['localidad_Escenario']);
				}

				if($request['Id_Upz_Escenario']!=''){
					$qb->where('i_fk_upzEscenario','=',$request['Id_Upz_Escenario']);
				}

				if($request['gestor']!=''){
					$qb->where('i_fk_usuario','=',$request['gestor']);
				}

				if($request['resposanble']!=''){
					$qb->where('i_fk_usuarioResponsable','=',$request['resposanble']);
				}

			$qb->where('i_estadoAprobaEjecucion', Configuracion::EJECUCION_APROBADA);
			$actividades = $qb->get();	

			//dd($actividades->pluck('i_pk_id')->toArray());
			
			$ejecuciones = DB::table('ejecucion')
					->select('i_fk_id_situacion','i_fk_id_condicion','i_fk_id_tipo',
							DB::raw('SUM(i_0a5_f) as i_0a5_f'), 
							DB::raw('SUM(i_0a5_m) as i_0a5_m'), 
							DB::raw('SUM(i_6a12_f) as i_6a12_f'),
							DB::raw('SUM(i_6a12_m) as i_6a12_m'),
							DB::raw('SUM(i_13a17_f) as i_13a17_f'),
							DB::raw('SUM(i_13a17_m) as i_13a17_m'),
							DB::raw('SUM(i_18a26_f) as i_18a26_f'),
							DB::raw('SUM(i_18a26_m) as i_18a26_m'),
							DB::raw('SUM(i_27a59_f) as i_27a59_f'),
							DB::raw('SUM(i_27a59_m) as i_27a59_m'),
							DB::raw('SUM(i_60_f) as i_60_f'),
							DB::raw('SUM(i_60_m) as i_60_m')
					)
                    ->whereIn('i_fk_id_actividad', $actividades->pluck('i_pk_id')->toArray())
                    ->get();

			$totalAsistentes= $ejecuciones[0]->i_0a5_f+$ejecuciones[0]->i_0a5_m+$ejecuciones[0]->i_6a12_f+$ejecuciones[0]->i_6a12_m+$ejecuciones[0]->i_13a17_f+ $ejecuciones[0]->i_13a17_m+	$ejecuciones[0]->i_18a26_f +$ejecuciones[0]->i_18a26_m+$ejecuciones[0]->i_27a59_f+$ejecuciones[0]->i_27a59_m+$ejecuciones[0]->i_60_f+$ejecuciones[0]->i_60_m;		

			$totalAsistentesMasculino = $ejecuciones[0]->i_0a5_m+$ejecuciones[0]->i_6a12_m+$ejecuciones[0]->i_13a17_m+$ejecuciones[0]->i_18a26_m+$ejecuciones[0]->i_27a59_m+$ejecuciones[0]->i_60_m;

			$totalAsistentesFemenino = $ejecuciones[0]->i_0a5_f+$ejecuciones[0]->i_6a12_f+$ejecuciones[0]->i_13a17_f+$ejecuciones[0]->i_18a26_f+$ejecuciones[0]->i_27a59_f+$ejecuciones[0]->i_60_f;

			$datos=[
				'numeroActividades'=>$actividades->count(),
				'numeroTotalAsitentes'=>$totalAsistentes,
				'ejecuciones'=>$ejecuciones[0],
				'totalAsistentesMasculino'=>$totalAsistentesMasculino,
				'totalAsistentesFemenino'=>$totalAsistentesFemenino,
				'i_0a5_t'=>$ejecuciones[0]->i_0a5_f+$ejecuciones[0]->i_0a5_m,
				'i_6a12_t'=>$ejecuciones[0]->i_6a12_f+$ejecuciones[0]->i_6a12_m,
				'i_13a17_t'=>$ejecuciones[0]->i_13a17_f+$ejecuciones[0]->i_13a17_m,
				'i_18a26_t'=>$ejecuciones[0]->i_18a26_f+$ejecuciones[0]->i_18a26_m,
				'i_27a59_t'=>$ejecuciones[0]->i_27a59_f+$ejecuciones[0]->i_27a59_m,
				'i_60_t'=>$ejecuciones[0]->i_60_f+$ejecuciones[0]->i_60_m,
				'totalGeneros'=>$totalAsistentesMasculino+$totalAsistentesFemenino

			];
			//dd($datos);
            return response()->json(array('status' => 'ok', 'errors' => $validator->errors(), 'datos'=>$datos));
        }
    }


    public function actividadesFiltros(Request $request)
	{
		
		if(empty ($request['idActHiden']))
			{

				$qb = ActividadRecreativa::whereBetween('d_fechaEjecucion',[$request['fechaInicioHiden'],$request['fechaFinHiden']])->orWhere('d_fechaEjecucion',null);
				

				if($request['localidad_comunidadHiden']!=''){
					$qb->where('i_fk_localidadComunidad',$request['localidad_comunidadHiden']);
				}

				if($request['Id_Upz_ComunidadHiden']!=''){
					$qb->where('i_fk_upzComunidad',$request['Id_Upz_ComunidadHiden']);
				}

				if($request['Cod_IDRDHiden']!=''){
					$qb->where('vc_codigoParque',$request['Cod_IDRDHiden']);
				}

				if($request['localidad_EscenarioHiden']!=''){
					$qb->where('i_fk_localidadEscenario',$request['localidad_EscenarioHiden']);
				}

				if($request['Id_Upz_EscenarioHiden']!=''){
					$qb->where('i_fk_upzEscenario',$request['Id_Upz_EscenarioHiden']);
				}

				if($request['gestorHiden']!=''){
					$qb->where('i_fk_usuario',$request['gestorHiden']);
				}

				if($request['resposanbleHiden']!=''){
					$qb->where('i_fk_usuarioResponsable',$request['resposanbleHiden']);
				}
			}else{
					$qb = ActividadRecreativa::where('i_pk_id',$request['idActHiden']);
			}

			
			$actividades = $qb->get();

		
		$opcion="";
		$color="";
		$style="";
		
		if($request['opcion']==Configuracion::INCOMPLETA){
			$opcion="Actividades incompletas.";
			$color="warning";
			$style ="color: black;background-color: #dd5600;";
			$actividades=$actividades->where('i_estado',Configuracion::INCOMPLETA);


		}else if($request['opcion']==Configuracion::PENDIENTE){
			$opcion="Actividades en espera de revisón.";
			$color="default";
			$style ="color: black;background-color: #eeeeee;";
			$actividades=$actividades->where('i_estado',Configuracion::PENDIENTE);

		}else if($request['opcion']==Configuracion::APROBADO){
			$opcion="Actividades aprobadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->whereIn('i_estado',[Configuracion::APROBADO]);

		}else if($request['opcion']==Configuracion::DEVUELTO){
			$opcion="Actividades denegadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estado',Configuracion::DEVUELTO);

		}else if($request['opcion']==Configuracion::CANCELADO){
			$opcion="Actividades canceladas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estado',Configuracion::CANCELADO);

		}else if($request['opcion']==44){
			$opcion="Actividades confirmadas por el responsable de la actividad sin la revisión y aprobación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::CONFIRMADO)->where('i_usuarioAprobaPrograma','=','0');

		}else if($request['opcion']==Configuracion::CONFIRMADO){
			$opcion="Actividades confirmadas por el responsable de la actividad.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::CONFIRMADO)->filter(function ($value, $key) {
			   								return   ($value["i_usuarioAprobaPrograma"] != 0 );
										  });//->where('i_usuarioAprobaPrograma','<>',0);

		}else if($request['opcion']==Configuracion::CONFIRMADO_CANCELADO){
			$opcion="Actividades canceladas en confirmación.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estado',Configuracion::CONFIRMADO_CANCELADO);

		}else if($request['opcion']==Configuracion::CONFIRMADO_REPROGRAMADA){
			$opcion="Actividades reprogramadas en confirmación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::CONFIRMADO_REPROGRAMADA);

		}else if($request['opcion']==70){
			$opcion="Actividad sin revisión y aprobación de la programación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUTADA)->where('i_usuarioAprobaPrograma','=','0')->filter(function ($value, $key) {
			   								return   ($value["i_usuarioConfirmaPrograma"] != 0 );
										  });//->where('i_usuarioConfirmaPrograma','<>','0');

		}else if($request['opcion']==71){
			$opcion="Actividad sin confirmación del resposanble de la actividad.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   								return   ($value["i_usuarioAprobaPrograma"] != 0 && $value["i_usuarioConfirmaPrograma"] == 0 );
										  });// ->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','0');

		}else if($request['opcion']==72){
			$opcion="Actividad sin aprobación de la programación ni confirmación.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUTADA)->where('i_usuarioAprobaPrograma','=','0')->where('i_usuarioConfirmaPrograma','=','0');

		}else if($request['opcion']==Configuracion::EJECUTADA){
			$opcion="Actividades ejecutadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUTADA)->filter(function ($value, $key) {
			   								return   ($value["i_usuarioAprobaPrograma"] != 0 && $value["i_usuarioConfirmaPrograma"] != 0 );
										  });//->where('i_usuarioAprobaPrograma','<>','0')->where('i_usuarioConfirmaPrograma','<>','0');

		}else if($request['opcion']==Configuracion::EJECUTADA_CANCELADA){
			$opcion="Actividades ejecutadas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUTADA_CANCELADA);

		}else if($request['opcion']==Configuracion::EJECUCION_APROBADA){
			$opcion="Actividades ejecutadas.";
			$color="success";
			$style ="color: white;background-color: #6b9c35;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUCION_APROBADA);

		}else if($request['opcion']==Configuracion::EJECUCION_CANCELADA){
			$opcion="Actividades ejecutadas.";
			$color="danger";
			$style ="color: white;background-color: #c71c22;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUCION_CANCELADA);

		}else if($request['opcion']==Configuracion::EJECUCION_DENEGADA){
			$opcion="Actividades ejecutadas.";
			$color="warning";
			$style ="color: white;background-color: #dd5600;";
			$actividades=$actividades->where('i_estado',Configuracion::EJECUCION_DENEGADA);

		}else{
			$opcion="";
			$color="";
			$style="";
		}
		$Localidades = Localidad::all();

		$datos=[
			'actividades'=>$actividades,
			'tipo'=>$opcion,
			'color'=>$color,
			'style'=>$style,
			'Localidades'=>$Localidades,
		];
		
		return view('AdminitracionActividad.adminitracionActividad',$datos);
   
    }


   
}
